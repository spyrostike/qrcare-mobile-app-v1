import React from 'react';
import { GestureResponderEvent, StyleSheet, ViewProps, TouchableHighlight } from 'react-native';
import { Divider, Layout, Text } from '@ui-kitten/components';
import { MainTheme } from '../constants/LOV';

export interface ProfileSettingProps extends ViewProps {
  hint: string;
  value?: string;
  onPress?: (event: GestureResponderEvent) => void
}

export const ProfileSetting = (props: ProfileSettingProps): React.ReactElement => {

  const { style, hint, value, onPress, ...layoutProps } = props;

  return (
    <React.Fragment>
      <TouchableHighlight onPress={onPress}>
        <>
          <Layout
            level='1'
            {...layoutProps}
            style={[styles.container, style]}>
            <Text
              appearance='hint'
              category='s1'>
              {hint}
            </Text>
            <Text category='s1' status='control' style={onPress && { color: MainTheme.buttonColor }}  >
              {value}
            </Text>
          </Layout>
          <Divider/>
        </>
      </TouchableHighlight>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
