import React from 'react'
import {
  ImageBackgroundProps,
  ImageStyle,
  StyleProp,
  StyleSheet,
  View,
} from 'react-native'
import { CheckBox, Icon, Spinner, Text } from '@ui-kitten/components'
import FastImage, { Source } from 'react-native-fast-image'
import { MainTheme } from '../constants/LOV';


export interface ImageProps {
  index?: number;
  source: Source | number;
  style?: StyleProp<ImageStyle>
  children?: React.ReactChildren;
  checkboxVisabled?: boolean;
  onCheckChange?: (checked: boolean, index: number) => void;
};


const DEFAULT_OVERLAY_COLOR: string = 'rgba(0, 0, 0, 0.15)';

export const ImageItem = (props: ImageProps): React.ReactElement<ImageBackgroundProps> => {
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [isError, setIsError] = React.useState<boolean>(false);
  const [isChecked, setIsChecked] = React.useState<boolean>(false);

  const { children, checkboxVisabled, index, onCheckChange, source, style, ...ImageProps } = props;
  const { overlayColor: derivedOverlayColor, ...containerStyle } = StyleSheet.flatten(style);

  const onCheckBoxChange = (checked: boolean): void => {
    setIsChecked(checked)
    onCheckChange && onCheckChange(checked, index as number)
  }

  const onError = (): void => { 
    setIsError(true)
    setIsLoading(false)
  }

  return (
    <FastImage
      {...ImageProps}
      style={[ styles.container, containerStyle ]}
      onLoadStart={() => {
        setIsError(false)
        setIsLoading(true)}
      }
      onLoadEnd={() => {setIsLoading(false)}}
      source={source}
      onError={onError}>
      {children}
      
      {
        isLoading && 
        <View style={styles.absoluteContainer}>
          <Spinner size='giant' />
        </View>
      }

      {
        isError && 
        <View style={styles.absoluteContainer}>
          <Icon 
            name='image-broken' 
            pack='material-community' 
            style={styles.errorIcon}/>
        </View>
      }
      
      {
        checkboxVisabled === true &&
        <View style={styles.checkedContainer} >
          <CheckBox
            checked={isChecked}
            onChange={onCheckBoxChange}
          />
        </View>
      }
    </FastImage>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center'
  },
  absoluteContainer: { 
    flex: 1, 
    position: 'absolute', 
    justifyContent: 'center', 
    alignItems: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  checkedContainer: { 
    flex: 1,
    width: '100%', 
    alignItems: 'flex-end', 
    flexDirection: 'column-reverse',
    padding: 5 
  },
  errorIcon: { width: 50, height: 50, alignSelf: 'center', color: MainTheme.buttonColor }
})
