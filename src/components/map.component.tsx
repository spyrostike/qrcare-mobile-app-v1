import React from 'react';
import { Linking, Platform, PermissionsAndroid, StyleSheet, ToastAndroid, TouchableOpacity, View } from 'react-native';
import { Button, Card, Icon, Text } from '@ui-kitten/components'
import MapView, { Callout, MapViewProps, Marker, PROVIDER_GOOGLE } from 'react-native-maps'
import Geolocation, { GeoCoordinates } from 'react-native-geolocation-service'
import { MainTheme } from '../constants/LOV';

export interface MapProps extends MapViewProps {
  location?: GeoCoordinates | null;
  onLocationChange?: (coord: GeoCoordinates) => void;
  error?: string;
  previewOnly?: boolean;
}

export const Map = (props: MapProps): React.ReactElement => {
  const { error, location, onLocationChange, previewOnly, style, ...layoutProps } = props;

  const [currentLocation, setCurrentLocation] = React.useState<any>(null)
  const [region, setRegion] = React.useState<any>(null)
  const [mapRef, setMapRef] = React.useState<any>()
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  React.useEffect(() => {
    !previewOnly &&_getLocation(_onSetLocationSuccess)
  }, [])

  React.useEffect(() => {
    if (location) {
      _setRegion(location)
      _setCurrentLocation(location)
    }
  }, [location])

  const _onSetLocationSuccess = (position: GeoCoordinates) => {
    _setRegion(position)
    _setCurrentLocation(position)
    _onLocationChange(position)
  }

  const _setRegion = (position: GeoCoordinates) => {
    setRegion({
      latitude: position.latitude * 1,
      longitude: position.longitude * 1,
      latitudeDelta:  0.0922,
      longitudeDelta: 0.0421
    })
  }

  const _setCurrentLocation = (position: GeoCoordinates) => {
    const data = {
      title: 'ตำแหน่งของคุณ',
      coordinate: {
        latitude: position.latitude * 1,
        longitude: position.longitude * 1
      }
    }
    setCurrentLocation(data)
  }

  const _onLocationChange = (position: GeoCoordinates) => {
    const data: GeoCoordinates = {
      latitude: position.latitude,
      longitude: position.longitude
    } as GeoCoordinates
    
    onLocationChange && onLocationChange(data)
  } 

  const _hasLocationPermission = async () => {
    if (Platform.OS === 'ios' ||
        (Platform.OS === 'android' && Platform.Version < 23)) {
      return true
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    )

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    )

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true

    if (status === PermissionsAndroid.RESULTS.DENIED) {
        ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG)
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
        ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG)
    }

    return false
  }

  const _getLocation = async (success) => {
    const permission = await _hasLocationPermission()
    if (!permission) return

    Geolocation.getCurrentPosition(
      (position) => { success(position.coords) },
      (error) => {
        console.log('error', error)
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50, forceRequestLocation: true }
    )
  }

  const onRefreshButtonPress = (): void => {
    setRegion(null)
    _getLocation(_onSetLocationSuccessAndGotoRegionAndReNewDelta)
  }

  const gotoGoogleApp = () => {
    if (!currentLocation) return
    console.log('gotoGoogleAppc', currentLocation)
    const url = `http://maps.google.com/maps?daddr=${currentLocation.coordinate.latitude * 1},${currentLocation.coordinate.longitude * 1}`
    Linking.openURL(url)
  }

  const _onSetLocationSuccessAndGotoRegionAndReNewDelta = (position: GeoCoordinates) => {
    _onSetLocationSuccess(position)
    const data = {
        latitude: position.latitude,
        longitude: position.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
    }

    _setCurrentLocation(position)
    mapRef && mapRef.animateToRegion(data, 350)
  }

  const _renderCurrentMarker = (marker) => {
    return (
      <Marker key={`dss${Date.now()}`} tracksViewChanges={true} coordinate={marker.coordinate} >
        <Callout style={{width: 100}} >
          <Text>{marker.title}</Text>
          <Text>{marker.description}</Text>
        </Callout>
      </Marker>
    )
  }

  const renderMap = (): React.ReactElement => (
    <View style={[ styles.mapHeaderContainer, previewOnly && { height: 250 } ]}>
      <View {...layoutProps} style={[ styles.container, style ]} >
        <MapView 
          provider={PROVIDER_GOOGLE} 
          ref={ref => {setMapRef(ref)}} 
          style={styles.map}
          initialRegion={region} >
          { currentLocation  && _renderCurrentMarker(currentLocation) }
        </MapView>
        <View style={{ width: 35, height: 35, position: 'absolute', bottom: 0, right: 0 }}>
          <TouchableOpacity onPress={gotoGoogleApp}>
            <Icon name='near-me' pack='material-community' style={styles.iconButton} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )

  return (
    <View>
      <Text
        style={{ fontSize: 12 }}
        appearance='hint'
        category='s1'>
        Location
      </Text>
      <Card
        style={styles.mapContainer}
        header={() => renderMap()}
        // onPress={() => props.onItemPress(info.index)}
        >
        {
          !previewOnly &&
          <View style={styles.mapItemFooter}>
            <Button
              style={styles.mapButton}
              onPress={onRefreshButtonPress}>
              Try Again
            </Button>
          </View>
        }
      </Card>
      {
        errorMessage !== '' &&
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
          <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorMessage }</Text>
        </View>
      }
      {
        typeof error !== 'undefined' && error != '' &&
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
          <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ error }</Text>
        </View>
      }
    </View>

  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  mapContainer: {
    // aspectRatio: 1.0,
    height: 250,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  mapItemFooter: {
    height: 50,
    marginVertical: -30,
    marginHorizontal: -24,
  },
  mapButton: {
    backgroundColor: MainTheme.buttonColor,
    height: 50,
    borderWidth: 0
    
  },
  mapHeaderContainer: {
    height: 200,
    backgroundColor: MainTheme.colorSecondary,
  },
  iconButton: {
    width: 30,
    height: 30,
    borderRadius: 24,
    borderWidth: 0,
    color: '#3BB9FF'
  }
})