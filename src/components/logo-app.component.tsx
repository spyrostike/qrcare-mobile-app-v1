import React from 'react'
import { Image, ImageStyle, StyleProp } from 'react-native'

export interface LogoAppProps {
  style?: StyleProp<ImageStyle>;
}

export const LogoApp = (props: LogoAppProps): React.ReactElement => {
  const { style, ...ImageProps } = props;
  
  return (
    <Image
      {...ImageProps}
      resizeMode='contain'
      source={require('../assets/images/icon-app.jpg')}
      style={style} />
  )
}