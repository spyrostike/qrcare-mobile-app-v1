import React from 'react';
import { 
  ImageSourcePropType,
  ImageStyle, 
  Modal as NativeModal, 
  StyleProp, StyleSheet, 
  TextStyle,
  TouchableHighlight, 
  View, 
  ViewStyle
} from 'react-native';
import { Divider, Icon, Modal, Spinner, Text, TopNavigationAction } from '@ui-kitten/components'

import FastImage, { Source } from 'react-native-fast-image';
import { MainTheme } from '../constants/LOV';
import ImageViewer from 'react-native-image-zoom-viewer'
import { Toolbar } from '../components/toolbar.component';

export interface PhotoChooserBoxPreviewProps {
  hint: string;
  source?: string | null | undefined;
  thumbnail?: string | null;
  sourcePlaceHolder?: ImageSourcePropType;
  resizeMode?: 'contain' | 'cover' | 'stretch' | 'center';
  style?: StyleProp<ImageStyle>;
  containerStyle?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>

}

export const PhotoChooserBoxPreview = (props: PhotoChooserBoxPreviewProps): React.ReactElement => {
  const { containerStyle, hint, source, textStyle, thumbnail } = props;

  const [urlThumbnail, setUrlThumbnail] = React.useState<Source | number>(require('../assets/images/icon-avatar.png'));
  const [images, setImages ] = React.useState<any[]>([{ url: 'https://unsplash.it/400/400?image=1' }]);
  const [modalPreviewVisible, setModalPreviewVisible] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (typeof thumbnail !== 'undefined' && thumbnail !== null && thumbnail !== '') setUrlThumbnail({ uri: thumbnail })
    if (typeof source !== 'undefined' && source !== null && source !== '') setImages([{ url: source }])
  }, [thumbnail, source])
 
  const onBoxPress = (): void => {
    setModalPreviewVisible(!modalPreviewVisible)
  }

  return (
    <View style={[ styles.container, containerStyle ]}>
      {
        hint && <Text
          style={[ { fontSize: 15 }, textStyle ]}
          appearance='hint'
          category='s1'>
          { hint }
        </Text>
      }         

    <TouchableHighlight style={styles.takePhotoContainer} onPress={onBoxPress}>
      <FastImage
        style={{ flex: 1, justifyContent: 'center', borderRadius: 5, }}
        resizeMode={FastImage.resizeMode.cover}
        // source={require('../assets/images/icon-app.jpg')}\
        source={urlThumbnail}
        onError={() => {setUrlThumbnail(require('../assets/images/medical.png'))}}
      />
    </TouchableHighlight>
  
      <NativeModal visible={modalPreviewVisible} transparent={true}>
        <ImageViewer
          imageUrls={images}
          enableSwipeDown
          onSwipeDown={() => setModalPreviewVisible(false)}
          renderIndicator={() => <Spinner />}
          renderHeader={() => (
            <Toolbar
              titleStyle={{ color: MainTheme.colorSecondary }}
              rightControls={
                <TopNavigationAction
                  icon={() => (
                    <Icon name='close' pack='ant-design' style={{ width: 35, height: 35, color: MainTheme.colorSecondary }} />
                  )}
                  onPress={() => setModalPreviewVisible(false)}
                />
              }
              style={{ backgroundColor: MainTheme.colorPrimary }}
            />
          )}
          renderImage={(item) => (
            <FastImage 
              source={{...item.source}} 
              style={{...item.style}} 
            />
          )}
        />
      </NativeModal>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 18,
  },
  takePhotoContainer: {
    borderRadius: 5,
    justifyContent: 'center',
    backgroundColor: MainTheme.colorSecondary,
    // aspectRatio: 1.0,
    height: 250
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  childrenModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
})