import React from 'react';
import { Dimensions, RefreshControl, ListRenderItemInfo, StyleSheet, NativeSyntheticEvent, NativeScrollEvent } from 'react-native';
import { Card, List, ListElement, ListItemElement, ListProps, Text } from '@ui-kitten/components';
import { MenuItem } from '../model/menu-item.model';

export interface MenuGridListProps extends Omit<ListProps, 'renderItem'> {
  data: any[];
  renderItem: any;
  onItemPress?: (index: number) => void;
  refreshing?: boolean | null;
  onRefresh?: () => void;
  onScroll?: () => void;
}

export const MenuGridList = (props: MenuGridListProps): ListElement => {
  
  const { contentContainerStyle, onRefresh, onScroll, refreshing, ...listProps } = props;

  const _onScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const frameHeight = event.nativeEvent.layoutMeasurement.height
    const contentHeight = event.nativeEvent.contentSize.height
    const maxOffset = 0.95 * (contentHeight - frameHeight)
    const currentOffset = event.nativeEvent.contentOffset.y
    currentOffset >= maxOffset && !refreshing && onScroll ? onScroll() : null
  }

  const keyExtractor = (item, index) => index.toString()

  return (
    <List
      keyExtractor={keyExtractor}
      {...listProps}
      contentContainerStyle={[styles.container, contentContainerStyle]}
      refreshControl={onRefresh && <RefreshControl
        refreshing={refreshing ? refreshing : false}
        onRefresh={() => onRefresh && onRefresh()} />
      }
      onScroll={_onScroll}
      numColumns={2}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 8,
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    aspectRatio: 1.0,
    margin: 8,
    maxWidth: Dimensions.get('window').width / 2 - 24,
  },
  itemImage: {
    alignSelf: 'center',
    width: 64,
    height: 64,
  },
  itemTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
});
