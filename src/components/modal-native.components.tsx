import React from 'react';
import { Modal, ModalProps, StyleProp } from 'react-native';

export interface ModalNativeProps extends ModalProps {
  children?: React.ReactChildren;
}

export const ModalNative = (props: ModalNativeProps): React.ReactElement<ModalProps> => {
    const { animationType, children, transparent, visible, ...modalProps } = props;
    return (
      <Modal visible={visible}>
        {children}
      </Modal>
    )
}