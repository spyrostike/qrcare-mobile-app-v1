import React from 'react';
import { ImageSourcePropType, Modal as NativeModal, PermissionsAndroid, Platform, StyleSheet,View } from 'react-native';
import { Button, Divider, Icon, Modal, Spinner, Text, TopNavigationAction } from '@ui-kitten/components';
import ImagePicker, { Image } from 'react-native-image-crop-picker';
import FastImage from 'react-native-fast-image';
import { ProfileAvatar } from './profile-avatar.component';
import { CameraIcon } from '../assets/icons';
import { MainTheme } from '../constants/LOV';
import ImageViewer from 'react-native-image-zoom-viewer';
import { useStoreActions,  } from '../store';
import { FileProps } from '../store/models/file';
import { Toolbar } from '../components/toolbar.component';
import { API_IMAGE_PATH } from '../../app-config';

export interface PhotoChooserProps {
  onChange?: (file: FileProps[]) => void;
  source?: string | null | undefined;
  thumbnail?: string | null;
  sourcePlaceHolder?: ImageSourcePropType;
  error?: string;
  nameUpload?: string;
  tagUpload?: string;
}

export const PhotoChooser = (props: PhotoChooserProps): React.ReactElement => {
  const { error, nameUpload, onChange, source, tagUpload, thumbnail, sourcePlaceHolder } = props;

  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [urlThumbnail, setUrlThumbnail] = React.useState<string>('');
  const [images, setImages ] = React.useState<any[]>([{ url: '' }]);
  const [modalPreviewVisible, setModalPreviewVisible] = React.useState<boolean>(false);
  const [modalVisible, setModalVisible] = React.useState<boolean>(false);
  
  React.useEffect(() => {
    if (typeof thumbnail !== 'undefined' && thumbnail !== null && thumbnail !== '') {
      setUrlThumbnail(thumbnail)
    } else {
      setUrlThumbnail('')
    }
    if (typeof source !== 'undefined' && source !== null && source !== '') {
      setImages([{ url: source }])
    } else {
      setImages([{ url: '' }])
    }
  }, [source, thumbnail])

  const checkPermission = async () => {
    const permissions = [
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
    ]

    if (Platform.OS === 'android') {
      await PermissionsAndroid.requestMultiple(permissions)
    }
  }

  checkPermission()

  const fileAdd = useStoreActions(a => a.file.add)

  const onImageChange = async(image: Image) => {
    setErrorMessage('')
    const data = {
      name: nameUpload,
      tag: tagUpload,
      type: image.mime,
      uri: image.path
    }

    console.log('data', data)

    try {
      const res = await fileAdd(data as FileProps) as FileProps[]
      // setUrlThumbnail('https://unsplash.it/400/400?image=1')
      // setImages([ 
      //   { url: 'https://unsplash.it/400/400?image=1' } 
      // ])
      setUrlThumbnail(API_IMAGE_PATH + '/' + res[1].path + '/' + res[1].name)
      setImages([ { url: API_IMAGE_PATH + '/' + res[0].path + '/' + res[0].name} ])
      onChange && onChange(res)
    } catch (error) {
      setErrorMessage(error)
    }
  }
  
  let placeHolder =  sourcePlaceHolder ? sourcePlaceHolder : require('../assets/images/icon-avatar.png')

  const onPhotoButtonPress = (): void => {
    setModalVisible(!modalVisible)
  }

  const renderPhotoButton = (): React.ReactElement => (
    <Button
      style={styles.editAvatarButton}
      status='basic'
      icon={CameraIcon}
      onPress={onPhotoButtonPress}
    />
  )

  const toggleModal = (): void => {
    setModalVisible(!modalVisible);
  };

  const onCameraTextPress = async () => {
    setModalVisible(!modalVisible);
    try {
      const image = await ImagePicker.openCamera({ mediaType: 'photo' }) as Image
      const crop = await ImagePicker.openCropper({ path: image.path })
      onImageChange(crop)
    } catch (error) {
      console.log("openCamera catch" + error.toString())
    }
  };

  const onSelectPhotoTextPress = async () => {
    try {
      setModalVisible(!modalVisible);
      const image = await ImagePicker.openPicker({ mediaType:  'photo' }) as Image
      const crop = await ImagePicker.openCropper({ path: image.path })
      await onImageChange(crop)
    } catch (error) {
      console.log("openCamera catch" + error.toString())
    }
  };
  

  const onPreviewTextPress = (): void => {
    setModalVisible(false);
    setModalPreviewVisible(true);
  };

  return (
    <View>
      <ProfileAvatar
        style={styles.profileAvatar}
        source={urlThumbnail ? { uri: urlThumbnail } : placeHolder}
        editButton={renderPhotoButton}
      />
      {
        errorMessage !== '' &&
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
          <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorMessage }</Text>
        </View>
      }
      {
        error !== '' &&
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
          <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ error }</Text>
        </View>
      }
      <Modal
        visible={modalVisible}
        onBackdropPress={toggleModal}
        backdropStyle={styles.backdrop}
        style={{ width: '60%' }}>
          <View style={styles.childrenModalCantainer}>
            <Text style={styles.childrenModalText} onPress={onCameraTextPress}>Camera</Text>
            <Divider />
            <Text style={styles.childrenModalText} onPress={onSelectPhotoTextPress}>Select Photo</Text>
            { urlThumbnail ? <Divider /> : null }
            {
              urlThumbnail ? <Text style={styles.childrenModalText} onPress={onPreviewTextPress}>Preview</Text> : null
            }
          </View>
      </Modal>
      <NativeModal visible={modalPreviewVisible} transparent={true}>
        <ImageViewer
          imageUrls={images}
          enableSwipeDown
          onSwipeDown={() => setModalPreviewVisible(false)}
          renderIndicator={() => <Spinner />}
          renderHeader={() => (
            <Toolbar
              titleStyle={{ color: MainTheme.colorSecondary }}
              rightControls={
                <TopNavigationAction
                  icon={() => (
                    <Icon name='close' pack='ant-design' style={{ width: 35, height: 35, color: MainTheme.colorSecondary }} />
                  )}
                  onPress={() => setModalPreviewVisible(false)}
                />
              }
              style={{ backgroundColor: MainTheme.colorPrimary }}
            />
          )}
          renderImage={(item) => (
            <FastImage source={{...item.source}} style={{...item.style}} />
          )}
        />
      </NativeModal>
    </View>
  )
}

const styles = StyleSheet.create({
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    borderRadius: 124 / 2,
    alignSelf: 'center',
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  previewBackdrop: {
    backgroundColor: 'rgba(0, 0, 0, 1)',
  },
  childrenModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
})