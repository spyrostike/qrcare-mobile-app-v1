import React from 'react'
import {
  ImageBackgroundProps,
  ImageStyle,
  StyleProp,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native'
import { CheckBox, Icon, Spinner, Text } from '@ui-kitten/components'
import FastImage, { Source } from 'react-native-fast-image'
import { MainTheme } from '../constants/LOV';


export interface ImageProps extends ViewStyle {
  index?: number;
  source: Source | number;
  style?: StyleProp<ImageStyle>
  children?: React.ReactNode;
  checkboxVisabled?: boolean;
  onCheckChange?: (checked: boolean, index: number) => void;
};


const DEFAULT_OVERLAY_COLOR: string = 'rgba(0, 0, 0, 0.15)';

export const ImageItemText = (props: ImageProps): React.ReactElement<ImageBackgroundProps> => {

  const [isChecked, setIsChecked] = React.useState<boolean>(false);

  const { children, checkboxVisabled, index, onCheckChange, source, style, ...ImageProps } = props;

  const onCheckBoxChange = (checked: boolean): void => {
    setIsChecked(checked)
    onCheckChange && onCheckChange(checked, index as number)
  }

  return (
    <FastImage
      {...ImageProps}
      style={[ styles.container, style ]}
      source={source}>
      {children}
      {
        checkboxVisabled === true &&
        <View style={styles.checkedContainer} >
          <CheckBox
            checked={isChecked}
            onChange={onCheckBoxChange}
          />
        </View>
      }
    </FastImage>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center'
  },
  absoluteContainer: { 
    flex: 1, 
    position: 'absolute', 
    justifyContent: 'center', 
    alignItems: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  checkedContainer: { 
    flex: 1,
    width: '100%', 
    alignItems: 'flex-end', 
    flexDirection: 'column-reverse',
    padding: 5 
  },
  errorIcon: { width: 50, height: 50, alignSelf: 'center', color: MainTheme.buttonColor }
})
