import React from 'react'
import {
  Dimensions,
  ImageStyle,
  StyleProp,
  StyleSheet,
  TouchableHighlight,
  View,
} from 'react-native'
import { 
  Card,
  CheckBox,
  Text
} from '@ui-kitten/components';
import FastImage, { Source } from 'react-native-fast-image'
import { MainTheme } from '../constants/LOV';
import { Image } from '../components/image.component';

export interface CreatureItemProps {
  text: string;
  index?: number;
  source: string;
  style?: StyleProp<ImageStyle>
  checkboxVisabled?: boolean;
  onPress?: (index: number) => void;
  onLongPress?: () => void;
  onCheckChange?: (checked: boolean, index: number) => void;
};

export const CreatureItemBox = (props: CreatureItemProps): React.ReactElement => {
  const [isChecked, setIsChecked] = React.useState<boolean>(false);

  const { checkboxVisabled, index, onPress, onLongPress, onCheckChange, source, style, text } = props;
  
  const onCheckBoxChange = (checked: boolean): void => {
    setIsChecked(checked)
    onCheckChange && onCheckChange(checked, index as number)
  }

  return (
    <TouchableHighlight
      style={styles.item}
      onPress={() => onPress && onPress(index as number)}
      onLongPress={() => onLongPress && onLongPress()} 
    >
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: MainTheme.colorSecondary, borderRadius: 5 }}>
        <Image
          style={styles.avatarImage}
          resizeMode={FastImage.resizeMode.cover}
          source={source}
        />
        <Text
          style={styles.itemTitle}
          category='s2'>
          {text}
        </Text>
        {
          checkboxVisabled &&
          <View style={styles.checkedContainer} >
            <CheckBox
              checked={isChecked}
              onChange={onCheckBoxChange}
            />
          </View>
        }
      </View>
    </TouchableHighlight>
  )
}

const styles = StyleSheet.create({
  item: {
    flex: 1,
    aspectRatio: 1.0,
    margin: 8,
    maxWidth: Dimensions.get('window').width / 2 - 24
  },
  itemTitle: {
    alignSelf: 'center',
    marginTop: 8
  },
  avatarImage: { 
    aspectRatio: 1.0,
    height: 84, 
    justifyContent: 'center', 
    alignSelf: 'center', 
    borderRadius: 84 / 2, 
    backgroundColor: MainTheme.colorPrimary
  },
  checkedContainer: { 
    flex: 1,
    aspectRatio: 1.0,
    alignItems: 'flex-end', 
    flexDirection: 'column-reverse',
    padding: 5,
    position: 'absolute', 
    right: 0,
    bottom: 0
  }
})