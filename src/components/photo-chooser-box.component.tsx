import React from 'react';
import { Platform, Modal as NativeModal, PermissionsAndroid, StyleSheet, TouchableHighlight, View } from 'react-native';
import { Divider, Icon, Modal, Spinner, Text, TopNavigationAction } from '@ui-kitten/components'
import ImagePicker, { Image } from 'react-native-image-crop-picker';
import { FileProps } from '../store/models/file';
import FastImage from 'react-native-fast-image';
import { API_IMAGE_PATH } from '../../app-config';
import { MainTheme } from '../constants/LOV';
import { useStoreActions } from '../store';
import ImageViewer from 'react-native-image-zoom-viewer'
import { Toolbar } from '../components/toolbar.component';

export interface PhotoChooserBoxProps {
  hint: string
  onChange?: (file: FileProps[]) => void;
  source?: string | null | undefined;
  thumbnail?: string | null;
  error?: string;
  nameUpload?: string;
  tagUpload?: string;
}

export const PhotoChooserBox = (props: PhotoChooserBoxProps): React.ReactElement => {
  const { error, nameUpload, hint, onChange, source, tagUpload, thumbnail } = props;

  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [urlThumbnail, setUrlThumbnail] = React.useState<string>('');
  const [images, setImages ] = React.useState<any[]>([{ url: '' }]);
  const [modalPreviewVisible, setModalPreviewVisible] = React.useState<boolean>(false);
  const [modalVisible, setModalVisible] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (typeof thumbnail !== 'undefined' && thumbnail !== null && thumbnail !== '') {
      setUrlThumbnail(thumbnail)
    } else {
      setUrlThumbnail('')
    }
    if (typeof source !== 'undefined' && source !== null && source !== '') {
      setImages([{ url: source }])
    } else {
      setImages([{ url: '' }])
    }
  }, [source, thumbnail])
  
  const  checkPermission = async () => {
    const permissions = [
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
    ]

    if (Platform.OS === 'android') {
      await PermissionsAndroid.requestMultiple(permissions)
    }
  }

  checkPermission()

  const fileAdd = useStoreActions(a => a.file.add)

  

  const onImageChange = async(image: Image) => {
    setErrorMessage('')
    const data = {
      name: nameUpload,
      tag: tagUpload,
      type: image.mime,
      uri: image.path
    }

    try {
      const res = await fileAdd(data as FileProps) as FileProps[]
      // setUrlThumbnail('https://unsplash.it/400/400?image=1')
      // setImages([ 
      //   { url: 'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460' } 
      // ])
      setUrlThumbnail(API_IMAGE_PATH + '/' + res[1].path + '/' + res[1].name)
      setImages([ { url: API_IMAGE_PATH + '/' + res[0].path + '/' + res[0].name} ])
      onChange && onChange(res)
    } catch (error) {
      setErrorMessage(error)
    }
  }

  const onBoxPress = (): void => {
    setModalVisible(!modalVisible)
  }

  const toggleModal = (): void => {
    setModalVisible(!modalVisible);
  }

  const onCameraTextPress = async () => {
    setModalVisible(!modalVisible);
    try {
      const image = await ImagePicker.openCamera({ mediaType: 'photo' }) as Image
      const crop = await ImagePicker.openCropper({ path: image.path })
      onImageChange(crop)
    } catch (error) {
      console.log("openCamera catch" + error.toString())
    }
  };

  const onSelectPhotoTextPress = async () => {
    try {
      setModalVisible(!modalVisible);
      const image = await ImagePicker.openPicker({ mediaType:  'photo' }) as Image
      const crop = await ImagePicker.openCropper({ path: image.path })
      await onImageChange(crop)
    } catch (error) {
      console.log("openCamera catch" + error.toString())
    }
  };
  
  const onPreviewTextPress = (): void => {
    setModalVisible(!modalVisible);
    setModalPreviewVisible(!modalPreviewVisible)
  };

  const renderTakePictureIcon = (): React.ReactElement => (
    <TouchableHighlight style={styles.takePhotoContainer} onPress={onBoxPress}>
      <>
        <Icon name='camera' style={{ width: 30, height: 30, alignSelf: 'center' }} fill={MainTheme.colorPrimary} />
        <Text style={styles.takePhotoTitle} category='s2'>
          Take Picture
        </Text>
      </>
    </TouchableHighlight>
  )

  const renderPicture = (): React.ReactElement => (
    <TouchableHighlight style={styles.takePhotoContainer} onPress={onBoxPress}>
      <FastImage
        style={{ flex: 1, justifyContent: 'center', borderRadius: 5, }}
        resizeMode={FastImage.resizeMode.cover}
        // source={require('../assets/images/icon-app.jpg')}\
        source={{ uri: urlThumbnail }}
        >
      </FastImage>
    </TouchableHighlight>
  )

  return (
    <>
      {
        hint && <Text
          style={{ fontSize: 12 }}
          appearance='hint'
          category='s1'>
          { hint }
        </Text>
      }         

      { urlThumbnail ? renderPicture() : renderTakePictureIcon() }
      {
        errorMessage !== '' &&
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
          <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorMessage }</Text>
        </View>
      }
      {
        error != '' &&
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
          <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ error }</Text>
        </View>
      }
      <Modal
        visible={modalVisible}
        onBackdropPress={toggleModal}
        backdropStyle={styles.backdrop}
        style={{ width: '60%' }}>
          <View style={styles.childrenModalCantainer}>
            <Text style={styles.childrenModalText} onPress={onCameraTextPress}>Camera</Text>
            <Divider />
            <Text style={styles.childrenModalText} onPress={onSelectPhotoTextPress}>Select Photo</Text>
            { urlThumbnail ? <Divider /> : null }
            {
              urlThumbnail ? <Text style={styles.childrenModalText} onPress={onPreviewTextPress}>Preview</Text> : null
            }
          </View>
      </Modal>
      <NativeModal visible={modalPreviewVisible} transparent={true}>
        <ImageViewer
          imageUrls={images}
          enableSwipeDown
          onSwipeDown={() => setModalPreviewVisible(false)}
          renderIndicator={() => <Spinner />}
          renderHeader={() => (
            <Toolbar
              titleStyle={{ color: MainTheme.colorSecondary }}
              rightControls={
                <TopNavigationAction
                  icon={() => (
                    <Icon name='close' pack='ant-design' style={{ width: 35, height: 35, color: MainTheme.colorSecondary }} />
                  )}
                  onPress={() => setModalPreviewVisible(false)}
                />
              }
              style={{ backgroundColor: MainTheme.colorPrimary }}
            />
          )}
          renderImage={(item) => (
            <FastImage source={{...item.source}} style={{...item.style}} />
          )}
        />
      </NativeModal>
    </>
  )

}

const styles = StyleSheet.create({
  takePhotoContainer: {
    borderRadius: 5,
    justifyContent: 'center',
    backgroundColor: MainTheme.colorSecondary,
    // aspectRatio: 1.0,
    height: 250
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  childrenModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
})