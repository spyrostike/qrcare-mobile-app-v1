import React from 'react';
import { Image, StyleSheet, ViewProps } from 'react-native';
import { Button, Divider, Layout, Text } from '@ui-kitten/components';
import { MainTheme } from '../constants/LOV';
import { ImageOff } from '../assets/icons';

export interface ProfileImageSettingProps extends ViewProps {
  hint: string;
  value: any;
}

export const ProfileImageSetting = (props: ProfileImageSettingProps): React.ReactElement => {

  const { style, hint, value, ...layoutProps } = props;

  return (
    <React.Fragment>
      <Layout
        level='1'
        {...layoutProps}
        style={[styles.container, style]}>
        <Text
          appearance='hint'
          category='s1'>
          {hint}
        </Text>
        {
          value ? 
            <Image
              resizeMode='contain'
              source={value}
              style={{ width: 30, height: 30 }} />
            :
            <Button
                style={[ styles.iconButton, styles.plusButton ]}
                icon={ImageOff}
                // onPress={onScanQRCodeButtonPress}
              />
        }
      </Layout>
      <Divider/>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  iconButton: {
    width: 24,
    height: 24,
    backgroundColor: MainTheme.buttonColor,
    borderColor: MainTheme.buttonColor
  },
  plusButton: {
    borderRadius: 24,
    marginHorizontal: 0,
  }
});
