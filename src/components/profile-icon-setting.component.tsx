import React from 'react';
import { Image, ImageStyle, StyleSheet, ViewProps, GestureResponderEvent } from 'react-native';
import { Button, IconElement, Divider, Layout, Text } from '@ui-kitten/components';
import { MainTheme } from '../constants/LOV';

export interface ProfileIConSettingProps extends ViewProps {
  hint: string;
  icon: (ImageStyle) => IconElement;
  onPress?: (event: GestureResponderEvent) => void
}

export const ProfileIconSetting = (props: ProfileIConSettingProps): React.ReactElement => {

  const { style, hint, icon, onPress, ...layoutProps } = props;

  return (
    <React.Fragment>
      <Layout
        level='1'
        {...layoutProps}
        style={[styles.container, style]}>
        <Text
          appearance='hint'
          category='s1'>
          {hint}
        </Text>
 
        <Button
            style={[ styles.iconButton, styles.plusButton ]}
            icon={icon}
            onPress={onPress}
          />
        
      </Layout>
      <Divider/>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  iconButton: {
    width: 24,
    height: 24,
    backgroundColor: MainTheme.buttonColor,
    borderColor: MainTheme.buttonColor
  },
  plusButton: {
    borderRadius: 24,
    marginHorizontal: 0,
  }
});
