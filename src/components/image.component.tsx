import React from 'react'
import { ImageSourcePropType, ImageStyle, StyleProp, StyleSheet, View } from 'react-native'
import FastImage, { Source } from 'react-native-fast-image'
import { MainTheme } from '../constants/LOV';

export interface ImageProps {
  source?: string | null | undefined;
  sourcePlaceHolder?: ImageSourcePropType;
  resizeMode?: 'contain' | 'cover' | 'stretch' | 'center',
  style?: StyleProp<ImageStyle>
}

export const Image = (props: ImageProps): React.ReactElement => {
  const { resizeMode, source, style, ...ImageProps } = props;
  const [imageSrc, setImageSrc] = React.useState<Source | number>(require('../assets/images/icon-avatar.png'));

  React.useEffect(() => {
    if (typeof source !== 'undefined' && source !== null && source !== '') setImageSrc({ uri: source })
  }, [])
  
  const onError = (): void => {
    setImageSrc(require('../assets/images/icon-avatar.png'))
  }

  return (
      <FastImage
        {...ImageProps}
        style={[ styles.image, style ]}
        resizeMode={resizeMode}
        onError={onError}
        // source={require('../assets/images/icon-app.jpg')}\
        source={imageSrc}
        >
      </FastImage>
  )
}

const styles = StyleSheet.create({
  image: { 
    width: 84, 
    height: 84, 
    justifyContent: 'center', 
    alignSelf: 'center', 
    borderRadius: 84 / 2, 
    backgroundColor: MainTheme.colorPrimary 
  }
})