import React from 'react';
import { ImageSourcePropType, ImageStyle, Modal, StyleProp, StyleSheet, TouchableHighlight, ViewProps } from 'react-native';
import { Icon, Spinner, TopNavigationAction } from '@ui-kitten/components';
import ImageViewer from 'react-native-image-zoom-viewer';
import FastImage, { Source }  from 'react-native-fast-image';
import { MainTheme } from '../constants/LOV';
import { Toolbar } from '../components/toolbar.component';

export interface ProfileAvatarPreviewProps {
  source?: string | null | undefined;
  thumbnail?: string | null;
  sourcePlaceHolder?: ImageSourcePropType;
  resizeMode?: 'contain' | 'cover' | 'stretch' | 'center',
  style?: StyleProp<ImageStyle>
}

export const ProfileAvatarPreview = (props: ProfileAvatarPreviewProps): React.ReactElement<ViewProps> => {
  const { resizeMode, source, style, thumbnail, ...ImageProps } = props;
  const [urlThumbnail, setUrlThumbnail] = React.useState<Source | number>(require('../assets/images/icon-avatar.png'));
  const [images, setImages ] = React.useState<any[]>([{ url: 'https://unsplash.it/400/400?image=1' }]);
  const [modalPreviewVisible, setModalPreviewVisible] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (typeof thumbnail !== 'undefined' && thumbnail !== null && thumbnail !== '') {
      setUrlThumbnail({ uri: thumbnail })
    } else {
      setUrlThumbnail({ uri: '' })
    }
    if (typeof source !== 'undefined' && source !== null && source !== '') {
      setImages([{ url: source }])
    } else {
      setImages([{ url: '' }])
    }
  }, [source, thumbnail])

  const onPress = (): void => {
    setModalPreviewVisible(true);
  }

  const onThumbnailError = (): void => {
    setUrlThumbnail(require('../assets/images/icon-avatar.png'))
  }

  return (
    <>
      <TouchableHighlight onPress={onPress}>
        <FastImage 
          {...ImageProps}
          style={[ styles.image, style ]}
          resizeMode={resizeMode}
          onError={onThumbnailError}
          // source={require('../assets/images/icon-app.jpg')}\
          source={urlThumbnail} 
        />
      </TouchableHighlight>
      <Modal visible={modalPreviewVisible} transparent={true}>
        <ImageViewer
          imageUrls={images}
          enableSwipeDown
          onSwipeDown={() => setModalPreviewVisible(false)}
          renderIndicator={() => <Spinner />}
          renderHeader={() => (
            <Toolbar
              titleStyle={{ color: MainTheme.colorSecondary }}
              rightControls={
                <TopNavigationAction
                  icon={() => (
                    <Icon name='close' pack='ant-design' style={{ width: 35, height: 35, color: MainTheme.colorSecondary }} />
                  )}
                  onPress={() => setModalPreviewVisible(false)}
                />
              }
              style={{ backgroundColor: MainTheme.colorPrimary }}
            />
          )}
          renderImage={(item) => (
            <FastImage 
              source={{...item.source}} 
              style={{...item.style}} 
              onError={() => setImages([{ url: 'https://unsplash.it/400/400?image=1' }])}
            />
          )}
        />
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  image: { 
    width: 124, 
    height: 124, 
    justifyContent: 'center', 
    alignSelf: 'center', 
    borderRadius: 124 / 2, 
    backgroundColor: MainTheme.colorPrimary 
  }
});

