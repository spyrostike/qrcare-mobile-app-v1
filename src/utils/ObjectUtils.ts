import { API_IMAGE_PATH } from '../../app-config'
import { FileProps } from '../store/models/file'

export const genenrateFileObjToServer = (file: FileProps) => {
  var formData = new FormData()

  if (file.id) { formData.append('id', file.id) }
  if (file.refId) { formData.append('refId', file.refId) }
  formData.append('tag', file.tag)
  formData.append('type', 'image')

  formData.append('file-data', {
      uri: file.uri,
      name: file.name,
      type: file.type
  })

  return formData
}

export const convertMemberFromServerToApp = (payload) => ({
  id: payload.id,
  code: payload.code,
  username: payload.username,
  password: '',
  firstName: payload.first_name,
  lastName: payload.last_name,
  gender: payload.gender,
  genderName: payload.gender_name,
  bloodType: payload.blood_type,
  bloodTypeName: payload.blood_type_name,
  birthDate: payload.birth_date,
  relationship: payload.relationship,
  identificationNo: payload.identification_no,
  email: payload.email,
  contactNo1: payload.contact_no1,
  contactNo2: payload.contact_no2,
  address1: payload.address1,
  address2: payload.address2,
  address3: payload.address3,
  status: payload.status,
  placeLatitude: payload.place_latitude,
  placeLongitude: payload.place_longitude,
  profileImageId: payload.profile_image_id,
  profileImage: payload.profile_image_name ? API_IMAGE_PATH + '/' + payload.profile_image_path  + '/' + payload.profile_image_name : '',
  profileImageThumbnailId: payload.profile_image_thumbnail_id,
  profileImageThumbnail: payload.profile_image_thumbnail_name ? API_IMAGE_PATH + '/' + payload.profile_image_thumbnail_path  + '/' + payload.profile_image_thumbnail_name : '',
  locationImageId: payload.location_image_id,
  locationImage: payload.location_image_name ? API_IMAGE_PATH + '/' + payload.location_image_path  + '/' + payload.location_image_name : '',
  locationImageThumbnailId: payload.location_image_thumbnail_id,
  locationImageThumbnail: payload.location_image_thumbnail_name ? API_IMAGE_PATH + '/' + payload.location_image_thumbnail_path  + '/' + payload.location_image_thumbnail_name : '',
  qrcodeItemId: ''
})

export const convertCreatureFromServerToApp = (payload) => ({ 
  qrcodeItemId: payload.qrcode_item_id,
  id: payload.id,
  firstName: payload.first_name,
  lastName: payload.last_name,
  gender: payload.gender,
  bloodType: payload.blood_type,
  birthDate: payload.birth_date,
  weight: payload.weight.toString(),
  height: payload.height.toString(),
  creatureImageId: payload.creature_image_id,
  creatureImage: payload.creature_image_name ? API_IMAGE_PATH + '/' + payload.creature_image_path  + '/' + payload.creature_image_name : '',
  creatureImageThumbnailId: payload.creature_image_thumbnail_id,
  creatureImageThumbnail: payload.creature_image_thumbnail_name ? API_IMAGE_PATH + '/' + payload.creature_image_thumbnail_path  + '/' + payload.creature_image_thumbnail_name : '',
  birthCertificateId: payload.birth_certificate_image_id,
  birthCertificate: payload.birth_certificate_image_name ? API_IMAGE_PATH + '/' + payload.birth_certificate_image_path + '/' + payload.birth_certificate_image_name : '',
  birthCertificateThumbnailId: payload.birth_certificate_image_thumbnail_id,
  birthCertificateThumbnail: payload.birth_certificate_image_thumbnail_name ? API_IMAGE_PATH + '/' + payload.birth_certificate_image_thumbnail_path + '/' + payload.birth_certificate_image_thumbnail_name : '',
  genderName: payload.gender_name,
  bloodTypeName: payload.blood_type_name,
  memberId: payload.member_id
})

export const convertDrugAllergyFromServerToApp = (payload) => ({ 
  id: payload.id,
  name: payload.name,
  description: payload.description,
  imageId: payload.drug_allergy_image_id,
  image: payload.drug_allergy_image_name ? API_IMAGE_PATH + '/' + payload.drug_allergy_image_path  + '/' + payload.drug_allergy_image_name : '',
  imageThumbnailId: payload.drug_allergy_image_thumbnail_id,
  imageThumbnail: payload.drug_allergy_image_thumbnail_name ? API_IMAGE_PATH + '/' + payload.drug_allergy_image_thumbnail_path  + '/' + payload.drug_allergy_image_thumbnail_name : '',
  memberId: payload.member_id,
  creatureId: payload.creature_id
})

export const convertDrugFromServerToApp = (payload) => ({ 
  id: payload.id,
  name: payload.name,
  description: payload.description,
  imageId: payload.drug_image_id,
  image: payload.drug_image_name ? API_IMAGE_PATH + '/' + payload.drug_image_path  + '/' + payload.drug_image_name : '',
  imageThumbnailId: payload.drug_image_thumbnail_id,
  imageThumbnail: payload.drug_image_thumbnail_name ? API_IMAGE_PATH + '/' + payload.drug_image_thumbnail_path  + '/' + payload.drug_image_thumbnail_name : '',
  memberId: payload.member_id,
  creatureId: payload.creature_id
})

export const convertFoodAllergyFromServerToApp = (payload) => ({ 
  id: payload.id,
  name: payload.name,
  description: payload.description,
  imageId: payload.food_allergy_image_id,
  image: payload.food_allergy_image_name ? API_IMAGE_PATH + '/' + payload.food_allergy_image_path  + '/' + payload.food_allergy_image_name : '',
  imageThumbnailId: payload.food_allergymage_thumbnail_id,
  imageThumbnail: payload.food_allergy_image_thumbnail_name ? API_IMAGE_PATH + '/' + payload.food_allergy_image_thumbnail_path  + '/' + payload.food_allergy_image_thumbnail_name : '',
  memberId: payload.member_id,
  creatureId: payload.creature_id
})

export const convertMedicalFromServerToApp = (payload) => ({ 
  id: payload.id,
  hospitalName: payload.hospital_name,
  hospitalTel: payload.hospital_tel, 
  patientId: payload.patient_id, 
  bloodType: payload.blood_type, 
  bloodTypeName: payload.blood_type_name,
  doctorName: payload.doctor_name, 
  doctorContactNo: payload.doctor_contact_no,
  congenitalDisorder: payload.congenital_disorder,
  creatureId: payload.creature_id,
  birthCertificate: payload.birth_certificate_image_name ? API_IMAGE_PATH + '/' + payload.birth_certificate_image_path + '/' + payload.birth_certificate_image_name : '',
})

export const convertTreamentHistoryFromServerToApp = (payload) => ({
  id: payload.id,
  date: payload.date,
  time: payload.time,
  description: payload.description,
  department: payload.department,
  fileCategory: payload.file_category,
  fileCategoryOther: payload.file_category_other,
  fileCategoryName: payload.file_category_name,
  departmentName: payload.department_name,
  creatureId: payload.creature_id,
  imageId: payload.treatment_history_image_id,
  image: payload.treatment_history_image_name ? API_IMAGE_PATH + '/' + payload.treatment_history_image_path  + '/' + payload.treatment_history_image_name : '',
  imageThumbnailId: payload.treatment_history_image_thumbnail_id,
  imageThumbnail: payload.treatment_history_image_thumbnail_name ? API_IMAGE_PATH + '/' + payload.treatment_history_image_thumbnail_path  + '/' + payload.treatment_history_image_thumbnail_name : '',
  imageCount: payload.imageCount
})

export const convertFileFromServerToApp = (payload) => ({
  id: payload.id,
  uri:  payload.name ? API_IMAGE_PATH + '/' + payload.path  + '/' + payload.name : '',
  name: payload.name,
  type: payload.type,
  subType: payload.sub_type,
  size: payload.size,
  refId: payload.ref_id,
  tag: payload.tag,
  status: payload.status,
})

export const convertVaccineBookFromServerToApp = (payload) => ({
  id: payload.id,
  vaccineType: payload.vaccine_type,
  vaccineTypeName: payload.vaccine_type_name,
  vaccineTypeOther: payload.other,
  creatureId: payload.creature_id,
})

export const convertVaccineFromServerToApp = payload => ({
  id: payload.id,
  hospitalName: payload.hospital_name,
  date: payload.date,
  description: payload.description,
  vaccineBookId: payload.vaccine_book_id,
  imageId: payload.vaccine_image_id,
  image: payload.vaccine_image_name ? API_IMAGE_PATH + '/' + payload.vaccine_image_path  + '/' + payload.vaccine_image_name : '',
  imageThumbnailId: payload.vaccine_image_thumbnail_id,
  imageThumbnail: payload.vaccine_image_thumbnail_name ? API_IMAGE_PATH + '/' + payload.vaccine_image_thumbnail_path  + '/' + payload.vaccine_image_thumbnail_name : ''
})
