import { CriteriaSearch } from "../model/criteria-search.model"

export const serializeQueryString = (obj: CriteriaSearch): string => {
  var str = '';
  for (var key in obj) {
      if (str != '') {
          str += '&';
      }
      str += key + '=' + encodeURIComponent(obj[key]);
  }
  return str
}