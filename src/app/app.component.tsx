import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { light, mapping } from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';

import { AppNavigator } from '../navigation/app.navigator';
import { AppRoute } from '../navigation/app-routes';
import { default as appTheme } from './app-mapping-theme.json';
import { StoreProvider } from "easy-peasy";
import store from '../store'

import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { AssetIconsPack } from '../assets/asset-icons';
import { AntDesignIconsPack } from '../assets/ant-design-icons';
import { MaterialCommunityIconsPack } from '../assets/material-comunity-icons';
import { EntypoIconsPack } from '../assets/entypo-icons';
import { MaterialIconsPack } from '../assets/material-icons';
import { FontAwesomeIconsPack } from '../assets/font-awesome';
import { FoundationIconsPack } from '../assets/foundation-icons';
import { IonIconsPack } from '../assets/ionicons-icons'

export default (): React.ReactFragment => {

  // This value is used to determine the initial screen
  const isAuthorized: boolean = false;

  const customMapping: any = appTheme;

  return (
    <StoreProvider store={store}>
      <IconRegistry icons={[
          EvaIconsPack, 
          AntDesignIconsPack,
          AssetIconsPack, 
          MaterialCommunityIconsPack, 
          MaterialIconsPack, 
          EntypoIconsPack, 
          FontAwesomeIconsPack,
          FoundationIconsPack,
          IonIconsPack
        ]}
      />
      <ApplicationProvider
        mapping={mapping}
        customMapping={customMapping}
        theme={light}>
        <SafeAreaProvider>
          <NavigationContainer>
            {/* <AppNavigator initialRouteName={isAuthorized ? AppRoute.HOME : AppRoute.AUTH}/> */}
            <AppNavigator initialRouteName={AppRoute.SPLASH}/>
          </NavigationContainer>
        </SafeAreaProvider>
      </ApplicationProvider>
    </StoreProvider>
  );
};
