/* eslint-disable no-console, no-param-reassign */
import { create } from 'apisauce'
import { API_ENDPOINT, APPLICATION_ID } from '../../app-config'
import store from '../store'
// import qs from 'qs'
// import * as R from 'ramda'

const api = create({
  baseURL: API_ENDPOINT,
  headers: {
    Accept: 'application/json',
    'Cache-Control': 'no-cache',
    accessId: APPLICATION_ID
  },
  timeout: 30000
})

const monitor = (response) => {
  const { config: { method, url }, data, status } = response
  // console.group(`Requesting [${method.toUpperCase()}] ${url}:`)
  // console.log('Response Status:', status)
  // console.log('Response Data:', data)
  console.groupEnd()
}

api.addMonitor(monitor)

api.addRequestTransform((request) => {
  const token = store.getState().member.token
  if (typeof token?.value !== 'undefined' && token?.value !== null) {
    request.headers['Authorization'] = `Bearer ${token.value}`
  }

  // if (R.contains(request.method, ['delete', 'post', 'put'])) {
  //   if (!(request.data instanceof FormData)) {
  //     request.headers['Content-Type'] = 'application/x-www-form-urlencoded'
  //     request.data = qs.stringify(request.data)
  //   }
  // }
})

api.addResponseTransform((response) => {
  // if (response.status >= 400 || !response.ok) {
  //   const error = new Error(response.status || response.problem)

  //   error.status = response.status;
  //   error.response = response;
  //   throw error;
  // }
  // console.log('dsv', response.data)
  // if (response.data.error !== undefined) {
  //   console.log('xxx', response.data)
  //   response.ok = false
  //   let message = (typeof response.data.error === 'object')
  //     ? response.data.error.message
  //     : response.data.error

  //   if (!message) {
  //     message = 'Error desconhecido'
  //   }

  //   response.data = { message }
  // } else {
  //   console.log('xxxx', response)
  //   const data = response.data.item || response.data.itens || null
  //   if (response.data['access-token'] !== undefined) {
  //      data.tokenapi = response.data['access-token']
  //   }
  //   if (response.data.total_itens !== undefined) {
  //     response.count = +response.data.total_itens
  //   }
  //   response.data = data
  // }
})

export default api