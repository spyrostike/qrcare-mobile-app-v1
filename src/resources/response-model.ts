interface DataModel {
  result: any
}

export interface ResponseModel {
  status: string;
  data: DataModel;
  errorMessage: string;
}

