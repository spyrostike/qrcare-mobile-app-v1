import React from 'react';
import { ImageStyle } from 'react-native';
import { Icon, IconElement } from '@ui-kitten/components';
import { MainTheme } from '../constants/LOV';

export const ArrowForwardIcon = (style): IconElement => (
  <Icon {...style} name='arrow-forward-outline'/>
);

export const EmailIcon = (style): IconElement => (
  <Icon {...style} name='email'/>
);

export const BackIcon = (style): IconElement => (
  <Icon {...style} name='arrow-back' fill={MainTheme.colorSecondary} />
);

export const LayoutIcon = (style): IconElement => (
  <Icon {...style} name='layout-outline'/>
);

export const PersonIcon = (style): IconElement => (
  <Icon {...style} name='person-outline'/>
);

export const MoreVerticalIcon = (style): IconElement => (
  <Icon {...style} name='more-vertical'/>
);

export const LogoutIcon = (style): IconElement => (
  <Icon {...style} name='log-out-outline'/>
);

export const InfoIcon = (style): IconElement => (
  <Icon {...style} name='info-outline'/>
);

export const AlertTriangleIcon = (style): IconElement => (
  <Icon {...style} name='alert-triangle-outline'/>
);

export const EyeIcon = (style): IconElement => (
  <Icon {...style} name='eye-outline'/>
);

export const EyeOffIcon = (style): IconElement => (
  <Icon {...style} name='eye-off-outline'/>
);

export const MenuIcon = (style): IconElement => (
  <Icon {...style} name='menu-outline' fill={MainTheme.colorSecondary} />
);

export const HomeIcon = (style): IconElement => (
  <Icon {...style} name='home-outline'/>
);

export const DoneIcon = (style): IconElement => (
  <Icon {...style} name='checkmark-outline'/>
);

export const DoneAllIcon = (style): IconElement => (
  <Icon {...style} name='done-all-outline'/>
);

export const GridIcon = (style): IconElement => (
  <Icon {...style} name='grid-outline'/>
);

export const SearchIcon = (style): IconElement => (
  <Icon {...style} name='search-outline'/>
);

export const PlusIcon = (style): IconElement => (
  <Icon {...style} name='plus-outline' fill={MainTheme.colorSecondary} />
);

export const SettingIcon = (style): IconElement => (
  <Icon {...style} name='settings' />
);

export const PrinterDisconnectGreen = (style) => (
  <Icon 
    {...style} 
    iconStyle={{ width: 20, height: 20, alignSelf: 'center', marginHorizontal: 8 }} 
    name='printer-disconnect-green' 
    pack='assets' />
);

// Setting form
export const APIIcon = (style): IconElement => (
  <Icon {...style} name='API' pack='ant-design' />
);

export const VanUtilityIcon = (style): IconElement => (
  <Icon {...style} name='van-utility' pack='material-community' />
);

export const ManIcon = (style): IconElement => (
  <Icon {...style} name='man' pack='entypo' />
);

export const LabelIcon = (style): IconElement => (
  <Icon {...style} name='label' pack='material-community' />
);


export const CameraIcon = (style): IconElement => (
  <Icon {...style} name='camera'/>
);

export const CloseIcon = (style): IconElement => (
  <Icon {...style} name='close' pack='ant-design' style={{ color: MainTheme.colorSecondary }} />
);

export const ImageOff = (style): IconElement => (
  <Icon {...style} name='image-off' pack='material-community' />
);

//standard
export const PaperPlaneIcon = (style: ImageStyle): IconElement => (
  <Icon {...style} name='paper-plane' fill={MainTheme.colorSecondary} />
);

//ant design
export const QRCodeIcon = (style): IconElement => (
  <Icon {...style} name='qrcode' pack='ant-design' style={{ color: MainTheme.colorSecondary }} />
);

export const EditIcon = (style): IconElement => (
  <Icon {...style} name='edit' pack='ant-design' style={{ color: MainTheme.colorSecondary }} />
);

export const MedicineBoxIcon = (style): IconElement => (
  <Icon {...style} name='medicinebox' pack='ant-design' />
);

//material-community
export const AccountCardDetailsIcon = (style): IconElement => (
  <Icon {...style} name='account-card-details' pack='material-community' />
);

export const BarcodeScanIcon = (style): IconElement => (
  <Icon {...style} name='barcode-scan' pack='material-community' />
);

export const FoodIcon = (style): IconElement => (
  <Icon {...style} name='food' pack='material-community' />
);

export const ImageBrokenIcon = (style): IconElement => (
  <Icon {...style} name='image-broken' pack='material-community' />
);

export const TrashIcon = (style): IconElement => (
  <Icon {...style} name='trash-can-outline' pack='material-community' style={{ color: MainTheme.colorDanger }} />
);


//material-icons
export const LocationOnIcon = (style): IconElement => (
  <Icon {...style} name='location-on' pack='material-icons' />
);

//foundation
export const KeyIcon = (style): IconElement => (
  <Icon {...style} name='key' pack='foundation-icons' />
);

//awesome
export const PowerOffIcon = (style): IconElement => (
  <Icon {...style} name='power-off' pack='font-awesome' />
);

//ionicons
export const NavigateIcon = (style): IconElement => (
  <Icon {...style} name='near-me' pack='material-community' style={{ color: MainTheme.colorSecondary }} />
);




