import React from 'react';
import { StyleSheet } from 'react-native';
import { Image } from 'react-native';

const IconProvider = (source) => ({
    toReactElement: ({ animation, ...style }) => {
        const { iconStyle } = StyleSheet.flatten(style)
        return <Image 
            style={iconStyle}
          source={source}
          resizeMode='contain' />
    }
        
});

export const AssetIconsPack = {
    name: 'assets',
    icons: {
        // home icon
        'campaign': IconProvider(require('./images/campaign.png')),
        'chart': IconProvider(require('./images/chart.png')),
        'customer': IconProvider(require('./images/customer.png')),
        'logout': IconProvider(require('./images/logout.png')),
        'order': IconProvider(require('./images/order.png')),
        'report': IconProvider(require('./images/report.png')),
        'warehouse': IconProvider(require('./images/warehouse.png')),
        //printer
        'printer-disconnect-green': IconProvider(require('./images/printer-disconnect-green.png')),
        //
        'cart': IconProvider(require('./images/cart.png')),
        'payment': IconProvider(require('./images/payment.png')),
        'send-back': IconProvider(require('./images/send-back.png')),
        'visit-customer': IconProvider(require('./images/visit-customer.png')),
        'check': IconProvider(require('./images/check.png')),
        'checkin': IconProvider(require('./images/checkin.png')),
        'mile': IconProvider(require('./images/mile.png')),
        'bill': IconProvider(require('./images/bill.png'))

    }
};