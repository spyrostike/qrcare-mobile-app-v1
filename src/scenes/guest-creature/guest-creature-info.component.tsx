import React from 'react';
import { Dimensions, Image, ScrollView, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { EdgeInsets, useSafeArea } from 'react-native-safe-area-context';
import { 
  Button, 
  Divider, 
  Layout, 
  LayoutElement, 
} from '@ui-kitten/components';
import moment from 'moment'
import FastImage from 'react-native-fast-image';
import {
  SafeAreaLayout,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { GuestCreatureInfoScreenProps } from '../../navigation/guest-creature.navigator';
import { AppRoute } from '../../navigation/app-routes';
import { Toolbar } from '../../components/toolbar.component';
import { MainTheme } from '../../constants/LOV';
import { CameraIcon, BackIcon, FoodIcon, MedicineBoxIcon } from '../../assets/icons';
import { ProfileAvatarPreview } from '../../components/profile-avatar-preview.component';
import { ProfileSetting } from '../../components/profile-setting.component';
import { ProfileIconSetting } from '../../components/profile-icon-setting.component';
import { PhotoChooserBoxPreview } from '../../components/photo-chooser-box-preview.component';
import { useStoreState, useStoreActions } from '../../store';

export const GuestCreatureInfoScreen = (props: GuestCreatureInfoScreenProps): LayoutElement => {
  const setState = useStoreActions(a => a.app.setState)
  const prevState = useStoreState(s => s.app.prevState)
  const item = useStoreState(s => s.creature.item)

  const onDrugIconPress = (): void => {
    props.navigation.navigate(AppRoute.GUEST_DRUG_LIST, { creatureId: item?.id as string })
  }
  
  const onDrugAllergyIconPress = (): void => {
    props.navigation.navigate(AppRoute.GUEST_DRUG_ALLERGY_LIST, { creatureId: item?.id as string })
  }

  const onFoodAllergyIconPress = (): void => {
    props.navigation.navigate(AppRoute.GUEST_FOOD_ALLERGY_LIST, { creatureId: item?.id as string })
  }

  const onMedicalImagePress = (): void => {
    props.navigation.navigate(AppRoute.GUEST_MEDICAL_INFO, { creatureId: item?.id as string })
  }

  const onTreatmentImagePress = (): void => {
    props.navigation.navigate(AppRoute.GUEST_TREATMENT_HISTORY_LIST, { creatureId: item?.id as string })
  }

  const onVaccineImagePress = (): void => {
    props.navigation.navigate(AppRoute.GUEST_VACCINE_BOOK_LIST, { creatureId: item?.id as string })
  }

  const onParentImagePress = (): void => {
    props.navigation.navigate(AppRoute.GUEST_PARENT_INFO, { parentId: item?.memberId as string })
  }

  const onLocationImagePress = (): void => {
    props.navigation.navigate(AppRoute.GUEST_LOCATION_INFO, { parentId: item?.memberId as string })
  }
  
  const navigateBack = (): void => {
    if (prevState === AppRoute.HOME) setState(AppRoute.HOME)
    else if (prevState === AppRoute.ENTRANCE) setState(AppRoute.ENTRANCE)
  };
  
  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title='Creature Info'
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
        />
      <Divider />
      <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <ProfileAvatarPreview
          style={[ styles.profileAvatar ]}
          source={item?.creatureImage}
          thumbnail={item?.creatureImageThumbnail}
          resizeMode={FastImage.resizeMode.cover}
        />
        <ProfileSetting
          style={[styles.profileSetting, styles.section]}
          hint='First Name'
          value={item?.firstName}
        />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Last Name'
          value={item?.lastName}
        />
        <PhotoChooserBoxPreview
          hint='Birth Certificate'
          source={item?.birthCertificate}
          thumbnail={item?.birthCertificateThumbnail}
          resizeMode={FastImage.resizeMode.cover} />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Gender'
          value={item?.genderName}
        />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Blood Type'
          value={item?.bloodTypeName}
        />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Birth Date'
          value={moment(item?.birthDate).format('DD/MM/YYYY')}
        />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Weight'
          value={item?.weight}
        />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Height'
          value={item?.height}
        />
        <ProfileIconSetting
          style={styles.profileSetting}
          hint='Drug'
          icon={MedicineBoxIcon}
          onPress={onDrugIconPress}
        />
        <ProfileIconSetting
          style={styles.profileSetting}
          hint='Drug allergy'
          icon={MedicineBoxIcon}
          onPress={onDrugAllergyIconPress}
        />
        <ProfileIconSetting
          style={styles.profileSetting}
          hint='Food allergy'
          icon={FoodIcon}
          onPress={onFoodAllergyIconPress}
        />
        <Layout style={styles.imageButtonContainer}>
          <TouchableWithoutFeedback onPress={onMedicalImagePress} >
            <Image
              resizeMode='contain'
              source={require('../../assets/images/medical.png')}
              style={styles.imageButton} />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={onTreatmentImagePress} >
            <Image
              resizeMode='contain'
              source={require('../../assets/images/treatment.png')}
              style={styles.imageButton} />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={onVaccineImagePress} >
            <Image
              resizeMode='contain'
              source={require('../../assets/images/vaccine.png')}
              style={styles.imageButton} />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={onParentImagePress} >
            <Image
              resizeMode='contain'
              source={require('../../assets/images/parent.png')}
              style={styles.imageButton} />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={onLocationImagePress} >
            <Image
              resizeMode='contain'
              source={require('../../assets/images/location.png')}
              style={styles.imageButton} />
          </TouchableWithoutFeedback>
        </Layout>
      </ScrollView>
    </SafeAreaLayout>
  )

}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
    paddingTop: 24
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  contentContainer: {
    paddingBottom: 24,
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  profileSetting: {
    padding: 16,
    backgroundColor: MainTheme.colorPrimary
  },
  section: {
    marginTop: 24,
  },
  imageButtonContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    marginVertical: 15,
    alignItems: 'center', 
    backgroundColor: MainTheme.colorPrimary 
  },
  imageButton: {
    aspectRatio: 1.0,
    width: Dimensions.get('window').width / 5 - 24
  },
  editContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  editButton: {
    flexDirection: 'row-reverse',
    paddingHorizontal: 5,
  }
})