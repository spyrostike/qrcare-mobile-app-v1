import {
  HomeCampaignIcon,
  HomeCustomerIcon,
  HomeChartIcon,
  HomeLogoutIcon,
  HomeOrderIcon,
  HomeReportIcon,
  HomeWarehouseIcon,
  CartIcon,
  PaymentIcon,
  SendBackIcon,
  VisitCustomerIcon,
  CheckIcon,
  CheckInIcon,
  MileIcon,
  BillIcon
} from '../../assets/icons';
import { MenuItem } from '../../model/menu-item.model';
import { CreatureItem } from '../../model/creature-item.model';

export interface LayoutData extends MenuItem {
  route: string;
}

export const data: LayoutData[] = [
  {
    title: 'ทำรายการ',
    route: 'Order',
    icon: HomeOrderIcon
  },
  {
    title: 'คลังสินค้า',
    route: 'Warehouse',
    icon: HomeWarehouseIcon
  },
  {
    title: 'สรุปงาน',
    route: 'Report',
    icon: HomeReportIcon
  },
  {
    title: 'แสดงยอดขาย',
    route: 'SalesReport',
    icon: HomeChartIcon
  },
  {
    title: 'ลูกค้า',
    route: 'Customer',
    icon: HomeCustomerIcon
  },
  {
    title: 'แคมเปญ',
    route: 'Campaign',
    icon: HomeCampaignIcon
  },
  {
    title: 'ออกจากระบบ',
    route: 'Logout',
    icon: HomeLogoutIcon
  }
];

export const OrderMenuItems: LayoutData[] = [
  {
    title: 'จองสินค้า',
    route: 'Order',
    icon: CartIcon
  },
  {
    title: 'ขายสินค้า',
    route: 'Warehouse',
    icon: PaymentIcon
  },
  {
    title: 'คืนสินค้า',
    route: 'Report',
    icon: SendBackIcon
  },
  {
    title: 'ตรวจนับสินค้า',
    route: 'SalesReport',
    icon: HomeWarehouseIcon
  },
  {
    title: 'เยี่ยม',
    route: 'Customer',
    icon: VisitCustomerIcon
  },
  {
    title: 'สำรวจ',
    route: 'Campaign',
    icon: CheckIcon
  },
  {
    title: 'เช็คอิน',
    route: 'Logout',
    icon: CheckInIcon
  },
  {
    title: 'บันทึกเลขไมล์',
    route: 'Logout',
    icon: MileIcon
  },
  {
    title: 'บิลเอกสาร',
    route: 'Logout',
    icon: BillIcon
  }
];

export const MockCreatureItems: CreatureItem[] = [
  {
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },{
    id: '1',
    name: 'ดช นาย ตำป่า'
  },
]
