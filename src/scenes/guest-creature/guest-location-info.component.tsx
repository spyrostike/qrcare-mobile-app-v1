import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Divider, Layout, Modal, Spinner, Text } from '@ui-kitten/components';
import FastImage from 'react-native-fast-image';
import { GuestLocationInfoScreenProps } from '../../navigation/guest-creature.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { ConfigData, ConfigSchema } from '../../data/config.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { Map } from '../../components/map.component';
import { useStoreActions } from '../../store';
import { MemberProps } from '../../store/models/member';
import { GeoCoordinates } from 'react-native-geolocation-service';
import { PhotoChooserBoxPreview } from '../../components/photo-chooser-box-preview.component';
import { ProfileSetting } from '../../components/profile-setting.component';

export const GuestLocationInfoScreen = (props: GuestLocationInfoScreenProps): SafeAreaLayoutElement => {
  const parentId = props.route.params.parentId

  const [item, setItem] = React.useState<MemberProps | null>(null);
  const [location, setLocation] = React.useState<GeoCoordinates | null>(null);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const getInfoById = useStoreActions(a => a.member.getInfoById)

  React.useMemo(() => {
    const getParentInfo = async () => {
      try {
        setIsLoading(true)
        const res = await getInfoById(parentId)
        setIsLoading(false)
        setItem(res)
        setLocation({ 
          latitude: res?.placeLatitude, 
          longitude: res?.placeLongitude
        } as GeoCoordinates)
      } catch (error) {
        setIsLoading(false)
        setErrorMessage(error)
      }
    }
    getParentInfo()
  }, [])

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const renderForm = (props: FormikProps<ConfigData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <Map location={location} previewOnly={true} />
        <PhotoChooserBoxPreview
          hint='Location Image'
          source={item?.locationImage}
          thumbnail={item?.locationImageThumbnail}
          resizeMode={FastImage.resizeMode.cover}
          containerStyle={{ padding: 0 }}
          textStyle={{ fontSize: 12 }}
        />
        <Layout style={styles.descriptionContainer} level='1'>
          <Text
            appearance='hint'
            category='s1'
            style={{ marginBottom: 8 }} >
            address
          </Text>
          <Text style={{ color: MainTheme.colorSecondary }}>
            { item?.address1 }
          </Text>
        </Layout>
      </Layout>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title='Location Info'
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={BackIcon}
        onBackPress={navigateBack}
        style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
        <Formik
          initialValues={ConfigData.empty()}
          validationSchema={ConfigSchema}
          onSubmit={() => {}}>
          {renderForm}
        </Formik>
      </KeyboardAvoidingView>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    marginHorizontal: 24,
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    justifyContent: 'center',
    height: 250
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
  mapContainer: {
    height: 250,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  map: {
    height: 250,
    backgroundColor: MainTheme.colorSecondary,
  },
  mapButton: {
    backgroundColor: MainTheme.buttonColor,
    height: 50,
    borderWidth: 0
    
  },
  mapItemFooter: {
    height: 50,
    marginVertical: -30,
    marginHorizontal: -24,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  },
  profileSetting: {
    padding: 16,
    backgroundColor: MainTheme.colorPrimary
  },
  descriptionContainer: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,  
    marginTop: 5,
  }
});