import React from 'react';
import { Dimensions, ListRenderItemInfo, StyleSheet, TouchableHighlight, View } from 'react-native';
import { 
  Divider, 
  Input, 
  ListItemElement, 
  Modal,
  Text
} from '@ui-kitten/components';
import { GuestVaccineBookListScreenProps } from '../../navigation/guest-creature.navigator';
import { Toolbar } from '../../components/toolbar.component';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset
} from '../../components/safe-area-layout.component';
import { BackIcon, SearchIcon } from '../../assets/icons';
import { ImageItemText } from '../../components/image-text-item.component';
import { MenuGridList } from '../../components/menu-grid-list.component';
import { AppRoute } from '../../navigation/app-routes';
import { MainTheme } from '../../constants/LOV';
import { useStoreActions, useStoreState } from '../../store';
import { CriteriaSearch } from '../../model/criteria-search.model';
import { VaccineBookProps } from '../../store/models/vaccine-book';

export const GuestVaccineBookListScreen = (props: GuestVaccineBookListScreenProps): SafeAreaLayoutElement => {
  const creatureId = props.route.params.creatureId

  const [textSearch, setTextSearch] = React.useState<string>('');
  const [offset, setOffset] = React.useState<number>(0);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const find = useStoreActions(a => a.vaccineBook.find)
  const setItems = useStoreActions(a => a.vaccineBook.setItems)
  const items = useStoreState(s => s.vaccineBook.items)

  const getVaccineBookItems = async (criteria) => {
    try {
      setIsLoading(true)
      const res = await find(criteria as CriteriaSearch)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
    }
  }

  React.useMemo(() => {
    setItems([])
    getVaccineBookItems({ creatureId: creatureId, textSearch: textSearch, offset: offset })
  }, [])

  const onSearchIconPress = (): void => {
    setItems([])
    getVaccineBookItems({ creatureId: creatureId, textSearch: textSearch, offset: 0 })
    setOffset(0)
  }

  const onScroll = async () => {
    try {
      setIsLoading(true)
      await find({ creatureId: creatureId, textSearch: textSearch, offset: offset + 1 } as CriteriaSearch)
      setOffset(offset + 1)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
    }
  }

  const onRefresh = (): void => {
    setItems([])
    getVaccineBookItems({ creatureId: creatureId, textSearch: textSearch, offset: 0 })
    setOffset(0)
  }

  const onItemPress = (index: number): void => {
    const id: string = typeof items[index]?.id !== 'undefined' && items[index]?.id !== null ? items[index].id as string : ''
    props.navigation.navigate(AppRoute.GUEST_VACCINE_BOOK_INFO, { vaccineBookId: id })
  };

  const renderItem = (info: ListRenderItemInfo<VaccineBookProps>): ListItemElement => (
    <TouchableHighlight 
      style={styles.item} 
      onPress={() => onItemPress(info.index)}
    >
      <ImageItemText
        index={info.index}
        style={{ flex: 1, justifyContent: 'center' }}
        source={require('../../assets/images/square_button.png')}
      >
        <View style={styles.absoluteContainer}>
          <Text
            style={styles.itemTitle}
            category='s2'>
            {info.item.vaccineType === 14 ? info.item.vaccineTypeOther : info.item.vaccineTypeName}
          </Text>
        </View>
        
      </ImageItemText>
    </TouchableHighlight>
  );

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title='Vaccine Book List'
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={BackIcon}
        onBackPress={props.navigation.goBack}
        style={styles.toolbar}
      />
      <Divider/>
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', paddingHorizontal: 8, paddingVertical: 16 }} >
          <Input
            style={styles.searchInput}
            placeholder='Search...'
            value={textSearch}
            onChangeText={setTextSearch}
            onIconPress={onSearchIconPress}
            icon={SearchIcon}
          />
        </View>
        <Divider/>
        <MenuGridList
          data={items}
          renderItem={renderItem}
          style={{ backgroundColor: MainTheme.colorPrimary }}
          refreshing={isLoading}
          onRefresh={onRefresh}
          onScroll={onScroll}
        />
      </View>
    </SafeAreaLayout>
  )
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    aspectRatio: 1.0,
    margin: 8,
    maxWidth: Dimensions.get('window').width / 2 - 24,
    borderColor: MainTheme.itemBackGround
  },
  itemTitle: {
    alignSelf: 'center',
    color: MainTheme.colorSecondary
  },
  searchInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  scanButton: { 
    paddingHorizontal: 0,
  },
  iconButton: {
    width: 24,
    height: 24,
    backgroundColor: MainTheme.buttonColor,
    borderColor: MainTheme.buttonColor
  },
  messageInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  absoluteContainer: { 
    flex: 1, 
    position: 'absolute', 
    justifyContent: 'center', 
    alignItems: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  }
});
