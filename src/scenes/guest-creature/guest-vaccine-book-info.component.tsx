import React from 'react';
import {
   Dimensions, 
   ListRenderItemInfo, 
   StyleSheet, 
   TouchableHighlight, 
   View 
} from 'react-native';
import { 
  Divider, 
  Input,
  ListItemElement, 
  LayoutElement, 
  Modal,
  Spinner,
  Text
} from '@ui-kitten/components';
import moment from 'moment';
import {
  SafeAreaLayout,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { GuestVaccineBookInfoScreenProps } from '../../navigation/guest-creature.navigator';
import { AppRoute } from '../../navigation/app-routes';
import { Toolbar } from '../../components/toolbar.component';
import { MainTheme } from '../../constants/LOV';
import { BackIcon, SearchIcon } from '../../assets/icons';
import { ProfileSetting } from '../../components/profile-setting.component';
import { MenuGridList } from '../../components/menu-grid-list.component';
import { useStoreActions, useStoreState } from '../../store';
import { VaccineBookProps } from '../../store/models/vaccine-book';
import { VaccineProps } from '../../store/models/vaccine';
import { CriteriaSearch } from '../../model/criteria-search.model';
import { ImageItemText } from '../../components/image-text-item.component';

export const GuestVaccineBookInfoScreen = (props: GuestVaccineBookInfoScreenProps): LayoutElement => {
  const vaccineBookId = props.route.params.vaccineBookId
  
  const [textSearch, setTextSearch] = React.useState<string>('');
  const [offset, setOffset] = React.useState<number>(0);
  const [refreshing, setRefreshing] = React.useState<boolean>(false);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const getInfo = useStoreActions(a => a.vaccineBook.getInfo)
  const setItem = useStoreActions(a => a.vaccineBook.setItem)
  const findVaccines = useStoreActions(a => a.vaccine.find)
  const setVaccineItems = useStoreActions(a => a.vaccine.setItems)
  const item = useStoreState(s => s.vaccineBook.item)
  const vaccineItems = useStoreState(s => s.vaccine.items)

  const getVaccineItems = async (criteria) => {
    try {
      setRefreshing(true)
      const res = await findVaccines(criteria as CriteriaSearch)
      setRefreshing(false)
    } catch (error) {
      setRefreshing(false)
    }
  }

  const getVaccineBookInfo = async () => {
    try {
      setItem({} as VaccineBookProps)
      setIsLoading(true)
      const res =  await getInfo(vaccineBookId)
      setIsLoading(false)
      setItem(res)
      res && getVaccineItems({ vaccineBookId: vaccineBookId, textSearch: textSearch, offset: offset })
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  React.useMemo(() => {
    setVaccineItems([])
    getVaccineBookInfo()
  }, [])

  const onSearchIconPress = (): void => {
    setVaccineItems([])
    getVaccineItems({ vaccineBookId: vaccineBookId, textSearch: textSearch, offset: 0 })
    setOffset(0)
  }
  
  const onItemPress = (index: number): void => {
    const id: string = typeof vaccineItems[index]?.id !== 'undefined' && vaccineItems[index]?.id !== null ? vaccineItems[index].id as string : ''
    props.navigation.navigate(AppRoute.GUEST_VACCINE_INFO, { vaccineId: id })
  };

  const onScroll = async () => {
    try {
      setRefreshing(true)
      await findVaccines({ vaccineBookId: vaccineBookId, textSearch: textSearch, offset: offset + 1 } as CriteriaSearch)
      setOffset(offset + 1)
      setRefreshing(false)
    } catch (error) {
      setRefreshing(false)
    }
  }

  const onRefresh = (): void => {
    setVaccineItems([])
    getVaccineItems({ vaccineBookId: vaccineBookId, textSearch: textSearch, offset: 0 })
    setOffset(0)
  }

  const renderItem = (info: ListRenderItemInfo<VaccineProps>): ListItemElement => (
    <TouchableHighlight 
      style={styles.item} 
      onPress={() => onItemPress(info.index)}
    >
      <ImageItemText
        index={info.index}
        style={{ flex: 1, justifyContent: 'center' }}
        source={require('../../assets/images/square_button.png')}
      >
          <View style={styles.absoluteContainer}>
            <Text
              style={styles.itemTitle}
              category='s2'>
              {info.item.hospitalName}
            </Text>
            <Text
              style={styles.itemTitle}
              category='s2'>
              {moment(info.item.date).format('DD/MM/YYYY')}
            </Text>
          </View>
        
      </ImageItemText>
    </TouchableHighlight>
  );

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title='Vaccine Book Info'
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={props.navigation.goBack}
          style={styles.toolbar}
        />
      <Divider />
      <ProfileSetting
        style={styles.profileSetting}
        hint='Vaccine'
        value={item?.vaccineTypeName}
      />
      {
        item?.vaccineType === 14 &&
        <ProfileSetting
          style={styles.profileSetting}
          hint='Vaccine Other'
          value={item?.vaccineTypeOther}
        />
      }
      <View style={{ flexDirection: 'row', paddingVertical: 16, padding: 8 }} >
        <Input
          style={styles.searchInput}
          placeholder='Search...'
          onChangeText={setTextSearch}
          onIconPress={onSearchIconPress}
          icon={SearchIcon}
        />
      </View>
      <MenuGridList
        data={vaccineItems}
        renderItem={renderItem}
        style={{ backgroundColor: MainTheme.colorPrimary }}
        refreshing={refreshing}
        onRefresh={onRefresh}
        onScroll={onScroll}
      />
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  )

}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  contentContainer: {
    paddingBottom: 24,
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  profileSetting: {
    padding: 16,
    backgroundColor: MainTheme.colorPrimary
  },
  section: {
    marginTop: 24,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  imageButtonContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    marginTop: 15,
    alignItems: 'center', 
    backgroundColor: MainTheme.colorPrimary 
  },
  imageButton: {
    aspectRatio: 1.0,
    width: Dimensions.get('window').width / 3 - 24
  },
  editContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  editButton: {
    flexDirection: 'row-reverse',
    paddingHorizontal: 5,
  },
  descriptionContainer: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,  
    padding: 16,
  },
  itemTitle: {
    alignSelf: 'center',
    color: MainTheme.colorSecondary
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    aspectRatio: 1.0,
    margin: 8,
    maxWidth: Dimensions.get('window').width / 2 - 24,
    borderColor: MainTheme.itemBackGround
  },
  absoluteContainer: { 
    flex: 1, 
    position: 'absolute', 
    justifyContent: 'center', 
    alignItems: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  },
  searchInput: {
    flex: 1,
    marginHorizontal: 8,
  }
})