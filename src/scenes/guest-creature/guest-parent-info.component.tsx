import React from 'react';
import { Linking, StyleSheet, View } from 'react-native';
import { Button, Divider, Layout, Modal, Spinner, Text } from '@ui-kitten/components';
import FastImage from 'react-native-fast-image';
import moment from 'moment';
import { GuestParentInfoScreenProps } from '../../navigation/guest-creature.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { BackIcon, CameraIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { ProfileSetting } from '../../components/profile-setting.component';
import { useStoreActions } from '../../store';
import { ProfileAvatarPreview } from '../../components/profile-avatar-preview.component';
import { MemberProps } from '../../store/models/member';

export const GuestParentInfoScreen = (props: GuestParentInfoScreenProps): SafeAreaLayoutElement => {
  const parentId = props.route.params.parentId

  const [item, setItem] = React.useState<MemberProps | null>(null);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const getInfoById = useStoreActions(a => a.member.getInfoById)

  React.useMemo(() => {
    const getParentInfo = async () => {
      try {
        setIsLoading(true)
        const res = await getInfoById(parentId)
        setIsLoading(false)
        setItem(res)
      } catch (error) {
        setIsLoading(false)
        setErrorMessage(error)
      }
    }
    getParentInfo()
  }, [])

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const renderPhotoButton = (): React.ReactElement => (
    <Button
      style={styles.editAvatarButton}
      status='basic'
      icon={CameraIcon}
    />
  )

  const renderForm = (): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <ProfileAvatarPreview
          style={[ styles.profileAvatar ]}
          source={item?.profileImage}
          thumbnail={item?.profileImage}
          resizeMode={FastImage.resizeMode.cover}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='First name'
          value={item?.firstName}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Last name'
          value={item?.lastName}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Email'
          value={item?.email}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Gender'
          value={item?.genderName}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Blood type'
          value={item?.bloodTypeName}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Birth Date'
          value={moment(item?.birthDate).format('DD/MM/YYYY')}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Relationship'
          value={item?.relationship}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='ID Card Number'
          value={item?.identificationNo}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Contact Number'
          value={item?.contactNo1}
          onPress={() => Linking.openURL(`tel:${item?.contactNo1}`)}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Contact Number'
          value={item?.contactNo2}
          onPress={() => Linking.openURL(`tel:${item?.contactNo1}`)}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Address'
          value={item?.address1}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Address'
          value={item?.address2}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Address'
          value={item?.address3}
        />
      </Layout>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title='Parent Info'
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={BackIcon}
        onBackPress={navigateBack}
        style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}> 
        {renderForm()}
      </KeyboardAvoidingView>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
  },
  form: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    marginHorizontal: 24,
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    flex: 1,
    justifyContent: 'center',
    // aspectRatio: 1.0,
    height: 150
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
  mapContainer: {
    flex: 1,
    // aspectRatio: 1.0,
    height: 180,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  profileSetting: {
    padding: 16,
    backgroundColor: MainTheme.colorPrimary
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
});