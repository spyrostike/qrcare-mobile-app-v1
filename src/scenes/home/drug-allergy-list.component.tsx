import React from 'react';
import { Dimensions, ListRenderItemInfo, StyleSheet, TouchableHighlight, View } from 'react-native';
import { 
  Divider, 
  Input, 
  ListItemElement, 
  Modal,
  Text,
  TopNavigationAction, 
  TopNavigationActionElement  
} from '@ui-kitten/components';
import { DrugAllergyListScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { BackIcon, PlusIcon, SearchIcon, TrashIcon } from '../../assets/icons';
import { MenuGridList } from '../../components/menu-grid-list.component';
import { AppRoute } from '../../navigation/app-routes';
import { MainTheme } from '../../constants/LOV';
import { ImageItemText } from '../../components/image-text-item.component';
import { useStoreActions, useStoreState } from '../../store';
import { CriteriaSearch } from '../../model/criteria-search.model';
import { DrugAllergyProps } from '../../store/models/drug-allergy';

export const DrugAllergyListScreen = (props: DrugAllergyListScreenProps): SafeAreaLayoutElement => {
  const creatureId = props.route.params.creatureId
  let checkItems: string[] = []

  const [textSearch, setTextSearch] = React.useState<string>('');
  const [offset, setOffset] = React.useState<number>(0);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [checkboxVisabled, setCheckboxVisabled] = React.useState<boolean>(false);

  const find = useStoreActions(a => a.drugAllergy.find)
  const setItems = useStoreActions(a => a.drugAllergy.setItems)
  const removeItems = useStoreActions(a => a.drugAllergy.removeItems)
  const items = useStoreState(s => s.drugAllergy.items)

  const getDrugAllergyItems = async (criteria) => {
    try {
      setIsLoading(true)
      const res = await find(criteria as CriteriaSearch)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
    }
  }

  React.useMemo(() => {
    setItems([])
    getDrugAllergyItems({ creatureId: creatureId, textSearch: textSearch, offset: offset })
  }, [])

  const onSearchIconPress = (): void => {
    setCheckboxVisabled(false)
    setItems([])
    getDrugAllergyItems({ creatureId: creatureId, textSearch: textSearch, offset: 0 })
    setOffset(0)
  }

  const onScroll = async () => {
    try {
      setIsLoading(true)
      await find({ creatureId: creatureId, textSearch: textSearch, offset: offset + 1 } as CriteriaSearch)
      setOffset(offset + 1)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
    }
  }

  const onRefresh = (): void => {
    setCheckboxVisabled(false)
    setItems([])
    getDrugAllergyItems({ creatureId: creatureId, textSearch: textSearch, offset: 0 })
    setOffset(0)
  }

  const onPlusButtonPress = (): void => {
    if (checkboxVisabled === true) return
    props.navigation.navigate(AppRoute.DRUG_ALLERGY_FORM, { creatureId: creatureId, action: 'add' })
  }

  const onTrushButtonPress = async () => {
    try {
      setCheckboxVisabled(false)
      setIsLoading(true)
      await removeItems(checkItems)
      checkItems = []
      setIsLoading(false)
      onRefresh()
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  const onItemPress = (index: number): void => {
    const id: string = typeof items[index]?.id !== 'undefined' && items[index]?.id !== null ? items[index].id as string : ''
    if (id !== '') props.navigation.navigate(AppRoute.DRUG_ALLERGY_INFO, { drugAllergyId: id })
  }

  const onCheckChange = (checked: boolean, index: number): void => {
    if (checked) { checkItems.push(items[index].id) } 
    else {checkItems = checkItems.filter(item => item !== items[index].id)}
  }

  const plusIconRender: TopNavigationActionElement = (
    <TopNavigationAction
      icon={PlusIcon}
      onPress={onPlusButtonPress}
    />
  )

  const trashIconRender: TopNavigationActionElement = (
    <TopNavigationAction
      icon={TrashIcon}
      onPress={onTrushButtonPress}
    />
  )

  let rightControls: TopNavigationActionElement[] = [
    plusIconRender,
    trashIconRender
  ]

  if (checkboxVisabled === false) rightControls = rightControls.slice(0, 1)

  const renderItem = (info: ListRenderItemInfo<DrugAllergyProps>): ListItemElement => (
    <TouchableHighlight 
      style={styles.item} 
      onPress={() => onItemPress(info.index)}
      onLongPress={() => setCheckboxVisabled(true)} 
    >
      <ImageItemText
        index={info.index}
        style={{ flex: 1, justifyContent: 'center' }}
        source={require('../../assets/images/square_button.png')}
        checkboxVisabled={checkboxVisabled}
        onCheckChange={onCheckChange}>
          <View style={styles.absoluteContainer}>
            <Text
              style={styles.itemTitle}
              category='s2'>
              {info.item.name}
            </Text>
          </View>
        
      </ImageItemText>
    </TouchableHighlight>
  );

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title='Drug Allergy'
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={BackIcon}
        onBackPress={props.navigation.goBack}
        rightControls={rightControls}
        style={styles.toolbar}
      />
      <Divider/>
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', paddingHorizontal: 8, paddingVertical: 16 }} >
          <Input
            style={styles.searchInput}
            placeholder='Search...'
            value={textSearch}
            onChangeText={setTextSearch}
            onIconPress={onSearchIconPress}
            icon={SearchIcon}
          />
        </View>
        <Divider/>
        <MenuGridList
          data={items}
          renderItem={renderItem}
          onItemPress={onItemPress}
          style={{ backgroundColor: MainTheme.colorPrimary }}
          refreshing={isLoading}
          onRefresh={onRefresh}
          onScroll={onScroll}
        />
      </View>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  )
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    aspectRatio: 1.0,
    margin: 8,
    maxWidth: Dimensions.get('window').width / 2 - 24,
    borderColor: MainTheme.itemBackGround
  },
  itemTitle: {
    alignSelf: 'center',
    color: MainTheme.colorSecondary
  },
  searchInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  scanButton: { 
    paddingHorizontal: 0,
  },
  iconButton: {
    width: 24,
    height: 24,
    backgroundColor: MainTheme.buttonColor,
    borderColor: MainTheme.buttonColor
  },
  plusButton: {
    borderRadius: 24,
    marginHorizontal: 8,
  },
  messageInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  absoluteContainer: { 
    flex: 1, 
    position: 'absolute', 
    justifyContent: 'center', 
    alignItems: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  }
});
