import React from 'react';
import { StyleSheet, View, ViewProps } from 'react-native';
import { Drawer, DrawerElement, MenuItemType, Text } from '@ui-kitten/components';
import { DrawerHomeScreenProps } from '../../navigation/home.navigator';
import { LogoApp } from '../../components/logo-app.component'
import { APP_VERSION_FULL, MainTheme } from '../../constants/LOV';
import { AccountCardDetailsIcon, LocationOnIcon, KeyIcon, PowerOffIcon } from '../../assets/icons';
import { AppRoute } from '../../navigation/app-routes';
import { useStoreActions } from '../../store';

const DrawerHeader = (): React.ReactElement<ViewProps> => (
  <View style={styles.header} >
    <LogoApp style={{  width: 100, height: 100 }} />
    <Text style={styles.textLabel} status='control'>{ APP_VERSION_FULL }</Text>
  </View>
);

export const HomeDrawer = (props: DrawerHomeScreenProps): DrawerElement => {
  const setToken = useStoreActions(a => a.member.setToken)
  const setItem = useStoreActions(a => a.member.setItem)
  const setState = useStoreActions(a => a.app.setState)

  const DATA: MenuItemType[] = [
    { title: 'Profile', icon: AccountCardDetailsIcon },
    // { title: 'Location', icon: LocationOnIcon },
    { title: 'Change Password', icon: KeyIcon },
    { title: 'Logout', icon: PowerOffIcon }
  ];

  const onMenuItemSelect = (index: number): void => {
    switch (index) {
      case 0: {
        props.navigation.toggleDrawer();
        props.navigation.navigate(AppRoute.PROFILE);
        return;
      }
      // case 1: {
      //   props.navigation.toggleDrawer();
      //   props.navigation.navigate(AppRoute.LOCATION);
      //   return;
      // }
      case 1: {
        props.navigation.toggleDrawer();
        props.navigation.navigate(AppRoute.CHANGE_PASSWORD);
        return;
      }
      case 2: {
        setToken(null);
        setItem(null);
        setState(AppRoute.LOADING);

        return;
      }

    }
  };

  return (
    <Drawer
      header={DrawerHeader}
      data={DATA}
      onSelect={onMenuItemSelect}
    />
  );
};

const styles = StyleSheet.create({
  header: {
    height: 160,
    backgroundColor: MainTheme.colorPrimary,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textLabel: {
    zIndex: 1,
    alignSelf: 'center'
  }
});
