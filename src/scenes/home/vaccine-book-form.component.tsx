import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Divider, Icon, Input, Layout, Select, SelectOptionType, Spinner, Text } from '@ui-kitten/components';
import { VaccineBookFormScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { ConfigData, ConfigSchema } from '../../data/config.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { useStoreActions, useStoreState } from '../../store';
import { VaccineBookProps } from 'src/store/models/vaccine-book';

export const VaccineBookFormScreen = (props: VaccineBookFormScreenProps): SafeAreaLayoutElement => {
  const action = props.route.params.action
  const title = `Vaccine Book Form - ${ action.charAt(0).toUpperCase() + action.slice(1) }` 
  const creatureId = props.route.params.creatureId
  const vaccineBookId = props.route.params?.vaccineBookId

  const vaccineTypeOptions = [
    { text: '-- Select --', value: null, disabled: true },
    { text: 'BCG', value: 1 },
    { text: 'HBV', value: 2 },
    { text: 'Dtap, Tdap', value: 3 },
    { text: 'IPV', value: 4 },
    { text: 'Live JE', value: 5 },
    { text: 'Hib', value: 6 },
    { text: 'HAV', value: 7 },
    { text: 'VZV or MMRV', value: 8 },
    { text: 'Influenza', value: 9 },
    { text: 'PCV', value: 10 },
    { text: 'Rota', value: 11 },
    { text: 'HPV', value: 12 },
    { text: 'DEN', value: 13 },
    { text: 'Other', value: 14 }
  ]

  const [vaccineTypeSelected, setVaccineTypeSelected] = React.useState<SelectOptionType>(vaccineTypeOptions[0]);
  const [vaccineTypeId, setVaccineTypeId] = React.useState<number | null>(null);
  const [vaccineTypeOther, setVaccineTypeOther] = React.useState<string>('');
  const [errorVaccineTypeMessage, setErrorVaccineTypeMessage] = React.useState<string>('');
  const [errorVaccineTypeOtherMessage, setErrorVaccineTypeOtherMessage] = React.useState<string>('');
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const add = useStoreActions(a => a.vaccineBook.add);
  const edit = useStoreActions(a => a.vaccineBook.edit);
  const setItem = useStoreActions(a => a.vaccineBook.setItem);
  const item = useStoreState(s => s.vaccineBook.item);

  React.useMemo(() => {
    if (action === 'edit') {
      const vaccineTypeId = item?.vaccineType !== undefined ? item?.vaccineType : 0

      setVaccineTypeId(vaccineTypeId)
      setVaccineTypeSelected(vaccineTypeOptions[vaccineTypeId])
      setVaccineTypeOther(item?.vaccineTypeOther as string)
    } else {
      setItem({} as VaccineBookProps)
    }
  }, [])

  const onFormSubmit = (values: ConfigData): void => { };

  const onSaveButtonPress = async () => {
    try{
      const vaccineType: any = vaccineTypeSelected
      let validate = true

      setErrorVaccineTypeMessage('')
      setErrorVaccineTypeOtherMessage('')
      setErrorMessage('')

      if (vaccineType.value === null) { setErrorVaccineTypeMessage('Vaccine is required.'); validate = false }
      if (vaccineType.value === 14 && vaccineTypeOther === '') { setErrorVaccineTypeOtherMessage('Vaccine Other is required.'); validate = false }
      if (!validate) return 

      const data = {
        id: vaccineBookId,
        vaccineType: vaccineType.value as any,
        other: vaccineTypeOther,
        creatureId: creatureId
      } 
      
      setIsLoading(true)
      if (action === 'add') await add(data as VaccineBookProps)
      else if (action === 'edit') await edit(data as VaccineBookProps)
      setIsLoading(false)
      navigateBack()
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  const onVaccineTypeSelect = (option): void => {
    setVaccineTypeId(option.value)
    setVaccineTypeOther('')
    setErrorVaccineTypeOtherMessage('')
    setVaccineTypeSelected(option)
  }

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const renderForm = (props: FormikProps<ConfigData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <Select
          style={styles.input}
          label='Vaccine'
          data={vaccineTypeOptions}
          selectedOption={vaccineTypeSelected}
          onSelect={onVaccineTypeSelect}
        />
        {
          errorVaccineTypeMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorVaccineTypeMessage }</Text>
          </View>
        }
        {
          vaccineTypeId === 14  &&
          <Input
            style={styles.input}
            label='Vaccine Other'
            value={vaccineTypeOther}
            onChangeText={setVaccineTypeOther}
          />
        }
        {
          errorVaccineTypeOtherMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorVaccineTypeOtherMessage }</Text>
          </View>
        }
        {
          errorMessage !== '' &&
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
              <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorMessage }</Text>
            </View>
          }
          {
            isLoading &&
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Spinner/>
            </View>
          }
      </Layout>
      <Button
        style={styles.saveButton}
        onPress={onSaveButtonPress}>
        Save
      </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title={title}
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
          <Formik
            initialValues={ConfigData.empty()}
            validationSchema={ConfigSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
      </KeyboardAvoidingView>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  input: {
    marginVertical: 8,
  },
  saveButton: {
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  }
});