import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Divider, Icon, Layout, Spinner, Text } from '@ui-kitten/components';
import { FoodAllergyFormScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { FoodAllergyFormData, FoodAllergyFormSchema } from '../../data/food-allergy-form.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { PhotoChooser } from '../../components/photo-chooser.component';
import { useStoreActions, useStoreState } from '../../store';
import { FileProps } from '../../store/models/file';
import { FormInput } from '../../components/form-input.component';
import { FoodAllergyProps } from '../../store/models/food-allergy';

export const FoodAllergyFormScreen = (props: FoodAllergyFormScreenProps): SafeAreaLayoutElement => {
  const action = props.route.params.action
  const title = `Food Allergy Form - ${ action.charAt(0).toUpperCase() + action.slice(1) }` 
  const creatureId = props.route.params?.creatureId ? props.route.params.creatureId : ''
  const foodAllergyId = props.route.params.foodAllergyId as string;

  const [image, setImage] = React.useState<FileProps>();
  const [imageThumbnail, setImageThumbnail] = React.useState<FileProps>();
  const [oldImageId, setOldImageId] = React.useState<string>('');
  const [oldImageThumbnailId, setOldImageThumbnailId] = React.useState<string>('');
  const [isImageChange, setIsImageChange] = React.useState<boolean>(false)
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorImageMessage, setErrorImageMessage] = React.useState<string>('');

  const add = useStoreActions(a => a.foodAllergy.add)
  const edit = useStoreActions(a => a.foodAllergy.edit)
  const setItem = useStoreActions(a => a.foodAllergy.setItem)
  const item = useStoreState(s => s.foodAllergy.item)

  React.useMemo(() => {
    if (action === 'edit') {
      setImage({ id: item?.imageId ? item?.imageId : null } as FileProps)
      setImageThumbnail({ id: item?.imageThumbnailId ? item?.imageThumbnailId : null } as FileProps)
      setOldImageId(item?.imageId as string)
      setOldImageThumbnailId(item?.imageThumbnailId as string)
    } else {
      setItem({} as FoodAllergyProps)
    }
  }, [])
  
  const onFormSubmit = async (values: FoodAllergyFormData) => {
    try {
      let validate = true
      setErrorImageMessage('')
      setErrorMessage('')
      
      if (typeof image?.id === 'undefined' || image?.id === null) { 
        setErrorImageMessage('Image is required.'); 
        validate = false 
      }

      if (!validate) return 

      const data = {
        id: foodAllergyId,
        name: values.name,
        imageId: image?.id,
        imageThumbnailId: imageThumbnail?.id,
        oldImageId: isImageChange ? oldImageId : null,
        oldImageThumbnailId: isImageChange ? oldImageThumbnailId : null,
        description: values.description,
        creatureId: creatureId
      }

      setIsLoading(true)
      if (action === 'add') {
        await add(data as FoodAllergyProps)
        setIsLoading(false)
        navigateBack()
      } else if (action === 'edit') {
        await edit(data as FoodAllergyProps)
        setIsLoading(false)
        navigateBack()
      }
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  };

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const onImageChange = async(images: FileProps[]) => {
    setErrorImageMessage('')

    try {
      setImage(images[0])
      setImageThumbnail(images[1])
      setIsImageChange(true)
    } catch (error) {
      setErrorImageMessage(error)
    }
  }

  const renderForm = (props: FormikProps<FoodAllergyFormData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <PhotoChooser 
            source={item?.image}
            thumbnail={item?.imageThumbnail} 
            onChange={onImageChange} 
            error={errorImageMessage}
            nameUpload='food-allergy'
            tagUpload='food-allergy' 
        />
        <FormInput
          id='name'
          label='Food name'
          style={styles.input}
          defaultValue={item?.name}
        />
        <FormInput
          id='description'
          label='Description'
          style={[ styles.input , { alignItems: 'flex-start' } ]}
          defaultValue={item?.description}
          textStyle={{ height: 150, textAlignVertical: 'top', }}
          numberOfLines={10}
          multiline={true}
          maxLength={1500}
        />
         {
          errorMessage !== '' &&
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
              <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorMessage }</Text>
            </View>
          }
          {
            isLoading &&
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Spinner/>
            </View>
          }
      </Layout>
      <Button
        style={styles.saveButton}
        onPress={props.handleSubmit}
        disabled={isLoading}>
        Save
      </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title={title}
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
        <Formik
          initialValues={
            item ? 
              new FoodAllergyFormData(item.name, item.description) 
              : 
              FoodAllergyFormData.empty()
          }
          validationSchema={FoodAllergyFormSchema}
          onSubmit={onFormSubmit}>
          {renderForm}
        </Formik>
      </KeyboardAvoidingView>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    flex: 1,
    justifyContent: 'center',
    height: 150
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  }
});