import React from 'react';
import { 
  Dimensions, 
  ListRenderItemInfo, 
  Modal as NativeModal, 
  StyleSheet, 
  TouchableHighlight, 
  View 
} from 'react-native';
import { 
  Divider, 
  Icon,
  ListItemElement, 
  Modal, 
  Spinner, 
  Text, 
  TopNavigationAction, 
  TopNavigationActionElement 
} from '@ui-kitten/components';
import ImagePicker, { Image } from 'react-native-image-crop-picker';
import ImageViewer from 'react-native-image-zoom-viewer';
import FastImage  from 'react-native-fast-image';
import { TreatmentHistoryFilesScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { BackIcon, PlusIcon, TrashIcon } from '../../assets/icons';
import { MenuGridList } from '../../components/menu-grid-list.component';
import { MainTheme } from '../../constants/LOV';
import { ImageItem } from '../../components/image-item.component';
import { FileProps } from '../../store/models/file';
import { useStoreActions, useStoreState } from '../../store';
import { CriteriaSearch } from '../../model/criteria-search.model';

export const TreatmentHistoryFilesScreen = (props: TreatmentHistoryFilesScreenProps): SafeAreaLayoutElement => {
  const treatmentHistoryId = props.route.params.treatmentHistoryId
  let checkItems: string[] = []

  const [offset, setOffset] = React.useState<number>(0);
  const [imageTypeVisible, setImageTypeVisible] = React.useState<boolean>(false);
  const [checkboxVisabled, setCheckboxVisabled] = React.useState<boolean>(false);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [images, setImages ] = React.useState<any[]>([]);
  const [imageIndex, setImageIndex ] = React.useState<number>(0);
  const [modalPreviewVisible, setModalPreviewVisible] = React.useState<boolean>(false);

  const add = useStoreActions(a => a.file.add)
  const find = useStoreActions(a => a.file.find)
  const setItems = useStoreActions(a => a.file.setItems)
  const removeItems = useStoreActions(a => a.file.removeItems)
  const items = useStoreState(s => s.file.items)
  
  const getFileItems = async (criteria) => {
    try {
      setIsLoading(true)
      await find(criteria as CriteriaSearch)
      setIsLoading(false)

    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  React.useMemo(() => {
    setItems([])
    getFileItems({ offset: offset, refId: treatmentHistoryId, tag: 'treatment-history-list' })
  }, [])

  React.useEffect(() => {
    const imageItems = items.map((item: FileProps) => ({ url: item.uri }))
    // const imageItems = items.map((item: FileProps) => ({ url: 'https://get.pxhere.com/photo/marble-temple-thailand-bangkok-city-free-image-sky-mobile-photography-building-temple-place-of-worship-landmark-architecture-wat-hindu-temple-facade-shrine-historic-site-tourist-attraction-tourism-house-1567591.jpg' }))
    setImages(imageItems)
  }, [items])

  const onPlusButtonPress = (): void => {
    if (checkboxVisabled === true) return
    setImageTypeVisible(true)

  }

  const onTrashButtonPress = async () => {
    try {
      setIsLoading(true)
      await removeItems(checkItems)
      checkItems = []
      setIsLoading(false)
      onRefresh()
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  const onItemPress = (index: number): void => {
    setImageIndex(index)
    setModalPreviewVisible(true)
  };

  const onCameraTextPress = async () => {
    setImageTypeVisible(!imageTypeVisible);
    try {
      const image = await ImagePicker.openCamera({ mediaType: 'photo' }) as Image
      const crop = await ImagePicker.openCropper({ path: image.path })
      onImageChange(crop)
    } catch (error) {
      console.log('openCamera catch' + error.toString())
    }
  };

  const onSelectPhotoTextPress = async () => {
    try {
      setImageTypeVisible(!imageTypeVisible);
      const image = await ImagePicker.openPicker({ mediaType:  'photo' }) as Image
      const crop = await ImagePicker.openCropper({ path: image.path })
      await onImageChange(crop)
    } catch (error) {
      console.log("openCamera catch" + error.toString())
    }
  };

  const onImageChange = async(image: Image) => {
    try {
      const data = {
        refId: treatmentHistoryId,
        name: 'treatment-history-list',
        tag: 'treatment-history-list',
        type: image.mime,
        uri: image.path
      }
      setIsLoading(true)
      const res = await add(data as FileProps) as FileProps[]
      onRefresh()
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  const onScroll = async () => {
    try {
      setIsLoading(true)
      await find({ offset: offset + 1, refId: treatmentHistoryId, tag: 'treatment-history-list' } as CriteriaSearch)
      setOffset(offset + 1)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
    }
  }

  const onRefresh = (): void => {
    setCheckboxVisabled(false)
    setItems([])
    getFileItems({ offset: 0, refId: treatmentHistoryId, tag: 'treatment-history-list' })
    setOffset(0)
  }

  const onCheckChange = (checked: boolean, index: number): void => {
    if (checked) { checkItems.push(items[index].id) } 
    else {checkItems = checkItems.filter(item => item !== items[index].id)}
  }

  const renderItem = (info: ListRenderItemInfo<FileProps>): ListItemElement => {
    return (
      <TouchableHighlight 
        style={styles.item} 
        onPress={() => onItemPress(info.index)} 
        onLongPress={() => setCheckboxVisabled(true)} 
      >
        <ImageItem 
          index={info.index}
          style={styles.postItem}
          source={{ uri: info.item.uri }}
          checkboxVisabled={checkboxVisabled}
          onCheckChange={onCheckChange}
        />
      </TouchableHighlight>
    )
  }

  const plusIconRender: TopNavigationActionElement = (
    <TopNavigationAction
      icon={PlusIcon}
      onPress={onPlusButtonPress}
    />
  )

  const trashIconRender: TopNavigationActionElement = (
    <TopNavigationAction
      icon={TrashIcon}
      onPress={onTrashButtonPress}
    />
  )

  let rightControls: TopNavigationActionElement[] = [
    plusIconRender,
    trashIconRender
  ]

  if (checkboxVisabled === false) rightControls = rightControls.slice(0, 1) 

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title='Treatment History Files'
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={BackIcon}
        onBackPress={props.navigation.goBack}
        rightControls={rightControls}
        style={styles.toolbar}
      />
      <Divider/>
      <View style={styles.container}>
        <MenuGridList
          data={items}
          renderItem={renderItem}
          style={{ backgroundColor: MainTheme.colorPrimary }}
          refreshing={isLoading}
          onRefresh={onRefresh}
          onScroll={onScroll}
        />
      </View>
      <Modal
        visible={imageTypeVisible}
        onBackdropPress={() => setImageTypeVisible(!imageTypeVisible)}
        backdropStyle={styles.backdrop}
        style={{ width: '60%' }}>
          <View style={styles.childrenModalCantainer}>
            <Text style={styles.childrenModalText} onPress={onCameraTextPress}>Camera</Text>
            <Divider />
            <Text style={styles.childrenModalText} onPress={onSelectPhotoTextPress}>Select Photo</Text>
          </View>
      </Modal>
      {/* <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={{ alignItems: 'center' }}>
          <Spinner/>
        </View>
      </Modal> */}
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
      <NativeModal visible={modalPreviewVisible} transparent={true}>
        <ImageViewer
          index={imageIndex}
          imageUrls={images}
          enableSwipeDown
          onChange={(index?: number) => setImageIndex(index as number)}
          onSwipeDown={() => setModalPreviewVisible(false)}
          renderIndicator={() => <Spinner />}
          renderHeader={() => (
            <Toolbar
              title={`${imageIndex + 1} / ${images.length}`}
              titleStyle={{ color: MainTheme.colorSecondary }}
              rightControls={
                <TopNavigationAction
                  icon={() => (
                    <Icon name='close' pack='ant-design' style={{ width: 35, height: 35, color: MainTheme.colorSecondary }} />
                  )}
                  onPress={() => setModalPreviewVisible(false)}
                />
              }
              style={{ backgroundColor: MainTheme.colorPrimary }}
            />
          )}
          renderImage={(item) => (
            <FastImage 
              source={{...item.source}} 
              style={{...item.style}} 
              onError={() => setImages([{ url: 'https://unsplash.it/400/400?image=1' }])}
            />
          )}
        />
      </NativeModal>
    </SafeAreaLayout>
  )
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    aspectRatio: 1.0,
    margin: 8,
    maxWidth: Dimensions.get('window').width / 2 - 24,
    // backgroundColor: MainTheme.itemBackGround,
    borderColor: MainTheme.itemBackGround
  },
  itemTitle: {
    alignSelf: 'center',
    color: MainTheme.colorSecondary
  },
  searchInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  scanButton: { 
    paddingHorizontal: 0,
  },
  iconButton: {
    width: 24,
    height: 24,
    backgroundColor: MainTheme.buttonColor,
    borderColor: MainTheme.buttonColor
  },
  plusButton: {
    borderRadius: 24,
    marginHorizontal: 8,
  },
  messageInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  postItem: {
    flex: 1, 
    justifyContent: 'center',
    borderRadius: 4,
    overflow: 'hidden'
  },
  imageSpinner: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  previewBackdrop: {
    backgroundColor: 'rgba(0, 0, 0, 1)',
  },
  childrenModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  }
});
