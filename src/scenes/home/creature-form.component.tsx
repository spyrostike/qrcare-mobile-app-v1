import React from 'react';
import { StyleSheet, View } from 'react-native';
import { 
  Button, 
  Datepicker, 
  Divider, 
  Icon, 
  Layout, 
  Modal, 
  Select, 
  SelectOptionType, 
  Spinner, 
  Text 
} from '@ui-kitten/components';
import { CreatureFormScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { CreatureFormData, CreatureFormSchema } from '../../data/creature-form.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { AppRoute } from '../../navigation/app-routes';
import { useStoreActions, useStoreState } from '../../store';
import { PhotoChooser } from '../../components/photo-chooser.component';
import { FileProps } from '../../store/models/file';
import { FormInput } from '../../components/form-input.component';
import { PhotoChooserBox } from '../../components/photo-chooser-box.component';
import { CreatureProps } from '../../store/models/creature';

export const CreatureFormScreen = (props: CreatureFormScreenProps): SafeAreaLayoutElement => {
  const title = `Creature Form - ${ props.route.params.action.charAt(0).toUpperCase() + props.route.params.action.slice(1) }` 
  const id = props.route.params.creatureId as string
  const action = props.route.params.action
  const now = new Date();
  const yesterday = new Date(now.getFullYear() - 50, now.getMonth(), now.getDate() - 1);
  const tomorrow = new Date(now.getFullYear() + 50, now.getMonth(), now.getDate() + 1);

  const genderOptions = [
    { text: '-- Select --', value: null, disabled: true },
    { text: 'Male', value: 1 },
    { text: 'Female', value: 2 }
  ]

  const bloodTypeOptions = [
    { text: '-- Select --', value: null, disabled: true },
    { text: 'AB', value: 1 },
    { text: 'A', value: 2 },
    { text: 'B', value: 3 },
    { text: 'O', value: 4 }
  ]

  const [profileImage, setProfileImage] = React.useState<FileProps>();
  const [profileImageThumbnail, setProfileImageThumbnail] = React.useState<FileProps>();
  const [birthCertificateImage, setBirthCertificateImage] = React.useState<FileProps>();
  const [birthCertificateImageThumbnail, setBirthCertificateImageThumbnail] = React.useState<FileProps>();
  const [bloodTypeSelected, setBloodTypeSelected] = React.useState<SelectOptionType>(bloodTypeOptions[0]);
  const [genderSelected, setGenderSelected] = React.useState<SelectOptionType>(genderOptions[0]);
  const [birthDate, setBirthDate] = React.useState<Date>(new Date);
  const [errorGenderMessage, setErrorGenderMessage] = React.useState<string>('');
  const [errorBloodTypeMessage, setErrorBloodTypeMessage] = React.useState<string>('');
  const [errorImageProfileMessage, setErrorImageProfileMessage] = React.useState<string>('');
  const [errorImageBirthCertificateMessage, setErrorImageBirthCertificateMessage] = React.useState<string>('');
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [oldCreatureImageId, setOldCreatureImageId] = React.useState<string>('');
  const [oldCreatureImageThumbnailId, setOldCreatureImageThumbnailId] = React.useState<string>('');
  const [oldBirthCertificateImageId, setOldBirthCertificateImageId] = React.useState<string>('');
  const [oldBirthCertificateImageThumbnailId, setOldBirthCertificateImageThumbnailId] = React.useState<string>('');
  const [isCreatureImageChange, setIsCreatureImageChange] = React.useState<boolean>(false)
  const [isBirthCertificateImageChange, setIsBirthCertificateImageChange] = React.useState<boolean>(false)

  const add = useStoreActions(a => a.creature.add)
  const edit = useStoreActions(a => a.creature.edit)
  const qrcode = useStoreState(s => s.qrcode.item)
  const setItem = useStoreActions(a => a.creature.setItem)
  const item = useStoreState(s => s.creature.item)

  React.useMemo(() => {
    const getCretureInfo = () => {
      try {
        if (action !== 'edit') { setItem({} as CreatureProps); return }
        if (id === '') return
        // await getInfo(id)
        const genderId = item?.gender !== undefined ? parseInt(item?.gender) : 0
        const bloodTypeId = item?.bloodType !== undefined ? parseInt(item?.bloodType) : 0

        setProfileImage({ id: item?.creatureImageId ? item?.creatureImageId : null } as FileProps)
        setProfileImageThumbnail({ id: item?.creatureImageThumbnailId ? item?.creatureImageThumbnailId : null } as FileProps)
        setBirthCertificateImage({ id: item?.birthCertificateId ? item?.birthCertificateId : null } as FileProps)
        setBirthCertificateImageThumbnail({ id: item?.birthCertificateThumbnailId ? item?.birthCertificateThumbnailId : null } as FileProps)
        setOldCreatureImageId(item?.creatureImageId as string)
        setOldCreatureImageThumbnailId(item?.creatureImageThumbnailId as string)
        setOldBirthCertificateImageId(item?.birthCertificateId as string)
        setOldBirthCertificateImageThumbnailId(item?.birthCertificateThumbnailId as string)
        setGenderSelected(genderOptions[genderId])
        setBloodTypeSelected(bloodTypeOptions[bloodTypeId])  
        setBirthDate(item?.birthDate !== undefined ? new Date(item?.birthDate.toString()) : new Date)
      } catch (error) {
        setErrorMessage(error)
      }
    }
    
    getCretureInfo()
    
  }, [])

  const onFormSubmit = async (values: CreatureFormData) => {
    try {
      const bloodType: any = bloodTypeSelected
      const gender: any = genderSelected
      let validate = true
      
      setErrorGenderMessage('')
      setErrorBloodTypeMessage('')
      setErrorImageProfileMessage('')
      setErrorImageBirthCertificateMessage('')

      if (typeof profileImage?.id === 'undefined' || profileImage?.id === null) { 
        setErrorImageProfileMessage('Profile Image is required.'); 
        validate = false 
      }

      if (typeof birthCertificateImage?.id === 'undefined' || birthCertificateImage?.id === null) { 
        setErrorImageBirthCertificateMessage('Birth Certificate Image is required.'); 
        validate = false 
      }

      if (gender.value === null) { setErrorGenderMessage('Gender is required.'); validate = false }
      if (bloodType.value === null) { setErrorBloodTypeMessage('Blood Type is required.'); validate = false }
      
      if (!validate) return 

      const data = {
        qrcodeItemId: qrcode?.id,
        id: id,
        firstName: values.firstName,
        lastName: values.lastName,
        gender: gender.value as any,
        bloodType: bloodType.value as any,
        birthDate: new Date(birthDate.getFullYear(), birthDate.getMonth(), birthDate.getDate() + 1),
        weight: values.weight,
        height: values.height,
        creatureImageId: profileImage?.id,
        creatureImageThumbnailId: profileImageThumbnail?.id,
        birthCertificateId: birthCertificateImage?.id,
        birthCertificateThumbnailId: birthCertificateImageThumbnail?.id,
        oldCreatureImageId: isCreatureImageChange ? oldCreatureImageId : null,
        oldCreatureImageThumbnailId: isCreatureImageChange ? oldCreatureImageThumbnailId : null,
        oldBirthCertificateImageId: isBirthCertificateImageChange ? oldBirthCertificateImageId : null,
        oldBirthCertificateImageThumbnailId: isBirthCertificateImageChange ? oldBirthCertificateImageThumbnailId : null
      } 
      
      setIsLoading(true)
      if (action === 'add') {
        await add(data as CreatureProps)
        setIsLoading(false)
        navigateHome()
      } else if (action === 'edit') {
        await edit(data as CreatureProps)
        setIsLoading(false)
        navigateBack()
      }
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  };

  const navigateHome = (): void => {
    props.navigation.reset({
      index: 0,
      routes: [{ name: AppRoute.HOME }],
    })
  }

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const onBloodTypeSelect = (option): void => {
    setBloodTypeSelected(option)
  }

  const onGenderSelect = (option): void => {
    setGenderSelected(option)
  }

  const onProfileChange = async(images: FileProps[]) => {
    setErrorImageProfileMessage('')

    try {
      setProfileImage(images[0])
      setProfileImageThumbnail(images[1])
      setIsCreatureImageChange(true)
    } catch (error) {
      setErrorImageProfileMessage(error)
    }
  }

  const onBirthCertificateChange = async(images: FileProps[]) => {
    setErrorImageBirthCertificateMessage('')

    try {
      setBirthCertificateImage(images[0])
      setBirthCertificateImageThumbnail(images[1])
      setIsBirthCertificateImageChange(true)
    } catch (error) {
      setErrorImageBirthCertificateMessage(error)
    }
  }

  const renderForm = (props: FormikProps<CreatureFormData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <PhotoChooser 
            source={item?.creatureImage}
            thumbnail={item?.creatureImageThumbnail} 
            onChange={onProfileChange} 
            error={errorImageProfileMessage}
            nameUpload='creature-profile'
            tagUpload='creature-profile' 
        />
        <FormInput
          id='firstName'
          style={styles.input}
          defaultValue={item?.firstName}
          label='First Name'
        />
        <FormInput
          id='lastName'
          style={styles.input}
          defaultValue={item?.lastName}
          label='Last Name'
        />
        <PhotoChooserBox
          hint='Birth Certificate' 
          source={item?.birthCertificate}
          thumbnail={item?.birthCertificateThumbnail}
          nameUpload='creature-birth-certificate' 
          tagUpload='creature-birth-certificate'
          error={errorImageBirthCertificateMessage}
          onChange={onBirthCertificateChange} 
        />
        <Select
          style={styles.input}
          label='Gender'
          data={genderOptions}
          selectedOption={genderSelected}
          onSelect={onGenderSelect}
        />
        {
          errorGenderMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorGenderMessage }</Text>
          </View>
        }
        <Select
          style={styles.input}
          label='Blood type'
          data={bloodTypeOptions}
          selectedOption={bloodTypeSelected}
          onSelect={onBloodTypeSelect}
        />
        {
          errorBloodTypeMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorBloodTypeMessage }</Text>
          </View>
        }
        <Datepicker
          style={styles.input}
          label='Birth Date'
          date={birthDate}
          min={yesterday}
          max={tomorrow}
          onSelect={setBirthDate}
          boundingMonth={false}
        />
        <FormInput
          id='weight'
          style={styles.input}
          label='Weight'
          keyboardType='number-pad'
          defaultValue={item?.weight}
        />
        <FormInput
          id='height'
          style={styles.input}
          label='Height'
          keyboardType="number-pad"
          defaultValue={item?.height}
        />
      </Layout>
      <Button
          style={styles.saveButton}
          onPress={props.handleSubmit}>
          Save
      </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title={title}
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
          <Formik
            initialValues={
              item ? 
                new CreatureFormData(item.firstName, item.lastName, item.weight, item.height) 
                : 
                CreatureFormData.empty()
            }
            validationSchema={CreatureFormSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
      </KeyboardAvoidingView>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    // paddingHorizontal: 4,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    // marginHorizontal: 24,
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    flex: 1,
    justifyContent: 'center',
    // aspectRatio: 1.0,
    height: 150
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
});