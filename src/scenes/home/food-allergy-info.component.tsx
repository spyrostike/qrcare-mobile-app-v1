import React from 'react';
import { Dimensions, Linking, ScrollView, StyleSheet, View } from 'react-native';
import { 
  Divider, 
  Layout, 
  LayoutElement, 
  Modal, 
  Spinner, 
  Text, 
  TopNavigationAction, 
  TopNavigationActionElement 
} from '@ui-kitten/components';
import FastImage from 'react-native-fast-image';
import ParsedText from 'react-native-parsed-text';
import {
  SafeAreaLayout,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { FoodAllergyInfoScreenProps } from '../../navigation/home.navigator';
import { AppRoute } from '../../navigation/app-routes';
import { Toolbar } from '../../components/toolbar.component';
import { MainTheme } from '../../constants/LOV';
import { BackIcon, EditIcon } from '../../assets/icons';
import { ProfileAvatarPreview } from '../../components/profile-avatar-preview.component';
import { ProfileSetting } from '../../components/profile-setting.component';
import { useStoreActions, useStoreState } from '../../store';
import { FoodAllergyProps } from 'src/store/models/food-allergy';

export const FoodAllergyInfoScreen = (props: FoodAllergyInfoScreenProps): LayoutElement => {
  const foodAllergyId = props.route.params.foodAllergyId

  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const getInfo = useStoreActions(a => a.foodAllergy.getInfo)
  const setItem = useStoreActions(a => a.foodAllergy.setItem)
  const item = useStoreState(s => s.foodAllergy.item)

  const getFoodAllergyInfo = async () => {
    try{
      setItem({} as FoodAllergyProps)
      setIsLoading(true)
      const res =  await getInfo(foodAllergyId)
      setIsLoading(false)
      setItem(res)
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  React.useMemo(() => {
    getFoodAllergyInfo()
  }, [])

  const onEditButtonPress = (): void => {
    props.navigation.navigate(AppRoute.FOOD_ALLERGY_FORM, { action: 'edit', foodAllergyId: foodAllergyId })
  }
  
  const rightControl: TopNavigationActionElement = (
    <TopNavigationAction
      icon={EditIcon}
      onPress={onEditButtonPress}
    />
  )

  const onUrlPress = async (url) => {
    if (!url.startsWith('https://') || !url.startsWith('http://')) url = 'http://' + url
    const supported = await Linking.canOpenURL(url);
    if (supported) Linking.openURL(url);
    else setErrorMessage(`Could not open this URL: ${url}`)
  }

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title='Food Allergy Info'
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={props.navigation.goBack}
          rightControls={rightControl}
          style={styles.toolbar}
        />
      <Divider />
      <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <ProfileAvatarPreview
          style={[ styles.profileAvatar ]}
          source={item?.image}
          thumbnail={item?.imageThumbnail}
          resizeMode={FastImage.resizeMode.cover}
        />
        <ProfileSetting
          style={[styles.profileSetting, styles.section]}
          hint='Food Name'
          value={item?.name}
        />
        <Layout style={styles.descriptionContainer} level='1'>
          <Text
            appearance='hint'
            category='s1'
            style={{ marginBottom: 8 }} >
            Description
          </Text>
          <ParsedText
            style={{ color: MainTheme.colorSecondary }}
            parse={ [ {type: 'url', style: styles.url, onPress: onUrlPress} ]}
            childrenProps={{allowFontScaling: false}}>
            {item?.description}
          </ParsedText>
        </Layout>
        <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
          <View style={styles.childrenModalCantainer}>
            <Spinner/>
          </View>
        </Modal>
        <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
          <View style={styles.errorModalCantainer}>
            <Text style={styles.childrenModalText}>{ errorMessage }</Text>
            <Divider />
            <Text 
              style={[ 
                styles.childrenModalText, 
                { 
                  color: MainTheme.colorSecondary, 
                  backgroundColor: MainTheme.buttonColor, 
                  borderBottomLeftRadius: 5,
                  borderBottomRightRadius: 5 }
                ]}
                onPress={() => setErrorMessage('')}>
                OK
            </Text>
          </View>
        </Modal>
      </ScrollView>
    </SafeAreaLayout>
  )

}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
    paddingTop: 24
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  contentContainer: {
    paddingBottom: 24,
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  profileSetting: {
    padding: 16,
    backgroundColor: MainTheme.colorPrimary
  },
  section: {
    marginTop: 24,
  },
  imageButtonContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    marginTop: 15,
    alignItems: 'center', 
    backgroundColor: MainTheme.colorPrimary 
  },
  imageButton: {
    aspectRatio: 1.0,
    width: Dimensions.get('window').width / 3 - 24
  },
  editContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  editButton: {
    flexDirection: 'row-reverse',
    paddingHorizontal: 5,
  },
  descriptionContainer: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,  
    padding: 16,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  },
  url: {
    color: MainTheme.buttonColor,
    textDecorationLine: 'underline',
  }
})