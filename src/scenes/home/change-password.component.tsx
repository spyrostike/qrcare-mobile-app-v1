import React from 'react';
import { ImageBackground, StyleSheet, View } from 'react-native';
import { EdgeInsets, useSafeArea } from 'react-native-safe-area-context';
import { Button, Divider, Layout, LayoutElement, Modal, Spinner, Text } from '@ui-kitten/components';
import { Formik, FormikProps } from 'formik';
import { ChangePasswordScreenProps } from '../../navigation/home.navigator';
import { AppRoute } from '../../navigation/app-routes';
import { FormInput } from '../../components/form-input.component';
import { Toolbar } from '../../components/toolbar.component';
import { ChangePasswordFormData, ChangePasswordFormSchema } from '../../data/change-password-form.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { MainTheme } from '../../constants/LOV';
import { EyeIcon, EyeOffIcon } from '../../assets/icons';
import { useStoreActions, useStoreState } from '../../store';

export const ChangePasswordScreen = (props: ChangePasswordScreenProps): LayoutElement => {

  const [currentPasswordVisible, setCurrentPasswordVisible] = React.useState<boolean>(false);
  const [newPasswordVisible, setNewPasswordVisible] = React.useState<boolean>(false);
  const [repearPasswordVisible, setRepearPasswordVisible] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const changePassword = useStoreActions(a => a.member.changePassword)

  const onFormSubmit = async (values: ChangePasswordFormData) => {
    try {
      setIsLoading(true)
      await changePassword({ currentPassword: values.currentPassword, newPassword: values.newPassword })
      setIsLoading(false)
      navigateBack();
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  };

  const navigateBack = (): void => {
    props.navigation.goBack();
  };

  const renderForm = (props: FormikProps<ChangePasswordFormData>): React.ReactFragment => (
    <React.Fragment>
      <View style={{ flex: 1 }}>
        <FormInput
          label='Current Password'
          id='currentPassword'
          style={styles.formControl}
          secureTextEntry={!currentPasswordVisible}
          icon={currentPasswordVisible ? EyeIcon : EyeOffIcon}
          onIconPress={() => setCurrentPasswordVisible(!currentPasswordVisible)}
        />
        <FormInput
          label='New Password'
          id='newPassword'
          style={styles.formControl}
          secureTextEntry={!newPasswordVisible}
          icon={newPasswordVisible ? EyeIcon : EyeOffIcon}
          onIconPress={() => setNewPasswordVisible(!newPasswordVisible)}
        />
        <FormInput
          label='Repeat Password'
          id='repeatPassword'
          style={styles.formControl}
          secureTextEntry={!repearPasswordVisible}
          icon={repearPasswordVisible ? EyeIcon : EyeOffIcon}
          onIconPress={() => setRepearPasswordVisible(!repearPasswordVisible)}
        />
      </View>
      <Button
        style={styles.button}
        onPress={props.handleSubmit}>
        Change Password
      </Button>
    </React.Fragment>
  );

  return (
    <KeyboardAvoidingView>
      <View style={styles.container} >
        <Toolbar
          title='Change Password'
          titleStyle={{ color: MainTheme.colorSecondary }}
          appearance='control'
          onBackPress={props.navigation.goBack}
        />
        <View style={styles.formContainer}>
          <Formik
            initialValues={ChangePasswordFormData.empty()}
            validationSchema={ChangePasswordFormSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
        </View>
      </View>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  appBar: {
    height: 192,
  },
  formContainer: {
    flex: 1,
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  formControl: {
    marginVertical: 4,
  },
  button: {
    marginVertical: 24,
  },
  forgotPasswordLabel: {
    zIndex: 1,
    alignSelf: 'center',
  },
  enterEmailLabel: {
    zIndex: 1,
    alignSelf: 'center',
    marginTop: 64,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
});
