import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Divider, Icon, Layout, Modal, Select, SelectOptionType, Spinner, Text } from '@ui-kitten/components';
import { MedicalFormScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { MedicalFormData, MedicalFormSchema } from '../../data/medical-form.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { useStoreActions, useStoreState } from '../../store';
import { FormInput } from '../../components/form-input.component';
import { MainTheme } from '../../constants/LOV';
import { AppRoute } from '../../navigation/app-routes';
import { MedicalProps } from '../../store/models/medical'

export const MedicalFormScreen = (props: MedicalFormScreenProps): SafeAreaLayoutElement => {
  const title = `Medical Form - ${ props.route.params.action.charAt(0).toUpperCase() + props.route.params.action.slice(1) }` 
  const creatureId = props.route.params.creatureId as string
  const medicalId = props.route.params.medicalId as string
  const action = props.route.params.action

  const bloodTypeOptions = [
    { text: '-- Select --', value: null, disabled: true },
    { text: 'AB', value: 1 },
    { text: 'A', value: 2 },
    { text: 'B', value: 3 },
    { text: 'O', value: 4 }
  ]

  const [bloodTypeSelected, setBloodTypeSelected] = React.useState<SelectOptionType>(bloodTypeOptions[0]);
  const [errorBloodTypeMessage, setErrorBloodTypeMessage] = React.useState<string>('');
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const add = useStoreActions(a => a.medical.add)
  const edit = useStoreActions(a => a.medical.edit)
  const setItem = useStoreActions(a => a.medical.setItem)
  const item = useStoreState(s => s.medical.item)

  React.useMemo(() => {
    if (action !== 'edit') { 
      setItem({} as MedicalProps) 
      return }
    else { 
      const bloodTypeId = item?.bloodType !== undefined ? parseInt(item?.bloodType) : 0
      setBloodTypeSelected(bloodTypeOptions[bloodTypeId])
    }
    
  }, [])

  const onFormSubmit = async (values: MedicalFormData) => {
    try {
      const bloodType: any = bloodTypeSelected
      let validate = true

      setErrorBloodTypeMessage('')

      if (bloodType.value === null) { setErrorBloodTypeMessage('Blood Type is required.'); validate = false }

      if (!validate) return 

      const data = {
        id: item?.id,
        hospitalName: values.hospital,
        hospitalTel: values.hospitalTel,
        patientId: values.patientId,
        bloodType: bloodType.value as any,
        doctorName: values.doctorName,
        doctorContactNo: values.doctorContact,
        congenitalDisorder: values.congenitalDisorder,
        creatureId: creatureId
      } 

      setIsLoading(true)
      if (action === 'add') {
        await add(data as MedicalProps)
        setIsLoading(false)
        navigateBack()
      } else if (action === 'edit') {
        await edit(data as MedicalProps)
        setIsLoading(false)
        navigateBack()
      }
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  };

  const onBloodTypeSelect = (option): void => {
    setBloodTypeSelected(option)
  }

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const onMedicineIconPress = (): void => {
    props.navigation.navigate(AppRoute.MEDICAL_FORM, { action: 'add', creatureId: '1' })
  }

  const renderForm = (props: FormikProps<MedicalFormData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <FormInput
          id='hospital'
          style={styles.input}
          defaultValue={item?.hospitalName}
          label='Hospital'
        />
        <FormInput
          id='hospitalTel'
          style={styles.input}
          defaultValue={item?.hospitalTel}
          label='Hospital Tel'
        />
        <FormInput
          id='patientId'
          label='Patient ID'
          style={styles.input}
          defaultValue={item?.patientId}
        />
        <Select
          style={styles.input}
          label='Blood type'
          data={bloodTypeOptions}
          selectedOption={bloodTypeSelected}
          onSelect={onBloodTypeSelect}
        />
        {
          errorBloodTypeMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorBloodTypeMessage }</Text>
          </View>
        }
        <FormInput
          id='doctorName'
          label='Doctor’s name'
          style={styles.input}
          defaultValue={item?.doctorName}
        />
        <FormInput
          id='doctorContact'
          label='Doctor’s contact'
          style={styles.input}
          defaultValue={item?.doctorContactNo}
        />
        <FormInput
          id='congenitalDisorder'
          label='Congenital Disorder'
          style={styles.input}
          defaultValue={item?.congenitalDisorder}
        />
      </Layout>
      <Button
          style={styles.saveButton}
          onPress={props.handleSubmit}>
          Save
        </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title={title}
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
          <Formik
            initialValues={
              item ? 
                new MedicalFormData(
                  item.hospitalName, 
                  item.hospitalTel, 
                  item.patientId, 
                  item.doctorName,
                  item.doctorContactNo,
                  item.congenitalDisorder
                ) 
                : 
                MedicalFormData.empty()
              }
            validationSchema={MedicalFormSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
      </KeyboardAvoidingView>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  saveButton: {
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
});