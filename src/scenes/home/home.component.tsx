import React from 'react';
import { ListRenderItemInfo, StyleSheet, View } from 'react-native';
import { 
  Divider, 
  Input, 
  ListItemElement, 
  Modal,
  Text, 
  TopNavigationAction, 
  TopNavigationActionElement 
} from '@ui-kitten/components';
import { HomeScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MenuIcon, PlusIcon, SearchIcon, QRCodeIcon, TrashIcon } from '../../assets/icons';
import { MenuGridList } from '../../components/menu-grid-list.component';
import { AppRoute } from '../../navigation/app-routes';
import { APP_NAME, MainTheme } from '../../constants/LOV';
import { useStoreActions, useStoreState } from '../../store';
import { CreatureProps } from '../../store/models/creature';
import { CriteriaSearch } from '../../model/criteria-search.model';
import { CreatureItemBox } from '../../components/creature-item-box.component';

export const HomeScreen = (props: HomeScreenProps): SafeAreaLayoutElement => {
  let checkItems: string[] = []

  const [textSearch, setTextSearch] = React.useState<string>('');
  const [offset, setOffset] = React.useState<number>(0);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [checkboxVisabled, setCheckboxVisabled] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const find = useStoreActions(a => a.creature.find)
  const setItems = useStoreActions(a => a.creature.setItems)
  const removeItems = useStoreActions(a => a.creature.removeItems)
  const items = useStoreState(s => s.creature.items)

  const getCretureItems = async (criteria) => {
    try {
      setIsLoading(true)
      await find(criteria as CriteriaSearch)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
    }
  }

  React.useMemo(() => {
    setItems([])
    getCretureItems({ textSearch: textSearch, offset: offset })
  }, [])

  const onSearchIconPress = (): void => {
    setCheckboxVisabled(false)
    setItems([])
    getCretureItems({ textSearch: textSearch, offset: 0 })
    setOffset(0)
  }

  const onScroll = async () => {
    try {
      setIsLoading(true)
      await find({ textSearch: textSearch, offset: offset + 1 } as CriteriaSearch)
      setOffset(offset + 1)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
    }
  }

  const onRefresh = (): void => {
    setCheckboxVisabled(false)
    setItems([])
    getCretureItems({ textSearch: textSearch, offset: 0 })
    setOffset(0)
  }

  const onPlusButtonPress = (): void => {
    props.navigation.navigate(AppRoute.SCAN_QR_CODE, { action: 'creature-add' })
  }

  const onScanQRCodeButtonPress = (): void => {
    props.navigation.navigate(AppRoute.SCAN_QR_CODE, { action: 'creature-info' })
  }

  const onTrashButtonPress = async () => {
    try {
      setCheckboxVisabled(false)
      setIsLoading(true)
      await removeItems(checkItems)
      checkItems = []
      setIsLoading(false)
      onRefresh()
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  const plusIconRender: TopNavigationActionElement = (
    <TopNavigationAction
      icon={PlusIcon}
      onPress={onPlusButtonPress}
    />
  )

  const trashIconRender: TopNavigationActionElement = (
    <TopNavigationAction
      icon={TrashIcon}
      onPress={onTrashButtonPress}
    />
  )
  
  const scanQRCodeIconRender: TopNavigationActionElement = (
    <TopNavigationAction
      icon={QRCodeIcon}
      onPress={onScanQRCodeButtonPress}
    />
  )
  
  const onItemPress = (index: number): void => {
    const id: string = typeof items[index]?.id !== 'undefined' && items[index]?.id !== null ? items[index].id as string : ''
    if (id !== '') props.navigation.navigate(AppRoute.CREATURE_INFO, { creatureId: id })
  }

  const onCheckChange = (checked: boolean, index: number): void => {
    if (checked) { checkItems.push(items[index].id as string) } 
    else {checkItems = checkItems.filter(item => item !== items[index].id)}
  }
  
  let rightControls: TopNavigationActionElement[] = [
    plusIconRender,
    scanQRCodeIconRender,
    trashIconRender
  ]

  if (checkboxVisabled === false) rightControls = rightControls.slice(0, 2)

  const renderItem = (info: ListRenderItemInfo<CreatureProps>): ListItemElement => (
    <CreatureItemBox 
      index={info.index}
      onPress={() => onItemPress(info.index)}
      onLongPress={() => setCheckboxVisabled(true)} 
      source={info.item?.creatureImageThumbnail as string}
      text={`${info.item.firstName} ${info.item.lastName}`}
      checkboxVisabled={checkboxVisabled}
      onCheckChange={onCheckChange}
    />
  );

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title={APP_NAME}
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={MenuIcon}
        onBackPress={props.navigation.toggleDrawer}
        rightControls={rightControls}
        style={styles.toolbar}
      />
      <Divider/>
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', paddingHorizontal: 8, paddingVertical: 16 }} >
          <Input
            style={styles.searchInput}
            placeholder='Search...'
            value={textSearch}
            onChangeText={setTextSearch}
            onIconPress={onSearchIconPress}
            icon={SearchIcon}
          />
        </View>
        <Divider/>
        <MenuGridList
          data={items}
          renderItem={renderItem}
          onItemPress={onItemPress}
          style={{ backgroundColor: MainTheme.colorPrimary }}
          refreshing={isLoading}
          onRefresh={onRefresh}
          onScroll={onScroll}
        />
      </View>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  )
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1
  },
  searchInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  scanButton: { 
    paddingHorizontal: 0,
  },
  iconButton: {
    width: 24,
    height: 24,
    backgroundColor: MainTheme.buttonColor,
    borderColor: MainTheme.buttonColor
  },
  plusButton: {
    borderRadius: 24,
    marginHorizontal: 8,
  },
  messageInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  }
});
