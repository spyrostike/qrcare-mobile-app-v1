import React from 'react';
import { Dimensions, Linking, ScrollView, StyleSheet, View } from 'react-native';
import {  
  Divider, 
  LayoutElement, 
  Modal,
  Spinner,
  Text,
  TopNavigationAction, 
  TopNavigationActionElement  
} from '@ui-kitten/components';
import {
  SafeAreaLayout,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MedicalInfoScreenProps } from '../../navigation/home.navigator';
import { AppRoute } from '../../navigation/app-routes';
import { Toolbar } from '../../components/toolbar.component';
import { MainTheme } from '../../constants/LOV';
import { BackIcon, EditIcon, FoodIcon, MedicineBoxIcon } from '../../assets/icons';
import { ProfileSetting } from '../../components/profile-setting.component';
import { ProfileIconSetting } from '../../components/profile-icon-setting.component';
import { useStoreActions, useStoreState } from '../../store';
import { MedicalProps } from '../../store/models/medical';

export const MedicalInfoScreen = (props: MedicalInfoScreenProps): LayoutElement => {
  const creatureId = props.route.params.creatureId

  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const getInfo = useStoreActions(a => a.medical.getInfo)
  const setItem = useStoreActions(a => a.medical.setItem)
  const item = useStoreState(s => s.medical.item)

  React.useMemo(() => {
    const getMedicalInfo = async () => {
      try {
        setItem({} as MedicalProps)
        setIsLoading(true)
        const res = await getInfo(creatureId)
        setIsLoading(false)
        setItem(res)
      } catch (error) {
        setIsLoading(false)
        setErrorMessage(error)
      }
    }

    getMedicalInfo()
  }, [])

  const onEditButtonPress = (): void => {
    if (item?.id) props.navigation.navigate(AppRoute.MEDICAL_FORM, { action: 'edit', creatureId: creatureId, medicalId: item.id })
    else  props.navigation.navigate(AppRoute.MEDICAL_FORM, { action: 'add', creatureId: creatureId })
  }

  const onDrugIconPress = (): void => {
    props.navigation.navigate(AppRoute.DRUG_LIST, { creatureId: creatureId })
  }
  
  const onDrugAllergyIconPress = (): void => {
    props.navigation.navigate(AppRoute.DRUG_ALLERGY_LIST, { creatureId: creatureId })
  }

  const onFoodAllergyIconPress = (): void => {
    props.navigation.navigate(AppRoute.FOOD_ALLERGY_LIST, { creatureId: creatureId })
  }

  const rightControl: TopNavigationActionElement = (
    <TopNavigationAction
      icon={EditIcon}
      onPress={onEditButtonPress}
    />
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title='Medical Info'
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={BackIcon}
        onBackPress={props.navigation.goBack}
        rightControls={rightControl}
        style={styles.toolbar}
      />
      <Divider />
      <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <ProfileSetting
          style={styles.profileSetting}
          hint='Hospital'
          value={item?.hospitalName}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Hospital Tel'
          value={item?.hospitalTel}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Patient ID'
          value={item?.patientId}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Blood Type'
          value={item?.bloodTypeName}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Doctor’s name'
          value={item?.doctorName}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Doctor’s contact'
          value={item?.doctorContactNo}
          onPress={() => Linking.openURL(`tel:${item?.doctorContactNo}`)}
        />
        <ProfileSetting
          style={styles.profileSetting}
          hint='Congenital Disorder'
          value={item?.congenitalDisorder}
        />
        <ProfileIconSetting
          style={styles.profileSetting}
          hint='Drug'
          icon={MedicineBoxIcon}
          onPress={onDrugIconPress}
        />
        <ProfileIconSetting
          style={styles.profileSetting}
          hint='Drug allergy'
          icon={MedicineBoxIcon}
          onPress={onDrugAllergyIconPress}
        />
        <ProfileIconSetting
          style={styles.profileSetting}
          hint='Food allergy'
          icon={FoodIcon}
          onPress={onFoodAllergyIconPress}
        />
        <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
          <View style={styles.childrenModalCantainer}>
            <Spinner/>
          </View>
        </Modal>
        <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
          <View style={styles.errorModalCantainer}>
            <Text style={styles.childrenModalText}>{ errorMessage }</Text>
            <Divider />
            <Text 
              style={[ 
                styles.childrenModalText, 
                { 
                  color: MainTheme.colorSecondary, 
                  backgroundColor: MainTheme.buttonColor, 
                  borderBottomLeftRadius: 5,
                  borderBottomRightRadius: 5 }
                ]}
                onPress={() => setErrorMessage('')}>
                OK
            </Text>
          </View>
        </Modal>
      </ScrollView>
    </SafeAreaLayout>
  )

}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  contentContainer: {
    paddingBottom: 24,
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  profileSetting: {
    padding: 16,
    backgroundColor: MainTheme.colorPrimary
  },
  section: {
    marginTop: 24,
  },
  // editButton: {
  //   marginHorizontal: 24,
  //   marginTop: 24,
  //   backgroundColor: MainTheme.buttonColor,
  //   borderWidth: 0
  // },
  imageButtonContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    marginTop: 15,
    alignItems: 'center', 
    backgroundColor: MainTheme.colorPrimary 
  },
  imageButton: {
    aspectRatio: 1.0,
    width: Dimensions.get('window').width / 3 - 24
  },
  editContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  editButton: {
    flexDirection: 'row-reverse',
    paddingHorizontal: 5,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
})