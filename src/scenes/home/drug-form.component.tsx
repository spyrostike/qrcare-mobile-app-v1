import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Icon, Divider, Layout, Spinner, Text } from '@ui-kitten/components';
import { DrugFormScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { DrugFormData, DrugFormSchema } from '../../data/drug-form.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { PhotoChooser } from '../../components/photo-chooser.component';
import { useStoreActions, useStoreState } from '../../store';
import { FileProps } from '../../store/models/file';
import { FormInput } from '../../components/form-input.component';
import { DrugProps } from '../../store/models/drug';

export const DrugFormScreen = (props: DrugFormScreenProps): SafeAreaLayoutElement => {
  const action = props.route.params.action
  const title = `Drug Form - ${ action.charAt(0).toUpperCase() + action.slice(1) }` 
  const creatureId = props.route.params?.creatureId ? props.route.params.creatureId : ''
  const drugId = props.route.params.drugId as string;

  const [image, setImage] = React.useState<FileProps>();
  const [imageThumbnail, setImageThumbnail] = React.useState<FileProps>();
  const [oldImageId, setOldImageId] = React.useState<string>('');
  const [oldImageThumbnailId, setOldImageThumbnailId] = React.useState<string>('');
  const [isImageChange, setIsImageChange] = React.useState<boolean>(false)
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorImageMessage, setErrorImageMessage] = React.useState<string>('');

  const add = useStoreActions(a => a.drug.add)
  const edit = useStoreActions(a => a.drug.edit)
  const setItem = useStoreActions(a => a.drug.setItem)
  const item = useStoreState(s => s.drug.item)

  React.useMemo(() => {
    if (action === 'edit') {
      setImage({ id: item?.imageId ? item?.imageId : null } as FileProps)
      setImageThumbnail({ id: item?.imageThumbnailId ? item?.imageThumbnailId : null } as FileProps)
      setOldImageId(item?.imageId as string)
      setOldImageThumbnailId(item?.imageThumbnailId as string)
    } else {
      setItem({} as DrugProps)
    }
  }, [])

  const onFormSubmit = async (values: DrugFormData) => {
    try {
      let validate = true
      setErrorImageMessage('')
      setErrorMessage('')

      if (typeof image?.id === 'undefined' || image?.id === null) { 
        setErrorImageMessage('Image is required.'); 
        validate = false 
      }

      if (!validate) return 

      const data = {
        id: drugId,
        name: values.name,
        imageId: image?.id,
        imageThumbnailId: imageThumbnail?.id,
        oldImageId: isImageChange ? oldImageId : null,
        oldImageThumbnailId: isImageChange ? oldImageThumbnailId : null,
        description: values.description,
        creatureId: creatureId
      }

      setIsLoading(true)
      
      if (action === 'add') {
        await add(data as DrugProps)
        setIsLoading(false)
        navigateBack()
      } else if (action === 'edit') {
        await edit(data as DrugProps)
        setIsLoading(false)
        navigateBack()
      }
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  };

  const navigateBack = (): void => {
    props.navigation.goBack();
  };

  const onImageChange = async(images: FileProps[]) => {
    setErrorImageMessage('')

    try {
      setImage(images[0])
      setImageThumbnail(images[1])
      setIsImageChange(true)
    } catch (error) {
      setErrorImageMessage(error)
    }
  }

  const renderForm = (props: FormikProps<DrugFormData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <PhotoChooser 
            source={item?.image}
            thumbnail={item?.imageThumbnail} 
            onChange={onImageChange} 
            error={errorImageMessage}
            nameUpload='drug'
            tagUpload='drug' 
        />
        <FormInput
          id='name'
          label='Food name'
          style={styles.input}
          defaultValue={item?.name}
        />
        <FormInput
          id='description'
          label='Description'
          style={[ styles.input , { alignItems: 'flex-start' } ]}
          defaultValue={item?.description}
          textStyle={{ height: 150, textAlignVertical: 'top', }}
          numberOfLines={10}
          multiline={true}
          maxLength={1500}
        />
         {
          errorMessage !== '' &&
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
              <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorMessage }</Text>
            </View>
          }
          {
            isLoading &&
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Spinner/>
            </View>
          }
      </Layout>
      <Button
        style={styles.saveButton}
        onPress={props.handleSubmit}
        disabled={isLoading}>
        Save
      </Button>
    </React.Fragment>
  );

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title={title}
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
          <Formik
            initialValues={
              item ? 
              new DrugFormData(item.name, item.description) 
              : 
              DrugFormData.empty()}
            validationSchema={DrugFormSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
      </KeyboardAvoidingView>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    flex: 1,
    justifyContent: 'center',
    // aspectRatio: 1.0,
    height: 150
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  }
});