import React from 'react';
import {
   Dimensions, 
   ListRenderItemInfo, 
   StyleSheet, 
   TouchableHighlight, 
   View 
} from 'react-native';
import { 
  Button, 
  Divider, 
  Icon, 
  ListItemElement, 
  LayoutElement, 
  Input, 
  Modal,
  Spinner, 
  Text,
  TopNavigationAction,
  TopNavigationActionElement
} from '@ui-kitten/components';
import {
  SafeAreaLayout,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import moment from 'moment';
import { VaccineBookInfoScreenProps } from '../../navigation/home.navigator';
import { AppRoute } from '../../navigation/app-routes';
import { Toolbar } from '../../components/toolbar.component';
import { MainTheme } from '../../constants/LOV';
import { BackIcon, EditIcon, PlusIcon, SearchIcon } from '../../assets/icons';
import { ProfileSetting } from '../../components/profile-setting.component';
import { MenuGridList } from '../../components/menu-grid-list.component';
import { useStoreActions, useStoreState } from '../../store';
import { VaccineBookProps } from '../../store/models/vaccine-book';
import { VaccineProps } from '../../store/models/vaccine';
import { CriteriaSearch } from '../../model/criteria-search.model';
import { ImageItemText } from '../../components/image-text-item.component';

export const VaccineBookInfoScreen = (props: VaccineBookInfoScreenProps): LayoutElement => {
  const creatureId = props.route.params.creatureId
  const vaccineBookId = props.route.params.vaccineBookId
  let checkItems: string[] = []

  const [textSearch, setTextSearch] = React.useState<string>('');
  const [offset, setOffset] = React.useState<number>(0);
  const [refreshing, setRefreshing] = React.useState<boolean>(false);
  const [checkboxVisabled, setCheckboxVisabled] = React.useState<boolean>(false);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const getInfo = useStoreActions(a => a.vaccineBook.getInfo)
  const setItem = useStoreActions(a => a.vaccineBook.setItem)
  const findVaccines = useStoreActions(a => a.vaccine.find)
  const setVaccineItems = useStoreActions(a => a.vaccine.setItems)
  const removeVaccineItems = useStoreActions(a => a.vaccine.removeItems)
  const item = useStoreState(s => s.vaccineBook.item)
  const vaccineItems = useStoreState(s => s.vaccine.items)

  const getVaccineItems = async (criteria) => {
    try {
      setRefreshing(true)
      const res = await findVaccines(criteria as CriteriaSearch)
      setRefreshing(false)
    } catch (error) {
      setRefreshing(false)
    }
  }

  const getVaccineBookInfo = async () => {
    try {
      setItem({} as VaccineBookProps)
      setIsLoading(true)
      const res =  await getInfo(vaccineBookId)
      setIsLoading(false)
      setItem(res)
      res && getVaccineItems({ vaccineBookId: vaccineBookId, textSearch: textSearch, offset: offset })
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  React.useMemo(() => {
    setVaccineItems([])
    getVaccineBookInfo()
  }, [])

  const onPlusButtonPress = (): void => {
    props.navigation.navigate(AppRoute.VACCINE_FORM, { action: 'add', creatureId: creatureId, vaccineBookId: vaccineBookId })
  }

  const onEditButtonPress = (): void => {
    props.navigation.navigate(AppRoute.VACCINE_BOOK_FORM, { action: 'edit', vaccineBookId: vaccineBookId })
  }
  
  const editIconRender = (): TopNavigationActionElement => (
    <TopNavigationAction
      icon={EditIcon}
      onPress={onEditButtonPress}
    />
  )
  
  const onTrashButtonPress = async () => {
    try {
      setCheckboxVisabled(false)
      setRefreshing(true)
      await removeVaccineItems(checkItems)
      checkItems = []
      setRefreshing(false)
      onRefresh()
    } catch (error) {
      setRefreshing(false)
      setErrorMessage(error)
    }
  }

  const onSearchIconPress = (): void => {
    setCheckboxVisabled(false)
    setVaccineItems([])
    getVaccineItems({ vaccineBookId: vaccineBookId, textSearch: textSearch, offset: 0 })
    setOffset(0)
  }

  const rightControls = (): TopNavigationActionElement[] => [
    editIconRender()
  ]
  
  const onItemPress = (index: number): void => {
    const id: string = typeof vaccineItems[index]?.id !== 'undefined' && vaccineItems[index]?.id !== null ? vaccineItems[index].id as string : ''
    props.navigation.navigate(AppRoute.VACCINE_INFO, { vaccineId: id })
  };

  const onScroll = async () => {
    try {
      setRefreshing(true)
      await findVaccines({ vaccineBookId: vaccineBookId, textSearch: textSearch, offset: offset + 1 } as CriteriaSearch)
      setOffset(offset + 1)
      setRefreshing(false)
    } catch (error) {
      setRefreshing(false)
    }
  }

  const onRefresh = (): void => {
    setCheckboxVisabled(false)
    setVaccineItems([])
    getVaccineItems({ vaccineBookId: vaccineBookId, textSearch: textSearch, offset: 0 })
    setOffset(0)
  }

  const onCheckChange = (checked: boolean, index: number): void => {
    if (checked) { checkItems.push(vaccineItems[index].id) } 
    else {checkItems = checkItems.filter(item => item !== vaccineItems[index].id)}
  }

  const renderItem = (info: ListRenderItemInfo<VaccineProps>): ListItemElement => (
    <TouchableHighlight 
      style={styles.item} 
      onPress={() => onItemPress(info.index)}
      onLongPress={() => setCheckboxVisabled(true)} 
    >
      <ImageItemText
        index={info.index}
        style={{ flex: 1, justifyContent: 'center' }}
        source={require('../../assets/images/square_button.png')}
        checkboxVisabled={checkboxVisabled}
        onCheckChange={onCheckChange}>
          <View style={styles.absoluteContainer}>
            <Text
              style={styles.itemTitle}
              category='s2'>
              {info.item.hospitalName}
            </Text>
            <Text
              style={styles.itemTitle}
              category='s2'>
              {moment(info.item.date).format('DD/MM/YYYY')}
            </Text>
          </View>
        
      </ImageItemText>
    </TouchableHighlight>
  );

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title='Vaccine Book Info'
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={BackIcon}
        onBackPress={props.navigation.goBack}
        rightControls={rightControls()}
        style={styles.toolbar}
        />
      <Divider />
      <ProfileSetting
        style={styles.profileSetting}
        hint='Vaccine'
        value={item?.vaccineTypeName}
      />
      {
        item?.vaccineType === 14 &&
        <ProfileSetting
          style={styles.profileSetting}
          hint='Vaccine Other'
          value={item?.vaccineTypeOther}
        />
      }
      <View style={{ flexDirection: 'row', paddingVertical: 16, padding: 8 }} >
        <Input
          style={styles.searchInput}
          placeholder='Search...'
          onChangeText={setTextSearch}
          onIconPress={onSearchIconPress}
          icon={SearchIcon}
        />
        <Button
          style={[ styles.iconButton ]}
          icon={PlusIcon}
          onPress={onPlusButtonPress} 
        />
        {
          checkboxVisabled === true &&
          <Button
            style={[ styles.iconButton, styles.trashButton ]}
            icon={(style) => (<Icon {...style} name='trash-can-outline' pack='material-community' />)}
            onPress={onTrashButtonPress} 
          />
        }
      </View>
      <MenuGridList
        data={vaccineItems}
        renderItem={renderItem}
        style={{ backgroundColor: MainTheme.colorPrimary }}
        refreshing={refreshing}
        onRefresh={onRefresh}
        onScroll={onScroll}
      />
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  )

}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  contentContainer: {
    paddingBottom: 24,
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  profileSetting: {
    padding: 16,
    backgroundColor: MainTheme.colorPrimary
  },
  section: {
    marginTop: 24,
  },
  imageButtonContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    marginTop: 15,
    alignItems: 'center', 
    backgroundColor: MainTheme.colorPrimary 
  },
  imageButton: {
    aspectRatio: 1.0,
    width: Dimensions.get('window').width / 3 - 24
  },
  editContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  iconButton: {
    width: 24,
    height: 24,
    borderRadius: 24,
    marginHorizontal: 8
  },
  trashButton: {
    backgroundColor: MainTheme.colorDanger,
    borderWidth: 0
  },
  descriptionContainer: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,  
    padding: 16,
  },
  itemTitle: {
    alignSelf: 'center',
    color: MainTheme.colorSecondary
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    aspectRatio: 1.0,
    margin: 8,
    maxWidth: Dimensions.get('window').width / 2 - 24,
    borderColor: MainTheme.itemBackGround
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  },
  searchInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  absoluteContainer: { 
    flex: 1, 
    position: 'absolute', 
    justifyContent: 'center', 
    alignItems: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  }
})