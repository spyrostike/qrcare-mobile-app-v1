import React from 'react';
import { Dimensions, Image, ListRenderItemInfo, StyleSheet, TouchableHighlight, View } from 'react-native';
import { 
  Button, 
  Divider, 
  Input, 
  ListItemElement, 
  Modal,
  Text,
  TopNavigationAction,
  TopNavigationActionElement
} from '@ui-kitten/components';
import moment from 'moment';
import { TreatmentHistoryListScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { BackIcon, PaperPlaneIcon, PlusIcon, SearchIcon, TrashIcon } from '../../assets/icons';
import { MenuGridList } from '../../components/menu-grid-list.component';
import { AppRoute } from '../../navigation/app-routes';
import { ImageItemText } from '../../components/image-text-item.component';
import { MainTheme } from '../../constants/LOV';
import { useStoreActions, useStoreState } from '../../store';
import { TreatmentHistoryProps } from '../../store/models/treatment-history';
import { CriteriaSearch } from '../../model/criteria-search.model';

export const TreatmentHistoryListScreen = (props: TreatmentHistoryListScreenProps): SafeAreaLayoutElement => {
  const creatureId = props.route.params.creatureId
  let checkItems: string[] = []

  const [textSearch, setTextSearch] = React.useState<string>('');
  const [offset, setOffset] = React.useState<number>(0);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [checkboxVisabled, setCheckboxVisabled] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const find = useStoreActions(a => a.treatmentHistory.find)
  const setItems = useStoreActions(a => a.treatmentHistory.setItems)
  const removeItems = useStoreActions(a => a.treatmentHistory.removeItems)
  const items = useStoreState(s => s.treatmentHistory.items)

  const getTreatmentHistoryItems = async (criteria) => {
    try {
      setIsLoading(true)
      const res = await find(criteria as CriteriaSearch)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
    }
  }

  React.useMemo(() => {
    setItems([])
    getTreatmentHistoryItems({ creatureId: creatureId, textSearch: textSearch, offset: offset })
  }, [])

  const onSearchIconPress = (): void => {
    setCheckboxVisabled(false)
    setItems([])
    getTreatmentHistoryItems({ creatureId: creatureId, textSearch: textSearch, offset: 0 })
    setOffset(0)
  }
  
  const onPlusButtonPress = (): void => {
    if (checkboxVisabled === true) return
    props.navigation.navigate(AppRoute.TREATMENT_HISTORY_FORM, { action: 'add', creatureId: creatureId  })
  }

  const onTrashButtonPress = async () => {
    try {
      setCheckboxVisabled(false)
      setIsLoading(true)
      await removeItems(checkItems)
      checkItems = []
      setIsLoading(false)
      onRefresh()
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  const onPaperPlaneButtonPress = (): void => {
    setCheckboxVisabled(false)
    if (checkItems.length === 0 ) return
    props.navigation.navigate(AppRoute.TREATMENT_HISTORY_EMAIL_FORM, { creatureId: creatureId, treatmentHistoryIds: checkItems })
  }  

  const onCheckChange = (checked: boolean, index: number): void => {
    if (checked) { checkItems.push(items[index].id) } 
    else {checkItems = checkItems.filter(item => item !== items[index].id)}
  }

  const onItemPress = (index: number): void => {
    const id: string = typeof items[index]?.id !== 'undefined' && items[index]?.id !== null ? items[index].id as string : ''
    props.navigation.navigate(AppRoute.TREATMENT_HISTORY_INFO, { treatmentHistoryId: id })
  };

  const onScroll = async () => {
    try {
      setIsLoading(true)
      await find({ creatureId: creatureId, textSearch: textSearch, offset: offset + 1 } as CriteriaSearch)
      setOffset(offset + 1)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
    }
  };

  const onRefresh = (): void => {
    setCheckboxVisabled(false)
    setItems([])
    getTreatmentHistoryItems({ creatureId: creatureId, textSearch: textSearch, offset: 0 })
    setOffset(0)
  }

  const plusIconRender: TopNavigationActionElement = (
    <TopNavigationAction
      icon={PlusIcon}
      onPress={onPlusButtonPress}
    />
  )

  const PaperPlaneIconRender: TopNavigationActionElement = (
    <TopNavigationAction
      icon={PaperPlaneIcon}
      onPress={onPaperPlaneButtonPress}
    />
  )

  const trashIconRender: TopNavigationActionElement = (
    <TopNavigationAction
      icon={TrashIcon}
      onPress={onTrashButtonPress}
    />
  )

  let rightControls: TopNavigationActionElement[] = [
    plusIconRender,
    PaperPlaneIconRender,
    trashIconRender
  ]
  
  if (checkboxVisabled === false) rightControls = rightControls.slice(0, 1) 

  const renderItem = (info: ListRenderItemInfo<TreatmentHistoryProps>): ListItemElement => (
    <TouchableHighlight 
      style={styles.item} 
      onPress={() => onItemPress(info.index)}
      onLongPress={() => setCheckboxVisabled(true)} 
    >
      <ImageItemText
        index={info.index}
        style={{ flex: 1, justifyContent: 'center' }}
        source={require('../../assets/images/square_button.png')}
        checkboxVisabled={checkboxVisabled}
        onCheckChange={onCheckChange}>
          <View style={styles.absoluteContainer}>
            <Text
              style={styles.itemTitle}
              category='s2'>
              {info.item.departmentName}
            </Text>
            <Text
              style={styles.itemTitle}
              category='s2'>
              {info.item.fileCategory === 5 ? info.item.fileCategoryOther : info.item.fileCategoryName}
            </Text>
            <Text
              style={styles.itemTitle}
              category='s2'>
              {moment(info.item.date).format('DD/MM/YYYY')}
            </Text>
          </View>
      </ImageItemText>
    </TouchableHighlight>
  );

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title='Treatment History'
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={BackIcon}
        onBackPress={props.navigation.goBack}
        rightControls={rightControls}
        style={styles.toolbar}
      />
      <Divider/>
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', paddingHorizontal: 8, paddingVertical: 16 }} >
          <Input
            style={styles.searchInput}
            placeholder='Search...'
            value={textSearch}
            onChangeText={setTextSearch}
            onIconPress={onSearchIconPress}
            icon={SearchIcon}
          />
        </View>
        <Divider/>
        <MenuGridList
          data={items}
          renderItem={renderItem}
          onItemPress={onItemPress}
          style={{ backgroundColor: MainTheme.colorPrimary }}
          refreshing={isLoading}
          onRefresh={onRefresh}
          onScroll={onScroll}
        />
      </View>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  )
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    aspectRatio: 1.0,
    margin: 8,
    maxWidth: Dimensions.get('window').width / 2 - 24,
    // backgroundColor: MainTheme.itemBackGround,
    borderColor: MainTheme.itemBackGround
  },
  itemTitle: {
    alignSelf: 'center',
    color: MainTheme.colorSecondary
  },
  searchInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  scanButton: { 
    paddingHorizontal: 0,
  },
  iconButton: {
    width: 24,
    height: 24,
    backgroundColor: MainTheme.buttonColor,
    borderColor: MainTheme.buttonColor
  },
  plusButton: {
    borderRadius: 24,
    marginHorizontal: 8,
  },
  messageInput: {
    flex: 1,
    marginHorizontal: 8,
  },
  absoluteContainer: { 
    flex: 1, 
    position: 'absolute', 
    justifyContent: 'center', 
    alignItems: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  }
});
