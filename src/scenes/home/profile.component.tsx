import React from 'react';
import { StyleSheet, View } from 'react-native';
import { 
  Button, 
  Datepicker, 
  Divider, 
  Icon, 
  Modal,
  Layout, 
  Select, 
  SelectOptionType, 
  Spinner, 
  Text 
} from '@ui-kitten/components';
import { ProfileScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { MemberProfileFormData, MemberProfileFormSchema } from '../../data/member-profile-form.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { PhotoChooser } from '../../components/photo-chooser.component';
import { Map } from '../../components/map.component'
import { GeoCoordinates } from 'react-native-geolocation-service';
import { PhotoChooserBox } from '../../components/photo-chooser-box.component';
import { FileProps } from 'src/store/models/file';
import { useStoreActions, useStoreState } from '../../store';
import { FormInput } from '../../components/form-input.component';
import { MemberProps } from '../../store/models/member';

export const ProfileScreen = (props: ProfileScreenProps): SafeAreaLayoutElement => {
  const now = new Date();
  const yesterday = new Date(now.getFullYear() - 50, now.getMonth(), now.getDate() - 1);
  const tomorrow = new Date(now.getFullYear() + 50, now.getMonth(), now.getDate() + 1);

  const genderOptions = [
    { text: '-- Select --', value: null, disabled: true },
    { text: 'Male', value: 1 },
    { text: 'Female', value: 2 }
  ]

  const bloodTypeOptions = [
    { text: '-- Select --', value: null, disabled: true },
    { text: 'AB', value: 1 },
    { text: 'A', value: 2 },
    { text: 'B', value: 3 },
    { text: 'O', value: 4 }
  ]

  const [profileImage, setProfileImage] = React.useState<FileProps>();
  const [profileImageThumbnail, setProfileImageThumbnail] = React.useState<FileProps>();
  const [locationImage, setLocationImage] = React.useState<FileProps>();
  const [locationImageThumbnail, setLocationImageThumbnail] = React.useState<FileProps>();
  const [location, setLocation] = React.useState<GeoCoordinates>();
  const [errorLocationMessage, setErrorLocationMessage] = React.useState<string>('');
  const [bloodTypeSelected, setBloodTypeSelected] = React.useState<SelectOptionType>(bloodTypeOptions[0]);
  const [genderSelected, setGenderSelected] = React.useState<SelectOptionType>(genderOptions[0]);
  const [birthDate, setBirthDate] = React.useState<Date>(new Date);
  const [oldProfileImageId, setOldProfileImageId] = React.useState<string>('');
  const [oldProfileImageThumbnailId, setOldProfileImageThumbnailId] = React.useState<string>('');
  const [oldLocationImageId, setOldLocationImageId] = React.useState<string>('');
  const [oldLocationImageThumbnailId, setOldLocationImageThumbnailId] = React.useState<string>(''); 
  const [errorImageProfileMessage, setErrorImageProfileMessage] = React.useState<string>('');
  const [errorImageLocationMessage, setErrorImageLocationMessage] = React.useState<string>('');
  const [errorGenderMessage, setErrorGenderMessage] = React.useState<string>('');
  const [errorBloodTypeMessage, setErrorBloodTypeMessage] = React.useState<string>('');
  const [isProfileImageChange, setIsProfileImageChange] = React.useState<boolean>(false)
  const [isLocationImageChange, setIsLocationImageChange] = React.useState<boolean>(false)
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const edit = useStoreActions(a => a.member.edit)
  const getInfo = useStoreActions(a => a.member.getInfo)
  const getItem = useStoreActions(a => a.member.setItem)
  const item = useStoreState(a => a.member.item)

  React.useMemo(() => {
    console.log('item', item)
    const getMemberInfo = () => {
      const genderId = item?.gender !== undefined ? parseInt(item?.gender) : 0
      const bloodTypeId = item?.bloodType !== undefined ? parseInt(item?.bloodType) : 0
      setProfileImage({ id: item?.profileImageId ? item?.profileImageId : null } as FileProps)
      setProfileImageThumbnail({ id: item?.profileImageThumbnailId ? item?.profileImageThumbnailId : null } as FileProps)
      setLocationImage({ id: item?.locationImageId ? item?.locationImageId : null } as FileProps)
      setLocationImageThumbnail({ id: item?.locationImageThumbnailId ? item?.locationImageThumbnailId : null } as FileProps)
      setOldProfileImageId(item?.profileImageId as string)
      setOldProfileImageThumbnailId(item?.profileImageThumbnailId as string)
      setOldLocationImageId(item?.locationImageId as string)
      setOldLocationImageThumbnailId(item?.locationImageThumbnailId as string)
      setGenderSelected(genderOptions[genderId])
      setBloodTypeSelected(bloodTypeOptions[bloodTypeId]) 
      setBirthDate(item?.birthDate !== undefined ? new Date(item?.birthDate.toString()) : new Date)
      setLocation({ latitude: item?.placeLatitude as number, longitude: item?.placeLongitude as number } as GeoCoordinates)
    }

    getMemberInfo()

  }, [])
  
  const onFormSubmit = async (values: MemberProfileFormData) => {
    try {
      const bloodType: any = bloodTypeSelected
      const gender: any = genderSelected
      let validate = true

      setErrorGenderMessage('')
      setErrorBloodTypeMessage('')
      setErrorImageProfileMessage('')
      setErrorImageLocationMessage('')

      if (typeof profileImage?.id === 'undefined' || profileImage?.id === null) { 
        setErrorImageProfileMessage('Profile Image is required.'); 
        validate = false 
      }

      if (typeof locationImage?.id === 'undefined' || locationImage?.id === null) { 
        setErrorImageLocationMessage('Location Image is required.'); 
        validate = false 
      }

      if (gender.value === null) { setErrorGenderMessage('Gender is required.'); validate = false }
      if (bloodType.value === null) { setErrorBloodTypeMessage('Blood Type is required.'); validate = false }

      if (!validate) return 

      const data = {
        id: '',
        code: '',
        username: '',
        password: '',
        firstName: values.firstName,
        lastName: values.lastName,
        email: values.email,
        gender: gender.value as any,
        bloodType: bloodType.value as any,
        birthDate: new Date(birthDate.getFullYear(), birthDate.getMonth(), birthDate.getDate() + 1),
        relationship: values.relationship,
        identificationNo: values.idCardNumber,
        contactNo1: values.contactNumber1,
        contactNo2: values.contactNumber2,
        address1: values.address1,
        address2: values.address2,
        address3: values.address3,
        profileImageId: profileImage?.id,
        profileImageThumbnailId: profileImageThumbnail?.id,
        locationImageId: locationImage?.id,
        locationImageThumbnailId: locationImageThumbnail?.id,
        placeLatitude: location?.latitude,
        placeLongitude: location?.longitude,
        oldProfileImageId: isProfileImageChange ? oldProfileImageId : null,
        oldProfileImageThumbnailId: isProfileImageChange ? oldProfileImageThumbnailId : null,
        oldLocationImageId: isLocationImageChange ? oldLocationImageId : null,
        oldLocationImageThumbnailId: isLocationImageChange ? oldLocationImageThumbnailId : null
      }
      setIsLoading(true)
      await edit(data as MemberProps)
      const res = await getInfo()
      setIsLoading(false)
      getItem(res)
      navigateBack()
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  };

  const onBloodTypeSelect = (option): void => {
    setBloodTypeSelected(option)
  }

  const onGenderSelect = (option): void => {
    setGenderSelected(option)
  }

  const navigateBack = (): void => {
    props.navigation.goBack();
  }
  
  const onLocationChange = (position: GeoCoordinates): void => {
    setErrorLocationMessage('')
    setLocation(position)
  }

  const onLocationImageChange = async(images: FileProps[]) => {
    setErrorImageLocationMessage('')
    try {
      setLocationImage(images[0])
      setLocationImageThumbnail(images[1])
      setIsLocationImageChange(true)
    } catch (error) {
      setErrorImageLocationMessage(error)
    }
  }

  const onProfileChange = async(images: FileProps[]) => {
    setErrorImageProfileMessage('')

    try {
      setProfileImage(images[0])
      setProfileImageThumbnail(images[1])
      setIsProfileImageChange(true)
    } catch (error) {
      setErrorImageProfileMessage(error)
    }
  }

  const renderForm = (props: FormikProps<MemberProfileFormData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <PhotoChooser 
            source={item?.profileImage}
            thumbnail={item?.profileImage} 
            onChange={onProfileChange} 
            error={errorImageProfileMessage}
            nameUpload='member-profile'
            tagUpload='member-profile' 
        />
        <FormInput
          id='firstName'
          label='First name'
          style={styles.input}
          defaultValue={item?.firstName}
        />
        <FormInput
          id='lastName'
          label='Last name'
          style={styles.input}
          defaultValue={item?.lastName}
        />
        <FormInput
          id='email'
          label='Email'
          style={styles.input}
          defaultValue={item?.email}
          disabled
        />
        <Select
          style={styles.input}
          label='Gender'
          data={genderOptions}
          selectedOption={genderSelected}
          onSelect={onGenderSelect}
        />
        {
          errorGenderMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorGenderMessage }</Text>
          </View>
        }
        <Select
          style={styles.input}
          label='Blood type'
          data={bloodTypeOptions}
          selectedOption={bloodTypeSelected}
          onSelect={onBloodTypeSelect}
        />
        {
          errorBloodTypeMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorBloodTypeMessage }</Text>
          </View>
        }
        <Datepicker
          style={styles.input}
          label='Birth Date'
          date={birthDate}
          onSelect={setBirthDate}
          min={yesterday}
          max={tomorrow}
          boundingMonth={false}
        />
        <FormInput
          id='relationship'
          label='Relationship'
          style={styles.input}
          defaultValue={item?.relationship}
        />
        <FormInput
          id='idCardNumber'
          label='ID Card Number'
          style={styles.input}
          defaultValue={item?.identificationNo}
        />
        <FormInput
          id='contactNumber1'
          label='Contact Number'
          style={styles.input}
          defaultValue={item?.contactNo1}
        />
        <FormInput
          id='contactNumber2'
          label='Contact Number'
          style={styles.input}
          defaultValue={item?.contactNo2}
        />
        <FormInput
          id='address1'
          label='Address'
          style={styles.input}
          defaultValue={item?.address1}
        />
        <FormInput
          id='address2'
          label='Address'
          style={styles.input}
          defaultValue={item?.address2}
        />
        <FormInput
          id='address3'
          label='Address'
          style={styles.input}
          defaultValue={item?.address3}
        />
        <Map 
          location={{ 
            latitude: item?.placeLatitude as number, 
            longitude: item?.placeLongitude as number
          } as GeoCoordinates}
          onLocationChange={onLocationChange} 
          error={errorLocationMessage} 
        />
        <PhotoChooserBox 
          hint='Location Image' 
          source={item?.locationImage}
          thumbnail={item?.locationImageThumbnail}
          nameUpload='member-location' 
          tagUpload='member-location'
          error={errorImageLocationMessage}
          onChange={onLocationImageChange} />
      </Layout>
      <Button
        style={styles.saveButton}
        onPress={props.handleSubmit}>
        Save
      </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title='Profile'
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
          <Formik
            initialValues={
              item ? 
                  new MemberProfileFormData(
                    item.firstName, 
                    item.lastName, 
                    item.relationship, 
                    item.identificationNo, 
                    item.email, 
                    item.contactNo1, 
                    item.contactNo2, 
                    item.address1, 
                    item.address2,
                    item.address3,
                  ) 
                  : 
                  MemberProfileFormData.empty()}
            validationSchema={MemberProfileFormSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
      </KeyboardAvoidingView>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    flex: 1,
    justifyContent: 'center',
    // aspectRatio: 1.0,
    height: 150
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
  mapContainer: {
    flex: 1,
    // aspectRatio: 1.0,
    height: 180,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  map: {
    height: 130,
    backgroundColor: MainTheme.colorSecondary,
  },
  mapButton: {
    backgroundColor: MainTheme.buttonColor,
    height: 50,
    borderWidth: 0
    
  },
  mapItemFooter: {
    height: 50,
    marginVertical: -30,
    marginHorizontal: -24,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
});