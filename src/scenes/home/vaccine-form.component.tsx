import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Datepicker, Divider, Input, Layout, Spinner, Text, Icon } from '@ui-kitten/components';
import { VaccineFormScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { ConfigData, ConfigSchema } from '../../data/config.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon, CameraIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { PhotoChooser } from '../../components/photo-chooser.component';
import { useStoreActions, useStoreState } from '../../store';
import { FileProps } from '../../store/models/file';
import { VaccineProps } from '../../store/models/vaccine';

export const VaccineFormScreen = (props: VaccineFormScreenProps): SafeAreaLayoutElement => {
  const action = props.route.params.action
  const title = `Vaccine Form - ${ action.charAt(0).toUpperCase() + action.slice(1) }`
  const creatureId = props.route.params.creatureId;
  const vaccineBookId = props.route.params.vaccineBookId;
  const vaccineId = props.route.params.vaccineId;

  const [image, setImage] = React.useState<FileProps>();
  const [imageThumbnail, setImageThumbnail] = React.useState<FileProps>();
  const [oldImageId, setOldImageId] = React.useState<string>('');
  const [oldImageThumbnailId, setOldImageThumbnailId] = React.useState<string>('');
  const [isImageChange, setIsImageChange] = React.useState<boolean>(false);
  const [hospital, setHospital] = React.useState<string>('');
  const [date, setDate] = React.useState<Date>(new Date);
  const [description, setDescription] = React.useState<string>('');
  const [errorImageMessage, setErrorImageMessage] = React.useState<string>('');
  const [errorHospitalMessage, setErrorHospitalMessage] = React.useState<string>('');
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const add = useStoreActions(a => a.vaccine.add)
  const edit = useStoreActions(a => a.vaccine.edit)
  const setItem = useStoreActions(a => a.vaccine.setItem)
  const item = useStoreState(s => s.vaccine.item)

  React.useMemo(() => {
    if (action === 'edit') {
      setImage({ id: item?.imageId ? item?.imageId : null } as FileProps)
      setImageThumbnail({ id: item?.imageThumbnailId ? item?.imageThumbnailId : null } as FileProps)
      setOldImageId(item?.imageId as string)
      setOldImageThumbnailId(item?.imageThumbnailId as string)
      setHospital(item?.hospitalName as string)
      setDate(item?.date !== undefined ? new Date(item?.date.toString()) : new Date)
      setDescription(item?.description as string)
    } else {
      setItem({} as VaccineProps)
    }
  }, [])

  const onFormSubmit = (values: ConfigData): void => { };

  const onSaveButtonPress = async () => {
    try {
      let validate = true
      setErrorImageMessage('')
      setErrorHospitalMessage('')
      setErrorMessage('')

      if (typeof image?.id === 'undefined' || image?.id === null) { 
        setErrorImageMessage('Image is required.'); 
        validate = false 
      }
      if (hospital === '') {setErrorHospitalMessage('Hospital is required.'); validate = false }

      if (!validate) return

      const data = {
        id: vaccineId,
        hospitalName: hospital,
        date: new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1),
        vaccineBookId: vaccineBookId,
        description: description,
        imageId: image?.id,
        imageThumbnailId: imageThumbnail?.id,
        oldImageId: isImageChange ? oldImageId : null,
        oldImageThumbnailId: isImageChange ? oldImageThumbnailId : null,
        creatureId: creatureId
      }

      setIsLoading(true)
      if (action === 'add') {
        await add(data as VaccineProps)
        setIsLoading(false)
        navigateBack()
      } else if (action === 'edit') {
        await edit(data as VaccineProps)
        setIsLoading(false)
        navigateBack()
      }
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const onImageChange = async(images: FileProps[]) => {
    setErrorImageMessage('')

    try {
      setImage(images[0])
      setImageThumbnail(images[1])
      setIsImageChange(true)
    } catch (error) {
      setErrorImageMessage(error)
    }
  }

  const renderForm = (props: FormikProps<ConfigData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <PhotoChooser 
          source={item?.image}
          thumbnail={item?.imageThumbnail} 
          onChange={onImageChange} 
          error={errorImageMessage}
          nameUpload='vaccine'
          tagUpload='vaccine' 
        />
        <Input
          label='Hospital'
          style={styles.input}
          value={hospital}
          onChangeText={setHospital}
        />
        {
          errorHospitalMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorHospitalMessage }</Text>
          </View>
        }
        <Datepicker
          style={styles.input}
          label='Date'
          date={date}
          onSelect={setDate}
          boundingMonth={false}
        />
        <Input
          label='Description'
          style={[ styles.input , { alignItems: 'flex-start' } ]}
          value={description}
          onChangeText={setDescription}
          textStyle={{ height: 150, textAlignVertical: 'top', }}
          numberOfLines={10}
          multiline={true}
          maxLength={1500}
        />
        {
          errorMessage !== '' &&
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
              <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorMessage }</Text>
            </View>
          }
          {
            isLoading &&
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Spinner/>
            </View>
          }
      </Layout>
      <Button
        style={styles.saveButton}
        onPress={onSaveButtonPress}>
        Save
      </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title={title}
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
          <Formik
            initialValues={ConfigData.empty()}
            validationSchema={ConfigSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
      </KeyboardAvoidingView>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
    paddingTop: 24
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    flex: 1,
    justifyContent: 'center',
    // aspectRatio: 1.0,
    height: 150
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  }
});