import React from 'react';
import { Dimensions, Image, ScrollView, StyleSheet, TouchableWithoutFeedback, View } from 'react-native';
import { 
  Divider, 
  Layout, 
  LayoutElement, 
  Modal,
  Spinner,
  Text,
  TopNavigationAction, 
  TopNavigationActionElement 
} from '@ui-kitten/components';
import moment from 'moment';
import FastImage from 'react-native-fast-image';
import {
  SafeAreaLayout,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { CreatureInfoScreenProps } from '../../navigation/home.navigator';
import { AppRoute } from '../../navigation/app-routes';
import { Toolbar } from '../../components/toolbar.component';
import { MainTheme } from '../../constants/LOV';
import { BackIcon, EditIcon, FoodIcon, MedicineBoxIcon } from '../../assets/icons';
import { ProfileSetting } from '../../components/profile-setting.component';
import { ProfileIconSetting } from '../../components/profile-icon-setting.component';
import { useStoreActions, useStoreState } from '../../store';
import { ProfileAvatarPreview } from '../../components/profile-avatar-preview.component';
import { PhotoChooserBoxPreview } from '../../components/photo-chooser-box-preview.component';
import { CreatureProps } from '../../store/models/creature';

export const CreatureInfoScreen = (props: CreatureInfoScreenProps): LayoutElement => {
  const creatureId = props.route.params.creatureId

  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const getInfo = useStoreActions(a => a.creature.getInfo)
  const setItem = useStoreActions(a => a.creature.setItem)
  const item = useStoreState(s => s.creature.item)

  React.useMemo(() => {
    const getCretureInfo = async () => {
      try {
        setItem({} as CreatureProps)
        setIsLoading(true)
        const res = await getInfo(creatureId)
        setIsLoading(false)
        setItem(res)
      } catch (error) {
        setIsLoading(false)
        setErrorMessage(error)
      }
    }
    getCretureInfo()
  }, [])

  const onEditButtonPress = (): void => {
    props.navigation.navigate(AppRoute.CREATURE_FORM, { action: 'edit', creatureId: props.route.params.creatureId })
  }

  const rightControl: TopNavigationActionElement = (
    <TopNavigationAction
      icon={EditIcon}
      onPress={onEditButtonPress}
    />
  )

  const onDrugIconPress = (): void => {
    props.navigation.navigate(AppRoute.DRUG_LIST, { creatureId: creatureId })
  }
  
  const onDrugAllergyIconPress = (): void => {
    props.navigation.navigate(AppRoute.DRUG_ALLERGY_LIST, { creatureId: creatureId })
  }

  const onFoodAllergyIconPress = (): void => {
    props.navigation.navigate(AppRoute.FOOD_ALLERGY_LIST, { creatureId :creatureId })
  }

  const onMedicalImagePress = (): void => {
    props.navigation.navigate(AppRoute.MEDICAL_INFO, { creatureId: creatureId })
  }

  const onTreatmentImagePress = (): void => {
    props.navigation.navigate(AppRoute.TREATMENT_HISTORY_LIST, { creatureId: creatureId })
  }

  const onVaccineImagePress = (): void => {
    props.navigation.navigate(AppRoute.VACCINE_BOOK_LIST, { creatureId: creatureId })
  }
  
  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title='Creature Info'
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={props.navigation.goBack}
          rightControls={rightControl}
          style={styles.toolbar}
        />
      <Divider />
      <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <ProfileAvatarPreview
          style={[ styles.profileAvatar ]}
          source={item?.creatureImage}
          thumbnail={item?.creatureImageThumbnail}
          resizeMode={FastImage.resizeMode.cover}
        />
        <ProfileSetting
          style={[styles.profileSetting, styles.section]}
          hint='First Name'
          value={item?.firstName}
        />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Last Name'
          value={item?.lastName}
        />
        <PhotoChooserBoxPreview
          hint='Birth Certificate'
          source={item?.birthCertificate}
          thumbnail={item?.birthCertificateThumbnail}
          resizeMode={FastImage.resizeMode.cover} />
        <Divider />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Gender'
          value={item?.genderName}
        />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Blood Type'
          value={item?.bloodTypeName}
        />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Birth Date'
          value={moment(item?.birthDate).format('DD/MM/YYYY')}
        />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Weight'
          value={item?.weight}
        />
        <ProfileSetting
          style={[styles.profileSetting]}
          hint='Height'
          value={item?.height}
        />
        <ProfileIconSetting
          style={styles.profileSetting}
          hint='Drug'
          icon={MedicineBoxIcon}
          onPress={onDrugIconPress}
        />
        <ProfileIconSetting
          style={styles.profileSetting}
          hint='Drug allergy'
          icon={MedicineBoxIcon}
          onPress={onDrugAllergyIconPress}
        />
        <ProfileIconSetting
          style={styles.profileSetting}
          hint='Food allergy'
          icon={FoodIcon}
          onPress={onFoodAllergyIconPress}
        />
        <Layout style={styles.imageButtonContainer}>
          <TouchableWithoutFeedback onPress={onMedicalImagePress} >
            <Image
              resizeMode='contain'
              source={require('../../assets/images/medical.png')}
              style={styles.imageButton} />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={onTreatmentImagePress} >
            <Image
              resizeMode='contain'
              source={require('../../assets/images/treatment.png')}
              style={styles.imageButton} />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={onVaccineImagePress} >
            <Image
              resizeMode='contain'
              source={require('../../assets/images/vaccine.png')}
              style={styles.imageButton} />
          </TouchableWithoutFeedback>
        </Layout>
        <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
          <View style={styles.childrenModalCantainer}>
            <Spinner/>
          </View>
        </Modal>
        <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
          <View style={styles.errorModalCantainer}>
            <Text style={styles.childrenModalText}>{ errorMessage }</Text>
            <Divider />
            <Text 
              style={[ 
                styles.childrenModalText, 
                { 
                  color: MainTheme.colorSecondary, 
                  backgroundColor: MainTheme.buttonColor, 
                  borderBottomLeftRadius: 5,
                  borderBottomRightRadius: 5 }
                ]}
                onPress={() => setErrorMessage('')}>
                OK
            </Text>
          </View>
        </Modal>
      </ScrollView>
    </SafeAreaLayout>
  )

}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
    paddingTop: 24
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  contentContainer: {
    paddingBottom: 24,
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    borderRadius: 124 / 2,
    alignSelf: 'center', 
    backgroundColor: MainTheme.colorSecondary
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  profileSetting: {
    padding: 16,
    backgroundColor: MainTheme.colorPrimary
  },
  section: {
    marginTop: 24,
  },
  imageButtonContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    marginVertical: 15,
    alignItems: 'center', 
    backgroundColor: MainTheme.colorPrimary 
  },
  imageButton: {
    aspectRatio: 1.0,
    width: Dimensions.get('window').width / 3 - 24
  },
  editContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  editButton: {
    flexDirection: 'row-reverse',
    paddingHorizontal: 5,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
})