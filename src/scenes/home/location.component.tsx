import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Card, Divider, Icon, Layout, Text } from '@ui-kitten/components';
import { LocationScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { ConfigData, ConfigSchema } from '../../data/config.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon, CameraIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { Map } from '../../components/map.component'

export const LocationScreen = (props: LocationScreenProps): SafeAreaLayoutElement => {
  const onFormSubmit = (values: ConfigData): void => {
    
  };

  const onSaveButtonPress = (): void => {
    // props.navigation.navigate(AppRoute.CREATURE_FORM, { action: 'add', id: '1' })
  }

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const renderPhotoButton = (): React.ReactElement => (
    <Button
      style={styles.editAvatarButton}
      status='basic'
      icon={CameraIcon}
    />
  )
  
  const renderMap = (info: any): React.ReactElement => (
    <View style={styles.map}>
      <Map />
    </View>
  )

  const renderForm = (props: FormikProps<ConfigData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <Text
          style={{ fontSize: 12 }}
          appearance='hint'
          category='s1'>
          Location
        </Text>
        <Card
          style={styles.mapContainer}
          header={() => renderMap('sd')}
          // onPress={() => props.onItemPress(info.index)}
          >
          <View style={styles.mapItemFooter}>
            <Button
              style={styles.mapButton}
              onPress={onSaveButtonPress}>
              Try Again
            </Button>
          </View>
        </Card>
        <Text
          style={{ fontSize: 12 }}
          appearance='hint'
          category='s1'>
          Location Image
        </Text>
        <Card
          style={styles.takePhotoContainer}
          // onPress={() => props.onItemPress(info.index)}
          >
          <Icon name='camera' style={{ width: 30, height: 30, alignSelf: 'center' }} fill={MainTheme.colorPrimary} />
          <Text style={styles.takePhotoTitle} category='s2'>
            Take Picture
          </Text>
        </Card>
      </Layout>
      <Button
        style={styles.saveButton}
        onPress={onSaveButtonPress}>
        Save
      </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title='Profile'
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
          <Formik
            initialValues={ConfigData.empty()}
            validationSchema={ConfigSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
      </KeyboardAvoidingView>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    marginHorizontal: 24,
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    justifyContent: 'center',
    height: 250
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
  mapContainer: {
    height: 250,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  map: {
    height: 200,
    backgroundColor: MainTheme.colorSecondary,
  },
  mapButton: {
    backgroundColor: MainTheme.buttonColor,
    height: 50,
    borderWidth: 0
    
  },
  mapItemFooter: {
    height: 50,
    marginVertical: -30,
    marginHorizontal: -24,
  }
});