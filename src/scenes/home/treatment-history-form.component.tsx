import React from 'react';
import { StyleSheet, View } from 'react-native';
import { 
  Button, 
  Datepicker, 
  Divider, 
  Icon, 
  Input, 
  Layout, 
  Modal, 
  Select, 
  SelectOptionType, 
  Spinner, 
  Text 
} from '@ui-kitten/components';
import moment from 'moment';
import { TreatmentHistoryFormScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { ConfigData, ConfigSchema } from '../../data/config.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { PhotoChooser } from '../../components/photo-chooser.component';
import { FileProps } from '../../store/models/file';
import { useStoreActions, useStoreState } from '../../store';
import { TreatmentHistoryProps } from 'src/store/models/treatment-history';

export const TreatmentHistoryFormScreen = (props: TreatmentHistoryFormScreenProps): SafeAreaLayoutElement => {
  const action = props.route.params.action
  const title = `Treatment History Form - ${ action.charAt(0).toUpperCase() + action.slice(1) }` 
  const creatureId = props.route.params?.creatureId ? props.route.params.creatureId : ''
  const treatmentHistoryId = props.route.params.treatmentHistoryId as string;

  const now = new Date();
  const yesterday = new Date(now.getFullYear() - 50, now.getMonth(), now.getDate() - 1);
  const tomorrow = new Date(now.getFullYear() + 50, now.getMonth(), now.getDate() + 1);

  const departmentOptions = [
    { text: '-- Select --', value: null, disabled: true },
    { text: 'Medicine', value: 1 },
    { text: 'Ear nose throat', value: 2 },
    { text: 'Teeth', value: 3 },
    { text: 'Endocrine system', value: 4 },
    { text: 'Heart', value: 5 },
    { text: 'Lung', value: 6 },
    { text: 'Liver', value: 7 },
    { text: 'Kidney', value: 8 },
    { text: 'Bone & Joint', value: 9 }
  ]

  const fileCategoryOptions = [
    { text: '-- Select --', value: null, disabled: true },
    { text: '-Check up', value: 1 },
    { text: '-X ray', value: 2 },
    { text: '-MRI / CT scan', value: 3 },
    { text: '-Test  result', value: 4 },
    { text: '-Other', value: 5 }
  ]

  const [image, setImage] = React.useState<FileProps>();
  const [imageThumbnail, setImageThumbnail] = React.useState<FileProps>();
  const [oldImageId, setOldImageId] = React.useState<string>('');
  const [oldImageThumbnailId, setOldImageThumbnailId] = React.useState<string>('');
  const [isImageChange, setIsImageChange] = React.useState<boolean>(false)
  const [departmentSelected, setDepartmentSelected] = React.useState<SelectOptionType>(departmentOptions[0]);
  const [fileCategorySelected, setFileCategorySelected] = React.useState<SelectOptionType>(fileCategoryOptions[0]);
  const [fileCategoryOther, setFileCategoryOther] = React.useState<string>('');
  const [fileCategoryId, setFileCategoryId] = React.useState<number | null>(null);
  const [date, setDate] = React.useState<Date>(new Date);
  const [time, setTime] = React.useState<string>('');
  const [description, setDescription] = React.useState<string>('');
  const [errorDepartmentMessage, setErrorDepartmentMessage] = React.useState<string>('');
  const [errorFileCategoryMessage, setErrorFileCategoryMessage] = React.useState<string>('');
  const [errorFileCategoryOtherMessage, setErrorFileCategoryOtherMessage] = React.useState<string>('');
  const [errorTimeMessage, setErrorTimeMessage] = React.useState<string>('');
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorImageMessage, setErrorImageMessage] = React.useState<string>('');

  const add = useStoreActions(a => a.treatmentHistory.add)
  const edit = useStoreActions(a => a.treatmentHistory.edit)
  const setItem = useStoreActions(a => a.treatmentHistory.setItem)
  const item = useStoreState(s => s.treatmentHistory.item)

  React.useMemo(() => {
    if (action === 'edit') {
      setImage({ id: item?.imageId ? item?.imageId : null } as FileProps)
      setImageThumbnail({ id: item?.imageThumbnailId ? item?.imageThumbnailId : null } as FileProps)
      setOldImageId(item?.imageId as string)
      setOldImageThumbnailId(item?.imageThumbnailId as string)

      const departmentId = item?.department !== undefined ? item?.department : 0
      const fileCategoryId = item?.fileCategory !== undefined ? item?.fileCategory : 0

      setDepartmentSelected(departmentOptions[departmentId])
      setFileCategorySelected(fileCategoryOptions[fileCategoryId])
      setFileCategoryId(fileCategoryId)
      setFileCategoryOther(item?.fileCategoryOther as string)
      setDate(item?.date !== undefined ? new Date(item?.date.toString()) : new Date)
      setTime(moment(item?.time, 'HH:mm:ss').format('HH:mm'))
      setDescription(item?.description as string)
    } else {
      setItem({} as TreatmentHistoryProps)
    }
  }, [])

  const onFormSubmit = (values: ConfigData): void => {};

  const onSaveButtonPress = async () => {
    try {
      const department: any = departmentSelected
      const fileCategory: any = fileCategorySelected
      let validate = true
      
      setErrorDepartmentMessage('')
      setErrorFileCategoryMessage('')
      setErrorFileCategoryOtherMessage('')
      setErrorTimeMessage('')
      setErrorMessage('')

      if (typeof image?.id === 'undefined' || image?.id === null) { 
        setErrorImageMessage('Image is required.'); 
        validate = false 
      }

      if (department.value === null) { setErrorDepartmentMessage('Department is required.'); validate = false }
      if (fileCategory.value === null) { setErrorFileCategoryMessage('File Category is required.'); validate = false }
      if (fileCategory.value === 5 && fileCategoryOther === '') { setErrorFileCategoryOtherMessage('File Category Other is required.'); validate = false }
      if (time === null) { setErrorTimeMessage('Time is required.'); validate = false }
      if (!/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/.test(time)) { setErrorTimeMessage('Time should be HH:MM'); validate = false }
      if (!validate) return 
      
      const data = {
        id: treatmentHistoryId,
        date: new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1),
        time: time,
        description: description,
        department: department.value as any,
        fileCategory: fileCategory.value as any,
        fileCategoryOther: fileCategoryOther,
        creatureId: creatureId,
        imageId: image?.id,
        imageThumbnailId: imageThumbnail?.id,
        oldImageId: isImageChange ? oldImageId : null,
        oldImageThumbnailId: isImageChange ? oldImageThumbnailId : null,
      } 

      setIsLoading(true)
      if (action === 'add') {
        await add(data as TreatmentHistoryProps)
        setIsLoading(false)
        navigateBack()
      } else if (action === 'edit') {
        await edit(data as TreatmentHistoryProps)
        setIsLoading(false)
        navigateBack()
      }
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  }

  const onDepartmentSelect = (option): void => {
    setDepartmentSelected(option)
  }

  const onFileCategorySelect = (option): void => {
    setFileCategoryId(option.value)
    setFileCategoryOther('')
    setFileCategorySelected(option)
  }

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const onImageChange = async(images: FileProps[]) => {
    setErrorImageMessage('')

    try {
      setImage(images[0])
      setImageThumbnail(images[1])
      setIsImageChange(true)
    } catch (error) {
      setErrorImageMessage(error)
    }
  }

  const renderForm = (props: FormikProps<ConfigData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <PhotoChooser 
          source={item?.image}
          thumbnail={item?.imageThumbnail} 
          onChange={onImageChange} 
          error={errorImageMessage}
          nameUpload='treatment-history'
          tagUpload='treatment-history' 
        />
        <Select
          style={styles.input}
          label='Department'
          data={departmentOptions}
          selectedOption={departmentSelected}
          onSelect={onDepartmentSelect}
        />
        {
          errorDepartmentMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorDepartmentMessage }</Text>
          </View>
        }
        <Select
          style={styles.input}
          label='File Category'
          data={fileCategoryOptions}
          selectedOption={fileCategorySelected}
          onSelect={onFileCategorySelect}
        />
        {
          errorFileCategoryMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorFileCategoryMessage }</Text>
          </View>
        }
        {
          fileCategoryId === 5  &&
          <Input
            style={styles.input}
            label='File Category Other'
            value={fileCategoryOther}
            onChangeText={setFileCategoryOther}
          />
        }
        {
          errorFileCategoryOtherMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorFileCategoryOtherMessage }</Text>
          </View>
        }
        <Datepicker
          style={styles.input}
          label='Date'
          date={date}
          min={yesterday}
          max={tomorrow}
          onSelect={setDate}
          boundingMonth={false}
        />
        <Input
          style={styles.input}
          label='Time'
          value={time}
          onChangeText={setTime}
        />
        {
          errorTimeMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorTimeMessage }</Text>
          </View>
        }
        <Input
          style={[ styles.input , { alignItems: 'flex-start' } ]}
          textStyle={{ height: 150, textAlignVertical: 'top', }}
          label='Description'
          numberOfLines={10}
          multiline={true}
          maxLength={1500}
          value={description}
          onChangeText={setDescription}
        />
      </Layout>
      <Button
          style={styles.saveButton}
          onPress={onSaveButtonPress}>
          Save
        </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title={title}
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
        <Formik
          initialValues={ConfigData.empty()}
          validationSchema={ConfigSchema}
          onSubmit={onFormSubmit}>
          {renderForm}
        </Formik>
      </KeyboardAvoidingView>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    flex: 1,
    justifyContent: 'center',
    // aspectRatio: 1.0,
    height: 150
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
});