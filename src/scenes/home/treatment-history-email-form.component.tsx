import React from 'react';
import { StyleSheet, View } from 'react-native';
import { 
  Button, 
  Divider, 
  Icon, 
  Layout, 
  Modal, 
  Spinner, 
  Text 
} from '@ui-kitten/components';
import { TreatmentHistoryEmailFormScreenProps } from '../../navigation/home.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { TreatmentHistoryEmailFormData, TreatmentHistoryEmailFormSchema } from '../../data/treatment-history-email-form.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { FormInput } from '../../components/form-input.component';
import { useStoreActions, useStoreState } from '../../store';
import { TreatmentHistoryEmailProps } from 'src/store/models/treatment-history';

export const TreatmentHistoryEmailFormScreen = (props: TreatmentHistoryEmailFormScreenProps): SafeAreaLayoutElement => {
  const title = `Treatment History Email Form` 
  const creatureId = props.route.params?.creatureId ? props.route.params.creatureId : ''
  const treatmentHistoryIds = props.route.params.treatmentHistoryIds;

  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const sendEmail = useStoreActions(a => a.treatmentHistory.sendEmail)
  const member = useStoreState(s => s.member.item)

  React.useMemo(() => {
    
  }, [])

  const onFormSubmit = async (values: TreatmentHistoryEmailFormData) => {
    try {
      const data = {
        items: treatmentHistoryIds,
        from: values.emailFrom,
        to: values.emailTo,
        tag: 'treatment-history',
        subject: values.subject,
        description: values.description
      }

      setIsLoading(true)
      await sendEmail(data as TreatmentHistoryEmailProps)
      setIsLoading(false)
      navigateBack()
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  };

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const renderForm = (props: FormikProps<TreatmentHistoryEmailFormData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <FormInput
          id='emailFrom'
          label='From'
          style={styles.input}
          defaultValue={member?.email}
        />
        <FormInput
          id='emailTo'
          label='To'
          style={styles.input}
        />
        <FormInput
          id='subject'
          label='Subject'
          style={styles.input}
        />
        <FormInput
          id='description'
          label='Description'
          style={[ styles.input , { alignItems: 'flex-start' } ]}
          // defaultValue={item?.description}
          textStyle={{ height: 150, textAlignVertical: 'top', }}
          numberOfLines={10}
          multiline={true}
          maxLength={1500}
        />
      </Layout>
      <Button
        style={styles.sendButton}
        onPress={props.handleSubmit}>
        Send
    </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title={title}
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={BackIcon}
        onBackPress={navigateBack}
        style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
        <Formik
          initialValues={TreatmentHistoryEmailFormData.empty(member?.email)}
          validationSchema={TreatmentHistoryEmailFormSchema}
          onSubmit={onFormSubmit}>
          {renderForm}
        </Formik>
      </KeyboardAvoidingView>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  sendButton: {
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    flex: 1,
    justifyContent: 'center',
    // aspectRatio: 1.0,
    height: 150
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
});