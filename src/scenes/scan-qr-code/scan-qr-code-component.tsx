import React from 'react';
import { Image, StyleSheet, TouchableHighlight, View } from 'react-native';
import { Button, LayoutElement, Modal, Spinner, Text, Divider } from '@ui-kitten/components';
import { RNCamera } from 'react-native-camera'
import { ScanQRCodeScreenProps } from '../../navigation/scan-qr-code.navigator';
import { AppRoute } from '../../navigation/app-routes';
import { useStoreActions } from '../../store';
import { QRCodeProps } from '../../store/models/qrcode'
import { MainTheme } from '../../constants/LOV';
import { LogoApp } from '../../components/logo-app.component'
import { DOMAIN_URL } from '../../../app-config';
import {
  SafeAreaLayout,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { Toolbar } from '../../components/toolbar.component';
import { BackIcon } from '../../assets/icons';

export const ScanQRCodeScreen = (props: ScanQRCodeScreenProps): LayoutElement => {
  const [isLoading, setIsloading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const getCreatureByQRCodeId = useStoreActions(a => a.creature.getInfoByQRCodeId)
  const setQRCodeId = useStoreActions(a => a.app.setQRCodeId)
  const setQRCode = useStoreActions(a => a.qrcode.setItem)
  const setState = useStoreActions(a => a.app.setState)
  const getQRCodeById = useStoreActions(a => a.qrcode.getQRCodeById)
  const setPrevState = useStoreActions(a => a.app.setPrevState)
  const setCreatureInfo = useStoreActions(a => a.creature.setItem)

  const onBarCodeRead = async (value) => {
    const { data, type } = value

    if (type !== 'QR_CODE' && type !== 'org.iso.QRCode') return
    try {
      if (isLoading || errorMessage !== '') return
      setIsloading(true)
      const qrcodeGUID = data.replace(DOMAIN_URL + '/creature/', '')
      const qrcode = await getQRCodeById(qrcodeGUID) as QRCodeProps
      
      // setTimeout(() => {
        if (qrcode.usage_status === 1) {
          setQRCodeId(qrcode.id)
          setQRCode(qrcode)
          setIsloading(false)
          setState(AppRoute.SIGN_UP)
        } else if (qrcode?.usage_status === 2) {
          setQRCode(null)
          setIsloading(false)
          setErrorMessage('QRCode have been already registered.')
        } else if (qrcode?.usage_status === 3) {
          const res = await getCreatureByQRCodeId(qrcode.id)
          setCreatureInfo(res)
          setIsloading(false)
          setPrevState(AppRoute.ENTRANCE)
          setState(AppRoute.GUEST_CREATURE)
        }
      // }, 300)
      
    } catch (error) {
      setQRCode(null)
      setIsloading(false)
      setErrorMessage(error)
    }
  }

  const renderFocus = () => (
    <View style={[barcodeStyles.container]}>
      <View style={barcodeStyles.finder}>
          <View style={barcodeStyles.topLeftEdge} />
          <View style={barcodeStyles.topRightEdge} />
          <View style={barcodeStyles.bottomLeftEdge} />
          <View style={barcodeStyles.bottomRightEdge} />
      </View>
    </View>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
        title='QRCode Scanner'
        titleStyle={{ color: MainTheme.colorSecondary }}
        backIcon={BackIcon}
        onBackPress={() => setState(AppRoute.ENTRANCE)}
        style={styles.toolbar}
      />
      <Divider />
      <View style={styles.cameraContainer}>
        <RNCamera
          onBarCodeRead={onBarCodeRead}
          androidCameraPermissionOptions={{ 
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera phone'
          }}
          style={{ flex: 1 }} >
          { renderFocus() }
        </RNCamera>
      </View>
      <View style={styles.labelContainer}>
        <Text style={styles.textLabel} status='control'>Align the QRcode within the</Text>
        <Text style={styles.textLabel}  status='control'>frame to start scanning</Text>
      </View>
      <View style={styles.logoContainer}>
        <LogoApp style={{  width: '100%', height: '100%' }} />
      </View>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  cameraContainer: {
    flex: 0.4,
    overflow: 'hidden'
  },
  labelContainer: { 
      flex: 0.2, 
      justifyContent: 'center', 
      alignItems: 'center', 
      backgroundColor: '#000000' 
    },
  logoContainer: {
    flex: 0.4,
    justifyContent: 'center', 
    alignItems: 'center',
  },
  textLabel: {
    zIndex: 1,
    alignSelf: 'center'
  },
  videoContainer: {
    flex: 1
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  }
});

const barcodeStyles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
  },
  finder: {
      alignItems: 'center',
      justifyContent: 'center',
      width: 280, 
      height: 220
  },
  topLeftEdge: {
    borderColor: 'red',
    position: 'absolute',
    borderLeftWidth: 2,
    borderTopWidth: 2,
    top: 0,
    left: 0,
    width: 40,
    height: 20
  },
  topRightEdge: {
    borderColor: 'red',
    position: 'absolute',
    borderRightWidth: 2,
    borderTopWidth: 2,
    top: 0,
    right: 0,
    width: 40,
    height: 20
  },
  bottomLeftEdge: {
    borderColor: 'red',
    position: 'absolute',
    borderLeftWidth: 2,
    borderBottomWidth: 2,
    bottom: 0,
    left: 0,
    width: 40,
    height: 20
  },
  bottomRightEdge: {
    borderColor: 'red',
    position: 'absolute',
    borderRightWidth: 2,
    borderBottomWidth: 2,
    bottom: 0,
    right: 0,
    width: 40,
    height: 20
  }
})
