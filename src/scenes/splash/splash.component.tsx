import React from 'react';
import { Layout, LayoutElement, Spinner, StyleService } from '@ui-kitten/components';
import { SplashScreenProps } from '../../navigation/splash.navigator';
import { MainTheme } from '../../constants/LOV'
import { LogoApp } from '../../components/logo-app.component'
import { AppRoute } from '../../navigation/app-routes';
import { useStoreActions, useStoreState } from "../../store";

export const SplashScreen = (props: SplashScreenProps): LayoutElement => {

  // const [countDown, setCountDown] = React.useState<number>(10)
  // const [intervalId, setIntervalId] = React.useState()

  const getInfo = useStoreActions(a => a.member.getInfo)
  const setItem = useStoreActions(a => a.member.setItem)
  const setState = useStoreActions(a => a.app.setState)
  const token = useStoreState(s => s.member.token)

  React.useMemo(() => {
    const loadInfo = async () => {
      if (typeof token?.value !== 'undefined' && token?.value !== null) {
        try {
          const res = await getInfo()
          setItem(res)
          setState(AppRoute.HOME)
        } catch (error) {
          setState(AppRoute.ENTRANCE)
        }
      } else {
        setState(AppRoute.ENTRANCE)
      }
    }

    setTimeout(() => {
      loadInfo()
    }, 5000)
  }, [])

  return (
    <Layout style={styles.container}>
      <LogoApp style={{ width: 200, height: 200, marginBottom: 10 }}/>
      <Spinner size='giant' />
    </Layout>
  )
}

const styles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
    justifyContent: 'center',
    alignItems: 'center'
  }
})