import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Divider, Icon, Layout , Text} from '@ui-kitten/components';
import { SignUpContactScreenProps } from '../../navigation/sign-up.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { SignUpContactData, SignUpContactSchema } from '../../data/sign-up-contact.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { FormInput } from '../../components/form-input.component';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { AppRoute } from '../../navigation/app-routes';
import { useStoreActions, useStoreState } from '../../store';
import { MemberProps } from '../../store/models/member';

export const SignUpContactScreen = (props: SignUpContactScreenProps): SafeAreaLayoutElement => {
  const [errorEmailMessage, setErrorEmailMessage] = React.useState<string>('');

  const checkEmailExist = useStoreActions(a => a.member.checkEmailExist)
  const setMember = useStoreActions(a => a.member.setItem)
  const member = useStoreState(s => s.member.item)

  const onFormSubmit = async (values: SignUpContactData) => {
    try {
      await checkEmailExist({ email: values.email })
    } catch(error) {
      setErrorEmailMessage(error)
      return
    }
    const data = {
      ...member, 
      email: values.email,
      contactNo1: values.contactNumber1, 
      contactNo2: values.contactNumber2, 
      address1: values.address1, 
      address2: values.address2, 
      address3: values.address3
    }

    setMember(data as MemberProps)
    navigateSignUpLocation()
  };

  const navigateSignUpLocation = (): void => {
    props.navigation.navigate(AppRoute.SIGN_UP_LOCATION)
  }

  const navigateBack = (): void => {
    props.navigation.goBack();
  }


  const renderForm = (props: FormikProps<SignUpContactData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <FormInput
          id='email'
          style={styles.input}
          label='Email'
          keyboardType='email-address'
        />
        {
          errorEmailMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorEmailMessage }</Text>
          </View>
        }
        <FormInput
          id='contactNumber1'
          style={styles.input}
          label='Contact Number'
        />
        <FormInput
          id='contactNumber2'
          style={styles.input}
          label='Contact Number'
        />
        <FormInput
          id='address1'
          style={styles.input}
          label='Address'
        />
        <FormInput
          id='address2'
          style={styles.input}
          label='Address'
        />
        <FormInput
          id='address3'
          style={styles.input}
          label='Address'
        />
      </Layout>
      <Button
        style={styles.saveButton}
        onPress={props.handleSubmit}>
        Next
      </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title='Contact Infomation'
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
          <Formik
            initialValues={SignUpContactData.empty()}
            validationSchema={SignUpContactSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
      </KeyboardAvoidingView>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    // paddingHorizontal: 4,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    // marginHorizontal: 24,
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  }
});