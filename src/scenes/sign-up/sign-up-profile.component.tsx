import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Datepicker, Divider, Icon, Layout, Select, Text, SelectOptionType } from '@ui-kitten/components';
import { SignUpProfileScreenProps } from '../../navigation/sign-up.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { AppRoute } from '../../navigation/app-routes';
import { PhotoChooser } from '../../components/photo-chooser.component';
import { SignUpProfileData, SignUpProfileSchema } from '../../data/sign-up-profile.model';
import { useStoreActions, useStoreState } from '../../store';
import { FormInput } from '../../components/form-input.component';
import { FileProps } from '../../store/models/file';
import { API_IMAGE_PATH } from '../../../app-config';
import { MemberProps } from '../../store/models/member';

export const SignUpProfileScreen = (props: SignUpProfileScreenProps): SafeAreaLayoutElement => {
  const now = new Date();
  const yesterday = new Date(now.getFullYear() - 50, now.getMonth(), now.getDate() - 1);
  const tomorrow = new Date(now.getFullYear() + 50, now.getMonth(), now.getDate() + 1);

  const genderOptions = [
    { text: '-- Select --', value: null, disabled: true },
    { text: 'Male', value: 1 },
    { text: 'Female', value: 2 }
  ]

  const bloodTypeOptions = [
    { text: '-- Select --', value: null, disabled: true },
    { text: 'AB', value: 1 },
    { text: 'A', value: 2 },
    { text: 'B', value: 3 },
    { text: 'O', value: 4 }
  ]

  const [bloodTypeSelected, setBloodTypeSelected] = React.useState<SelectOptionType>(bloodTypeOptions[0]);
  const [genderSelected, setGenderSelected] = React.useState<SelectOptionType>(genderOptions[0]);
  const [birthDate, setBirthDate] = React.useState(new Date);
  const [errorImageProfileMessage, setErrorImageProfileMessage] = React.useState<string>('');
  const [errorGenderMessage, setErrorGenderMessage] = React.useState<string>('');
  const [errorBloodTypeMessage, setErrorBloodTypeMessage] = React.useState<string>('');
  const [profileImage, setProfileImage] = React.useState<FileProps>();
  const [profileImageThumbnail, setProfileImageThumbnail] = React.useState<FileProps>();

  
  const setMember = useStoreActions(a => a.member.setItem)
  const member = useStoreState(s => s.member.item)

  const onFormSubmit = (values: SignUpProfileData): void => {
    const bloodType: any = bloodTypeSelected
    const gender: any = genderSelected
    let validate = true
    
    setErrorGenderMessage('')
    setErrorBloodTypeMessage('')
    setErrorImageProfileMessage('')

    if (typeof profileImage?.id === 'undefined' || profileImage?.id === null) { 
      setErrorImageProfileMessage('Profile Image is required.'); 
      validate = false 
    }

    if (gender.value === null) { setErrorGenderMessage('Gender is required.'); validate = false }
    if (bloodType.value === null) { setErrorBloodTypeMessage('Blood Type is required.'); validate = false }

    if (!validate) return 

    const data = {
      ...member, 
      firstName: values.firstName, 
      lastName: values.lastName,
      gender: gender.value as any,
      bloodType: gender.value as any,
      birthDate: new Date(birthDate.getFullYear(), birthDate.getMonth(), birthDate.getDate() + 1),
      relationship: values.relationship,
      identificationNo: values.idCardNumber,
      profileImageId: profileImage?.id,
      profileImageThumbnailId: profileImageThumbnail?.id
    } 
    
    setMember(data as MemberProps)
    navigateSignUpContact()
  };

  const onBloodTypeSelect = (option): void => {
    setBloodTypeSelected(option)
  }

  const onGenderSelect = (option): void => {
    setGenderSelected(option)
  }

  const navigateBack = (): void => {
    props.navigation.goBack();
  }

  const navigateSignUpContact = (): void => {
    props.navigation.navigate(AppRoute.SIGN_UP_CONTACT);
  }

  const onProfileChange = async(images: FileProps[]) => {
    setErrorImageProfileMessage('')

    try {
      setProfileImage(images[0])
      setProfileImageThumbnail(images[1])
    } catch (error) {
      setErrorImageProfileMessage(error)
    }
  }
  
  const renderForm = (props: FormikProps<SignUpProfileData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <PhotoChooser 
          source={profileImage ? API_IMAGE_PATH + '/' + profileImage.path + '/' + profileImage.name  : null} 
          thumbnail={profileImageThumbnail ? API_IMAGE_PATH + '/' + profileImageThumbnail.path + '/' + profileImageThumbnail.name  : null}
          onChange={onProfileChange} 
          error={errorImageProfileMessage}
          nameUpload='member-profile'
          tagUpload='member-profile' />
        <FormInput
          id="firstName"
          style={styles.input}
          label='First Name'
        />
        <FormInput
          id="lastName"
          style={styles.input}
          label='Last Name'
        />
        <Select
          style={styles.input}
          label='Gender'
          data={genderOptions}
          selectedOption={genderSelected}
          onSelect={onGenderSelect}
        />
        {
          errorGenderMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorGenderMessage }</Text>
          </View>
        }
        <Select
          style={styles.input}
          label='Blood type'
          data={bloodTypeOptions}
          selectedOption={bloodTypeSelected}
          onSelect={onBloodTypeSelect}
        />
        {
          errorBloodTypeMessage !== '' &&
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: -5 }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 12 }} >{ errorBloodTypeMessage }</Text>
          </View>
        }
        <Datepicker
          style={styles.input}
          label='Birth Date'
          date={birthDate}
          min={yesterday}
          max={tomorrow}
          onSelect={setBirthDate}
          boundingMonth={false}
        />
        <FormInput
          id='relationship'
          style={styles.input}
          label='Relationship'
        />
        <FormInput
          id='idCardNumber'
          style={styles.input}
          label='ID Card Number'
        />
      </Layout>
      <Button
        style={styles.saveButton}
        onPress={props.handleSubmit}>
        Next
      </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title='Account Information'
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
          <Formik
            initialValues={SignUpProfileData.empty()}
            validationSchema={SignUpProfileSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
      </KeyboardAvoidingView>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    // paddingHorizontal: 4,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    // marginHorizontal: 24,
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    flex: 1,
    justifyContent: 'center',
    // aspectRatio: 1.0,
    height: 150
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  }
});