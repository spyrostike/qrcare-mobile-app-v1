import React from 'react';
import { StyleSheet, View } from 'react-native';
import { 
  Button, 
  Divider, 
  Layout, 
  Modal,
  Spinner,
  Text 
} from '@ui-kitten/components';
import { SignUpLocationScreenProps } from '../../navigation/sign-up.navigator';
import { Toolbar } from '../../components/toolbar.component';
import { SignUpLocationData, SignUpLocationSchema } from '../../data/sign-up-location.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { Formik, FormikProps } from 'formik';
import { BackIcon } from '../../assets/icons';
import {
  SafeAreaLayout,
  SafeAreaLayoutElement,
  SaveAreaInset,
} from '../../components/safe-area-layout.component';
import { MainTheme } from '../../constants/LOV';
import { Map } from '../../components/map.component';
import { GeoCoordinates } from 'react-native-geolocation-service';
import { PhotoChooserBox } from '../../components/photo-chooser-box.component';
import { FileProps } from '../../store/models/file';
import { useStoreActions, useStoreState } from '../../store';
import { MemberProps } from '../../store/models/member';
import { AppRoute } from '../../navigation/app-routes';

export const SignUpLocationScreen = (props: SignUpLocationScreenProps): SafeAreaLayoutElement => {
  
  const [location, setLocation] = React.useState<GeoCoordinates>();
  const [locationImage, setLocationImage] = React.useState<FileProps>();
  const [locationImageThumbnail, setLocationImageThumbnail] = React.useState<FileProps>();
  const [errorLocationMessage, setErrorLocationMessage] = React.useState<string>('');
  const [errorImageLocationMessage, setErrorImageLocationMessage] = React.useState<string>('');
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const signUp = useStoreActions(a => a.member.signUp)
  const setMemberToken = useStoreActions(a => a.member.setToken)
  const setMember = useStoreActions(a => a.member.setItem)
  const setState = useStoreActions(a => a.app.setState)
  const member = useStoreState(s => s.member.item)

  const onFormSubmit = async (values: SignUpLocationData) => {
    try {
      let validate = true
      setErrorLocationMessage('')
      setErrorImageLocationMessage('')

      if (location === null) { 
        setErrorLocationMessage('Location is required.'); 
        validate = false 
      }
      if (typeof locationImage?.id === 'undefined' || locationImage?.id === null) { 
        setErrorImageLocationMessage('Location Image is required.'); 
        validate = false 
      }
      if (!validate) return 

      const data = {
        ...member, 
        placeLatitude: location?.latitude,
        placeLongitude: location?.longitude,
        locationImageId: locationImage?.id,
        locationImageThumbnailId: locationImageThumbnail?.id,
      }
      setMember(data as MemberProps)
      setIsLoading(true)
      const res = await signUp(data as MemberProps)
      setIsLoading(false)
      setMemberToken({ value: res.token })
      setMember(res.item)
      setState(AppRoute.HOME)
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  };

  const navigateBack = (): void => {
    props.navigation.goBack();
  }
  
  const onLocationChange = (position :GeoCoordinates): void => {
    setErrorLocationMessage('')
    setLocation(position)
  }

  const onLocationImageChange = async(images: FileProps[]) => {
    setErrorImageLocationMessage('')
    try {
      setLocationImage(images[0])
      setLocationImageThumbnail(images[1])
    } catch (error) {
      setErrorImageLocationMessage(error)
    }
  }

  const renderForm = (props: FormikProps<SignUpLocationData>): React.ReactFragment => (
    <React.Fragment>
      <Layout style={styles.form} level='1'>
        <Map onLocationChange={onLocationChange} error={errorLocationMessage} />
        <PhotoChooserBox 
          hint='Location Image' 
          nameUpload='member-location' 
          tagUpload='member-location'
          error={errorImageLocationMessage}
          onChange={onLocationImageChange} />
      </Layout>
      <Button
        style={styles.saveButton}
        onPress={props.handleSubmit}>
        Done
      </Button>
    </React.Fragment>
  )

  return (
    <SafeAreaLayout
      style={styles.safeArea}
      insets={SaveAreaInset.TOP}>
      <Toolbar
          title='Lacation'
          titleStyle={{ color: MainTheme.colorSecondary }}
          backIcon={BackIcon}
          onBackPress={navigateBack}
          style={styles.toolbar}
      />
      <Divider />
      <KeyboardAvoidingView style={styles.formContainer}>
          <Formik
            initialValues={SignUpLocationData.empty()}
            validationSchema={SignUpLocationSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
      </KeyboardAvoidingView>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
          <View style={styles.childrenModalCantainer}>
            <Spinner/>
          </View>
        </Modal>
        <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
          <View style={styles.errorModalCantainer}>
            <Text style={styles.childrenModalText}>{ errorMessage }</Text>
            <Divider />
            <Text 
              style={[ 
                styles.childrenModalText, 
                { 
                  color: MainTheme.colorSecondary, 
                  backgroundColor: MainTheme.buttonColor, 
                  borderBottomLeftRadius: 5,
                  borderBottomRightRadius: 5 }
                ]}
                onPress={() => setErrorMessage('')}>
                OK
            </Text>
          </View>
        </Modal>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary,
  },
  toolbar: {
    backgroundColor: MainTheme.colorPrimary
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 16
  },
  form: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 24,
    backgroundColor: MainTheme.colorPrimary
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    alignSelf: 'center',
  },
  input: {
    // marginHorizontal: 12,
    marginVertical: 8,
  },
  button: {
    marginVertical: 24,
  },
  saveButton: {
    marginHorizontal: 24,
    marginBottom: 24,
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    borderRadius: 24,
  },
  takePhotoContainer: {
    justifyContent: 'center',
    // aspectRatio: 1.0,
    height: 250
  },
  takePhotoTitle: {
    alignSelf: 'center',
    marginTop: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
});