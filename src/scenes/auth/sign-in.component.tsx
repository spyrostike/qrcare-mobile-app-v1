import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Icon, LayoutElement, Spinner, Text } from '@ui-kitten/components';
import { Formik, FormikProps } from 'formik';
import { SignInScreenProps } from '../../navigation/auth.navigator';
import { AppRoute } from '../../navigation/app-routes';
import { FormInput } from '../../components/form-input.component';
import { EyeIcon, EyeOffIcon, PersonIcon } from '../../assets/icons';
import { SignInData, SignInSchema } from '../../data/sign-in.model';
import { LogoApp } from '../../components/logo-app.component'
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { useStoreActions } from "../../store";
import { MainTheme } from '../../constants/LOV';
import { MemberProps } from '../../store/models/member';

export const SignInScreen = (props: SignInScreenProps): LayoutElement => {
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const [passwordVisible, setPasswordVisible] = React.useState<boolean>(false);

  const signIn = useStoreActions(a => a.member.signIn)
  const setToken = useStoreActions(a => a.member.setToken)
  const setItem = useStoreActions(a => a.member.setItem)
  const setState = useStoreActions(a => a.app.setState)

  const onFormSubmit = async (values: SignInData) => {
    setErrorMessage('')
    try {
      setIsLoading(true)
      const res = await signIn({ username: values.username, password: values.password }  as MemberProps)
      setIsLoading(false)
      setToken({ value: res.token })
      setItem(res.item)
      navigateHome();
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  };

  const navigateHome = (): void => {
    setState(AppRoute.HOME)
    // props.navigation.navigate(AppRoute.HOME);
  };

  const navigateForgotPassword = (): void => {
    props.navigation.navigate(AppRoute.RESET_PASSWORD);
  };

  const onPasswordIconPress = (): void => {
    setPasswordVisible(!passwordVisible);
  };

  const renderForm = (props: FormikProps<SignInData>): React.ReactFragment => (
    <React.Fragment>
      <View style={{ flex: 1 }}>
        <FormInput
          id='username'
          style={styles.input}
          placeholder='Username'
          // keyboardType='email-address'
          icon={PersonIcon}
        />
        <FormInput
          id='password'
          style={styles.input}
          placeholder='Password'
          secureTextEntry={!passwordVisible}
          icon={passwordVisible ? EyeIcon : EyeOffIcon}
          onIconPress={onPasswordIconPress}
        />
        <View style={styles.forgotPasswordContainer}>
          <Button
            style={styles.forgotPasswordButton}
            appearance='ghost'
            status='control'
            onPress={navigateForgotPassword}>
            Forgot your password?
          </Button>
        </View>
        {
          errorMessage !== '' &&
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <Icon style={{ width: 10, height: 10, marginRight: 8 }} fill='#FF3D71' name='alert-triangle-outline'/>
            <Text style={{ color: '#FF3D71', fontSize: 13 }} >{ errorMessage }</Text>
          </View>
        }
        {
          isLoading &&
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Spinner/>
          </View>
        }
      </View>
      <Button
        style={styles.submitButton}
        onPress={props.handleSubmit}>
        SIGN IN
      </Button>
    </React.Fragment>
  );

  return (
    <KeyboardAvoidingView>
      <View style={styles.container} >
        <View style={styles.headerContainer}>
          <LogoApp style={{  width: 180, height: 180 }} />
        </View>
        <View style={styles.formContainer}>
          <Formik
            initialValues={SignInData.empty()}
            validationSchema={SignInSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  settingContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 15,
    marginRight: 15
  },
  settingButton: {
    flexDirection: 'row-reverse',
    paddingHorizontal: 0,
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 200,
    padding: 20
  },
  signInLabel: {
    marginTop: 16,
  },
  formContainer: {
    flex: 1,
    // marginTop: 32,
    paddingHorizontal: 16,
  },
  appBar: {
    height: 192,
  },
  resetPasswordContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  input: {
    marginVertical: 4,
  },
  submitButton: {
    marginVertical: 24,
  },
  forgotPasswordContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  forgotPasswordButton: {
    paddingHorizontal: 0,
  }
});
