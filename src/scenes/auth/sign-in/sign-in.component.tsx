import React from 'react';
import { ImageBackground, StyleSheet, View } from 'react-native';
import { Button, CheckBox, Layout, LayoutElement, Text } from '@ui-kitten/components';
import { Formik, FormikProps } from 'formik';
import { SignInScreenProps } from '../../../navigation/auth.navigator';
import { AppRoute } from '../../../navigation/app-routes';
import { FormInput } from '../../../components/form-input.component';
import { EyeIcon, EyeOffIcon, PersonIcon } from '../../../assets/icons';
import { SignInData, SignInSchema } from '../../../data/sign-in.model';
import { ImageOverlay } from './extra/image-overlay.component';

export const SignInScreen = (props: SignInScreenProps): LayoutElement => {

  const [passwordVisible, setPasswordVisible] = React.useState<boolean>(false);

  const onFormSubmit = (values: SignInData): void => {
    navigateHome();
  };

  const navigateHome = (): void => {
    props.navigation.navigate(AppRoute.HOME);
  };

  const navigateResetPassword = (): void => {
    props.navigation.navigate(AppRoute.RESET_PASSWORD);
  };

  const onPasswordIconPress = (): void => {
    setPasswordVisible(!passwordVisible);
  };

  const renderForm = (props: FormikProps<SignInData>): React.ReactFragment => (
    <React.Fragment>
      <View style={{ flex: 1 }}>
        <FormInput
          id='email'
          style={styles.formControl}
          placeholder='Email'
          keyboardType='email-address'
          icon={PersonIcon}
        />
        <FormInput
          id='password'
          style={styles.formControl}
          placeholder='Password'
          secureTextEntry={!passwordVisible}
          icon={passwordVisible ? EyeIcon : EyeOffIcon}
          onIconPress={onPasswordIconPress}
        />
        <View style={styles.resetPasswordContainer}>
          <Button
            appearance='ghost'
            status='basic'
            onPress={navigateResetPassword}>
            Forgot password?
          </Button>
        </View>
      </View>
      <Button
        style={styles.submitButton}
        onPress={props.handleSubmit}>
        SIGN IN
      </Button>
    </React.Fragment>
  );

  return (
    <React.Fragment>
      <ImageOverlay
        style={styles.container}
        source={require('./assets/image-background.jpg')}>
        <View style={styles.headerContainer}>
          <Text category='h1' status='control'>Safty</Text>
          <Text style={styles.signInLabel} category='s1' status='control'>Observation</Text>
        </View>
        <View style={styles.formContainer}>
          <Formik
            initialValues={SignInData.empty()}
            validationSchema={SignInSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
        </View>
      </ImageOverlay>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 216,
  },
  signInLabel: {
    marginTop: 16,
  },
  formContainer: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 16,
  },
  appBar: {
    height: 192,
  },
  resetPasswordContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  formControl: {
    marginVertical: 4,
  },
  submitButton: {
    marginVertical: 24,
  },
  noAccountButton: {
    alignSelf: 'center',
  }
});
