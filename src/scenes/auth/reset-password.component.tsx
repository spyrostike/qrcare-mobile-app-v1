import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Divider, LayoutElement, Modal, Spinner, Text } from '@ui-kitten/components';
import { Formik, FormikProps } from 'formik';
import { ResetPasswordScreenProps } from '../../navigation/auth.navigator';
import { FormInput } from '../../components/form-input.component';
import { Toolbar } from '../../components/toolbar.component';
import { ResetPasswordData, ResetPasswordSchema } from '../../data/reset-password.model';
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { MainTheme } from '../../constants/LOV';
import { EmailIcon } from '../../assets/icons';
import { useStoreActions } from "../../store";

export const ResetPasswordScreen = (props: ResetPasswordScreenProps): LayoutElement => {
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const resetPassword = useStoreActions(a => a.member.resetPassword)

  const onFormSubmit = async (values: ResetPasswordData) => {
    setErrorMessage('')

    try {
      setIsLoading(true)
      await resetPassword({ email: values.email })
      setIsLoading(false)
      navigateBack();
    } catch (error) {
      setIsLoading(false)
      setErrorMessage(error)
    }
  };

  const navigateBack = (): void => {
    props.navigation.goBack();
  };

  const renderForm = (props: FormikProps<ResetPasswordData>): React.ReactFragment => (
    <React.Fragment>
      <View style={{ flex: 1 }}>
        <FormInput
          id='email'
          style={styles.formControl}
          placeholder='Email'
          keyboardType='email-address'
          icon={EmailIcon}
        />
      </View>
      <Button
        style={styles.button}
        onPress={props.handleSubmit}>
        DONE
      </Button>
    </React.Fragment>
  );

  return (
    <KeyboardAvoidingView>
      <View style={styles.container} >
        <Toolbar
          appearance='control'
          onBackPress={props.navigation.goBack}
        />
        <Text
          style={styles.forgotPasswordLabel}
          category='h4'
          status='control'>
          Forgot Password
        </Text>
        <Text
          style={styles.enterEmailLabel}
          status='control'>
          Please enter your email address
        </Text>
        <View style={styles.formContainer}>
          <Formik
            initialValues={ResetPasswordData.empty()}
            validationSchema={ResetPasswordSchema}
            onSubmit={onFormSubmit}>
            {renderForm}
          </Formik>
        </View>
      </View>
      <Modal visible={isLoading} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.childrenModalCantainer}>
          <Spinner/>
        </View>
      </Modal>
      <Modal visible={errorMessage !== ''} backdropStyle={styles.backdrop} style={{ width: '60%' }}>
        <View style={styles.errorModalCantainer}>
          <Text style={styles.childrenModalText}>{ errorMessage }</Text>
          <Divider />
          <Text 
            style={[ 
              styles.childrenModalText, 
              { 
                color: MainTheme.colorSecondary, 
                backgroundColor: MainTheme.buttonColor, 
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5 }
              ]}
              onPress={() => setErrorMessage('')}>
              OK
          </Text>
        </View>
      </Modal>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  appBar: {
    height: 192,
  },
  formContainer: {
    flex: 1,
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  formControl: {
    marginVertical: 4,
  },
  button: {
    marginVertical: 24,
  },
  forgotPasswordLabel: {
    zIndex: 1,
    alignSelf: 'center',
  },
  enterEmailLabel: {
    zIndex: 1,
    alignSelf: 'center',
    marginTop: 64,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  errorModalCantainer: {
    backgroundColor: MainTheme.colorSecondary, 
    borderRadius: 5
  },
  errorButton: {
    backgroundColor: MainTheme.buttonColor,
    borderWidth: 0
  },
  childrenModalCantainer: {
    alignItems: 'center'
  },
  childrenModalText: {
    padding: 12, 
    textAlign: 'center', 
    color: MainTheme.buttonColor
  }
});
