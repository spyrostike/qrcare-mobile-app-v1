import React from 'react';
import { Image, StyleSheet, TouchableHighlight, View } from 'react-native';
import { Button, LayoutElement } from '@ui-kitten/components';
import Video from 'react-native-video';
import { VideoPresentScreenProps } from '../../navigation/entrance.navigator';
import { AppRoute } from '../../navigation/app-routes';
import { ArrowForwardIcon } from '../../assets/icons';
import { LogoApp } from '../../components/logo-app.component'
import { KeyboardAvoidingView } from '../../components/3rd-party.component';
import { useStoreActions } from "../../store";
import { MainTheme } from '../../constants/LOV';
import { API_VIDEO_PATH } from '../../../app-config';
import { SafeAreaLayout, SaveAreaInset } from '../../components/safe-area-layout.component';

export const VideoPresentScreen = (props: VideoPresentScreenProps): LayoutElement => {

  const setState = useStoreActions(a => a.app.setState)

  const navigateAuth = (): void => {
    setState(AppRoute.AUTH);
  };
  const navigateScanQR = (): void => {
    setState(AppRoute.SCAN_QR_CODE);
  };

  return (
    <SafeAreaLayout style={styles.safeArea} insets={SaveAreaInset.TOP}>
      <KeyboardAvoidingView>
        <View style={styles.container} >
          <View style={styles.headerContainer}>
            <View style={styles.signInContainer}>
              <Button
                style={styles.signInButton}
                appearance='ghost'
                status='control'
                size='giant'
                textStyle={{ fontSize: 12 }}
                icon={ArrowForwardIcon}
                onPress={navigateAuth} >
                Sign In
              </Button>
            </View>
            <LogoApp style={{  width: 180, height: 180, alignSelf: 'center' }}/>
          </View>
          <View style={styles.videoContainer}>
            <Video             
                source={{ uri: `${API_VIDEO_PATH}/title_video.mp4` }}                 
                style={{ width: '100%', height: '100%' }} 
                resizeMode='cover'
                repeat={true} />
          </View>
          <View style={styles.scanContainer}>
            <TouchableHighlight style={{ width: '50%', }} onPress={navigateScanQR} >
              <Image
                  resizeMode='contain'
                  source={require('../../assets/images/qr_scaner.png')}
                  style={{ 
                      width: '100%',
                      height: '100%'
                  }} />
            </TouchableHighlight>
          </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  container: {
    flex: 1,
    backgroundColor: MainTheme.colorPrimary
  },
  headerContainer: {
    // flex: 1,
    paddingTop: 0,
    padding: 8
  },
  signInContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  signInButton: {
    flexDirection: 'row-reverse',
    paddingHorizontal: 0,
  },
  signInLabel: {
    marginTop: 16,
  },
  videoContainer: {
    flex: 1
  },
  scanContainer: {
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  appBar: {
    height: 192,
  },
  resetPasswordContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  formControl: {
    marginVertical: 4,
  },
  submitButton: {
    marginVertical: 24,
  },
  noAccountButton: {
    alignSelf: 'center',
  }
});
