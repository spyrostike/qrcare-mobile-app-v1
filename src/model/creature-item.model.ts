export interface CreatureItem {
  id: string;
  name: string;
}