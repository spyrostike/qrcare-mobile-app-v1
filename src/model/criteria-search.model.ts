export interface CriteriaSearch {
    creatureId?: string | null;
    vaccineBookId?: string | null;
    refId?: string | null; 
    tag?: string | null;
    textSearch?: string | null;
    limit?: number | null;
    offset?: number | null;
    orderByKey?: string | null;
    orderByValue?: string | null;
    status?: number | null;
  }