import * as Yup from 'yup';

export class SignUpData {

  constructor(
    readonly username: string,
    readonly password: string) {

  }

  static empty(): SignUpData {
    return new SignUpData(
      '',
      '',
    );
  }
}

export const SignUpSchema = Yup.object().shape({
  username: Yup.string().required('Username is required.')
  .min(6, 'Username should be between 6-30')
  .max(30, 'Username should be between 6-30')
  .matches(/^[a-zA-Z0-9_]+$/, 'Username should be between a-z 0-9 '),
  password: Yup.string().required('Password is required.')
  .min(6, 'Password should be between 6-30')
  .max(30, 'Password should be between 6-30')
  .matches(/^[a-zA-Z0-9_]+$/, 'Password should be between a-z 0-9 ')
  // username: Yup.string().required('Username is required.').email('Invalid email.'),
  // email: Yup.string().email('Invalid email.'),
  // password: Yup.string().min(8, 'Password must be at least 8 characters.'),
});

