import * as Yup from 'yup';

export class SignUpContactData {

  constructor(
    readonly email: string,
    readonly contactNumber1: string,
    readonly contactNumber2: string,
    readonly address1: string,
    readonly address2: string,
    readonly address3: string) {

  }

  static empty(): SignUpContactData {
    return new SignUpContactData(
      '',
      '',
      '',
      '',
      '',
      ''
    );
  }
}

export const SignUpContactSchema = Yup.object().shape({
  email: Yup.string().required('Email is required.').email('Invalid email.'),
  contactNumber1: Yup.string().required('Contact is required.'),
  address1: Yup.string().required('Address is required.')
});

