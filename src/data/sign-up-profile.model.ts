import * as Yup from 'yup';

export class SignUpProfileData {

  constructor(
    readonly firstName: string,
    readonly lastName: string,
    readonly relationship: string,
    readonly idCardNumber: string) {

  }

  static empty(): SignUpProfileData {
    return new SignUpProfileData(
      '',
      '',
      '',
      ''
    );
  }
}

export const SignUpProfileSchema = Yup.object().shape({
  firstName: Yup.string().required('Fist Name is required.'),
  lastName: Yup.string().required('Last Name is required.'),
  relationship: Yup.string().required('relationShip is required.'),
  idCardNumber: Yup.string().required('ID Card Number is required.')
  .matches(/^[0-9]+$/, 'ID Card Number should be between 0-9.')
});

