import * as Yup from 'yup';

export class ChangePasswordFormData {

  constructor(
    readonly currentPassword: string,
    readonly newPassword: string,
    readonly repeatPassword: string) {
  }

  static empty(): ChangePasswordFormData {
  return new ChangePasswordFormData(
    '',
    '',
    ''
  );
  }
}

export const ChangePasswordFormSchema = Yup.object().shape({
  currentPassword: Yup.string().required('Current Password is required.')
  .min(6, 'Username should be between 6-30')
  .max(30, 'Username should be between 6-30')
  .matches(/^[a-zA-Z0-9_]+$/, 'Username should be between a-z 0-9 '),
  newPassword: Yup.string().required('New Password is required.')
  .min(6, 'New Password should be between 6-30')
  .max(30, 'New Password should be between 6-30')
  .matches(/^[a-zA-Z0-9_]+$/, 'New Password should be between a-z 0-9 '),
  repeatPassword: Yup.string().required('Repeat Password is required.')
  .min(6, 'Repeat Password should be between 6-30')
  .max(30, 'Repeat Password should be between 6-30')
  .matches(/^[a-zA-Z0-9_]+$/, 'Repeat Password should be between a-z 0-9 ')
  .test('passwords-match', 'Passwords must match', function(value) { 
    return this.parent.newPassword === value;
  }),
});
