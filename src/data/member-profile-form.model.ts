import * as Yup from 'yup';

export class MemberProfileFormData {
  constructor(
    readonly firstName: string,
    readonly lastName: string,
    readonly relationship: string,
    readonly idCardNumber: string,
    readonly email: string,
    readonly contactNumber1: string,
    readonly contactNumber2: string,
    readonly address1: string,
    readonly address2: string,
    readonly address3: string) {

  }

  static empty(): MemberProfileFormData {
    return new MemberProfileFormData(
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      ''
    );
  }
}

export const MemberProfileFormSchema = Yup.object().shape({
    firstName: Yup.string().required('Fist Name is required.'),
    lastName: Yup.string().required('Last Name is required.'),
    relationship: Yup.string().required('relationShip is required.'),
    idCardNumber: Yup.string().required('ID Card Number is required.')
    .matches(/^[0-9]+$/, 'ID Card Number should be between 0-9.'),
    email: Yup.string().required('Email is required.').email('Invalid email.'),
    contactNumber1: Yup.string().required('Contact is required.'),
    address1: Yup.string().required('Address is required.')
});

