import * as Yup from 'yup';

export class ConfigData {

  constructor(
    readonly baseURL: string,
    readonly vanMachine: string) {

  }

  static empty(): ConfigData {
    return new ConfigData(
      '',
      '',
    );
  }
}

export const ConfigSchema = Yup.object().shape({
  baseURL: Yup.string().required('กรุณากรอกเว็ปเซอร์วิส'),
  vanMachine: Yup.string().required('กรุณากรอกหมายเลขรถ'),
});

