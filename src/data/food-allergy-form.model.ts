import * as Yup from 'yup';

export class FoodAllergyFormData {

  constructor(
    readonly name: string,
    readonly description: string) {

  }

  static empty(): FoodAllergyFormData {
    return new FoodAllergyFormData(
      '',
      ''
    );
  }
}

export const FoodAllergyFormSchema = Yup.object().shape({
  name: Yup.string().required('Name is required.')
});

