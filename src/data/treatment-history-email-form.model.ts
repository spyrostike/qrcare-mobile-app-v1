import * as Yup from 'yup';

export class TreatmentHistoryEmailFormData {

  constructor(
    readonly emailFrom: string,
    readonly emailTo: string,
    readonly subject: string,
    readonly description: string) {
  }

  static empty(emailFrom): TreatmentHistoryEmailFormData {
    return new TreatmentHistoryEmailFormData(
      emailFrom,
      '',
      '',
      ''
    );
  }
}

export const TreatmentHistoryEmailFormSchema = Yup.object().shape({
  emailFrom: Yup.string().required('Email From is required.').email('Invalid email.'),
  emailTo: Yup.string().required('Email To is required.').email('Invalid email.'),
  subject: Yup.string().required('Subject is required.')
});

