import * as Yup from 'yup';

export class MedicalFormData {

  constructor(
    readonly hospital: string,
    readonly hospitalTel: string,
    readonly patientId: string,
    readonly doctorName: string,
    readonly doctorContact: string,
    readonly congenitalDisorder: string) {
  }

  static empty(): MedicalFormData {
    return new MedicalFormData(
      '',
      '',
      '',
      '',
      '',
      ''
    );
  }
}

export const MedicalFormSchema = Yup.object().shape({
  hospital: Yup.string().required('Hospital is required.'),
  hospitalTel: Yup.string().required('Hospital Tel is required.'),
  patientId: Yup.string().required('Patient ID is required.'),
  doctorName: Yup.string().required('Doctor’s Name is required.'),
  doctorContact: Yup.string().required('Doctor’s Contact is required.'),
  congenitalDisorder: Yup.string().required('Congenital Disorder is required.')
});

