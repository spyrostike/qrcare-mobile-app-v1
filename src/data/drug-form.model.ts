import * as Yup from 'yup';

export class DrugFormData {

  constructor(
    readonly name: string,
    readonly description: string) {

  }

  static empty(): DrugFormData {
    return new DrugFormData(
      '',
      ''
    );
  }
}

export const DrugFormSchema = Yup.object().shape({
  name: Yup.string().required('Name is required.')
});

