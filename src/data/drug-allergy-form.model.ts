import * as Yup from 'yup';

export class DrugAllergyFormData {

  constructor(
    readonly name: string,
    readonly description: string) {

  }

  static empty(): DrugAllergyFormData {
    return new DrugAllergyFormData(
      '',
      ''
    );
  }
}

export const DrugAllergyFormSchema = Yup.object().shape({
  name: Yup.string().required('Name is required.')
});

