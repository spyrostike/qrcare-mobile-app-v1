import * as Yup from 'yup';

export class CreatureFormData {

  constructor(
    readonly firstName: string,
    readonly lastName: string,
    readonly weight: string,
    readonly height: string) {

  }

  static empty(): CreatureFormData {
    return new CreatureFormData(
      '',
      '',
      '',
      ''
    );
  }
}

export const CreatureFormSchema = Yup.object().shape({
  firstName: Yup.string().required('Fist Name is required.'),
  lastName: Yup.string().required('Last Name is required.'),
  weight: Yup.string().required('Weight is required.')
  .matches(/^\d+(\.\d{1,2})?$/, 'Weight should be between 0-9.(Ex. 12.34)')
  .max(30, 'Username should not be over 5 characters'),
  height: Yup.string().required('Height is required.')
  .matches(/^\d+(\.\d{1,2})?$/, 'Height should be between 0-9.(Ex. 12.34)')
  .max(30, 'Username should not be over 5 characters'),
});

