import React from 'react';
import { RouteProp } from '@react-navigation/core';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { AppRoute } from './app-routes';
import { AppNavigatorParams } from './app.navigator';
import { 
  SignUpScreen,
  SignUpContactScreen,
  SignUpLocationScreen,
  SignUpProfileScreen
 } from '../scenes/sign-up';

type SignUpNavigatorParams = AppNavigatorParams & {
  [AppRoute.SIGN_UP]: undefined;
  [AppRoute.SIGN_UP_CONTACT]: undefined;
  [AppRoute.SIGN_UP_LOCATION]: undefined;
  [AppRoute.SIGN_UP_PROFILE]: undefined;
}

export interface SignUpScreenProps {
  navigation: StackNavigationProp<SignUpNavigatorParams, AppRoute.SIGN_UP>;
  route: RouteProp<SignUpNavigatorParams, AppRoute.SIGN_UP>;
}

export interface SignUpContactScreenProps {
  navigation: StackNavigationProp<SignUpNavigatorParams, AppRoute.SIGN_UP_CONTACT>;
  route: RouteProp<SignUpNavigatorParams, AppRoute.SIGN_UP_CONTACT>;
}

export interface SignUpLocationScreenProps {
  navigation: StackNavigationProp<SignUpNavigatorParams, AppRoute.SIGN_UP_LOCATION>;
  route: RouteProp<SignUpNavigatorParams, AppRoute.SIGN_UP_LOCATION>;
}

export interface SignUpProfileScreenProps {
  navigation: StackNavigationProp<SignUpNavigatorParams, AppRoute.SIGN_UP_PROFILE>;
  route: RouteProp<SignUpNavigatorParams, AppRoute.SIGN_UP_PROFILE>;
}

const Stack = createStackNavigator<SignUpNavigatorParams>();

export const SignUpNavigator = (): React.ReactElement => (
  <Stack.Navigator headerMode='none'>
    <Stack.Screen name={AppRoute.SIGN_UP} component={SignUpScreen}/>
    <Stack.Screen name={AppRoute.SIGN_UP_CONTACT} component={SignUpContactScreen}/>
    <Stack.Screen name={AppRoute.SIGN_UP_LOCATION} component={SignUpLocationScreen}/>
    <Stack.Screen name={AppRoute.SIGN_UP_PROFILE} component={SignUpProfileScreen}/>
  </Stack.Navigator>
);
