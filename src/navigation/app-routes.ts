export enum AppRoute {
  AUTH = 'Auth',
  CHANGE_PASSWORD = 'Change Password',
  CREATURE_FORM = 'Creature Form',
  CREATURE_INFO = 'Creature Info',
  DRUG_FORM = 'Drug Form',
  DRUG_INFO = 'Drug Info',
  DRUG_LIST = 'Drug List',
  DRUG_ALLERGY_FORM = 'Drug Allergy Form',
  DRUG_ALLERGY_INFO = 'Drug Allergy Info',
  DRUG_ALLERGY_LIST = 'Drug Allergy List',
  ENTRANCE = 'Entrance',
  FOOD_ALLERGY_FORM = 'Food Allergy Form',
  FOOD_ALLERGY_INFO = 'Food Allergy Info',
  FOOD_ALLERGY_LIST = 'Food Allergy List',
  GUEST_CREATURE = 'Guest Creature',
  GUEST_CREATURE_INFO = 'Guest Creature Info',
  GUEST_DRUG_INFO = 'Guest Drug Info',
  GUEST_DRUG_LIST = 'Guest Drug List',
  GUEST_DRUG_ALLERGY_INFO = 'Guest Drug Allergy Info',
  GUEST_DRUG_ALLERGY_LIST = 'Guest Drug Allergy List',
  GUEST_FOOD_ALLERGY_INFO = 'Guest Food Allergy Info',
  GUEST_FOOD_ALLERGY_LIST = 'Guest Food Allergy List',
  GUEST_MEDICAL_INFO = 'Guest Medical Info',
  GUEST_LOCATION_INFO = 'Guest Location Info',
  GUEST_PARENT_INFO = 'Guest Parent Info',
  GUEST_TREATMENT_HISTORY_FILES = 'Guest Treatment History Files',
  GUEST_TREATMENT_HISTORY_INFO = 'Guest Treatment History Info',
  GUEST_TREATMENT_HISTORY_LIST = 'Guest Treatment History List',
  GUEST_VACCINE_INFO = 'Guest Vaccine Info',
  GUEST_VACCINE_BOOK_INFO = 'Guest Vaccine Book Info',
  GUEST_VACCINE_BOOK_LIST = 'Guest Vaccine Book List',
  LOADING = 'Loading',
  LOCATION = 'Location',
  HOME = 'Home',
  MEDICAL_FORM = 'Medical Form',
  MEDICAL_INFO = 'Medical Info',
  PROFILE = 'Profile',
  RESET_PASSWORD = 'Reset Password',
  SCAN_QR_CODE = 'Scan QR Code',
  SIGN_IN = 'Sign In',
  SIGN_UP = 'Sign Up',
  SIGN_UP_ADDRESS = 'Sign Up Address',
  SIGN_UP_CONTACT = 'Sign Up Contract',
  SIGN_UP_LOCATION = 'Sign Up Location',
  SIGN_UP_PROFILE = 'Sign Up Profile',
  TREATMENT_HISTORY_EMAIL_FORM = 'Treatment History Email Form',
  TREATMENT_HISTORY_FILES = 'Treatment History Files',
  TREATMENT_HISTORY_FORM = 'Treatment History Form',
  TREATMENT_HISTORY_INFO = 'Treatment History Info',
  TREATMENT_HISTORY_LIST = 'Treatment History List',
  VACCINE_FORM = 'Vaccine Form',
  VACCINE_INFO = 'Vaccine Info',
  VACCINE_BOOK_FORM = 'Vaccine Book Form',
  VACCINE_BOOK_INFO = 'Vaccine Book Info',
  VACCINE_BOOK_LIST = 'Vaccine Book List',
  VIDEO_PRESENT = 'Video Present',

  // RESET_PASSWORD = 'Reset Password',
  ORDER = 'Order',
  ORDER_MENU = 'Order Menu',
  ORDER_SALES = 'Order Sales',
  ORDER_SALES_CALCULATE = 'Order Sales Calculate',
  ORDER_SALES_SUMMARY = 'Order Sales Summary',
  SPLASH = 'SPLASH',
  TODO = 'ToDo',
  TODO_IN_PROGRESS = 'ToDo In Progress',
  TODO_DONE = 'ToDo Done',
  TODO_DETAILS = 'ToDo Details',
  PRODUCT_FORM = 'Product Form',
  PRODUCT_LIST = 'Product List',

  ABOUT = 'About',
}
