import React from 'react';
import { RouteProp } from '@react-navigation/core';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { AppRoute } from './app-routes';
import { AppNavigatorParams } from './app.navigator';
import { VideoPresentScreen } from '../scenes/entrance';

type EntranceNavigatorParams = AppNavigatorParams & {
  [AppRoute.VIDEO_PRESENT]: undefined;
}

export interface VideoPresentScreenProps {
  navigation: StackNavigationProp<EntranceNavigatorParams, AppRoute.VIDEO_PRESENT>;
  route: RouteProp<EntranceNavigatorParams, AppRoute.VIDEO_PRESENT>;
}

const Stack = createStackNavigator<EntranceNavigatorParams>();

export const EntranceNavigator = (): React.ReactElement => (
  <Stack.Navigator headerMode='none'>
    <Stack.Screen name={AppRoute.VIDEO_PRESENT} component={VideoPresentScreen}/>
  </Stack.Navigator>
);
