import React from 'react';
import { RouteProp } from '@react-navigation/core';
import {
  createDrawerNavigator,
  DrawerContentComponentProps,
  DrawerNavigationProp,
} from '@react-navigation/drawer';
import { AppRoute } from './app-routes';
import { 
  ChangePasswordScreen,
  CreatureFormScreen,
  CreatureInfoScreen,
  DrugFormScreen,
  DrugInfoScreen,
  DrugListScreen,
  DrugAllergyFormScreen,
  DrugAllergyInfoScreen,
  DrugAllergyListScreen,
  FoodAllergyFormScreen,
  FoodAllergyInfoScreen,
  FoodAllergyListScreen,
  HomeDrawer, 
  HomeScreen, 
  LocationScreen,
  MedicalFormScreen, 
  MedicalInfoScreen,
  ProfileScreen,
  ScanQRCodeScreen,
  TreatmentHistoryEmailFormScreen,
  TreatmentHistoryFilesScreen,
  TreatmentHistoryFormScreen, 
  TreatmentHistoryInfoScreen,
  TreatmentHistoryListScreen,
  VaccineFormScreen,
  VaccineInfoScreen,
  VaccineBookFormScreen,
  VaccineBookInfoScreen,
  VaccineBookListScreen
} from '../scenes/home';

type HomeDrawerNavigatorParams = {
  [AppRoute.CHANGE_PASSWORD]: undefined;
  [AppRoute.CREATURE_FORM]: { action: 'add' | 'edit', creatureId?: string };
  [AppRoute.CREATURE_INFO]: { creatureId: string };
  [AppRoute.DRUG_FORM]: { action: 'add' | 'edit', creatureId?: string, drugId?: string }; 
  [AppRoute.DRUG_INFO]: { drugId: string }; 
  [AppRoute.DRUG_LIST]: { creatureId: string }; 
  [AppRoute.DRUG_ALLERGY_FORM]: { action: 'add' | 'edit', creatureId?: string, drugAllergyId?: string }; 
  [AppRoute.DRUG_ALLERGY_INFO]: { drugAllergyId: string }; 
  [AppRoute.DRUG_ALLERGY_LIST]: { creatureId: string }; 
  [AppRoute.FOOD_ALLERGY_FORM]: { action: 'add' | 'edit', creatureId?: string, foodAllergyId?: string }; 
  [AppRoute.FOOD_ALLERGY_INFO]: { foodAllergyId: string }; 
  [AppRoute.FOOD_ALLERGY_LIST]: { creatureId: string }; 
  [AppRoute.HOME]: undefined;
  [AppRoute.LOCATION]: undefined;
  [AppRoute.MEDICAL_FORM]: { action: 'add' | 'edit', creatureId: string, medicalId?: string };
  [AppRoute.MEDICAL_INFO]: { creatureId: string }; 
  [AppRoute.PROFILE]: undefined; 
  [AppRoute.SCAN_QR_CODE]: { action: 'creature-add' | 'creature-info' };
  [AppRoute.TREATMENT_HISTORY_EMAIL_FORM]: { creatureId: string, treatmentHistoryIds: string[] };
  [AppRoute.TREATMENT_HISTORY_FILES]: { treatmentHistoryId: string }; 
  [AppRoute.TREATMENT_HISTORY_FORM]: { action: 'add' | 'edit', creatureId?: string, treatmentHistoryId?: string }; 
  [AppRoute.TREATMENT_HISTORY_INFO]: { treatmentHistoryId: string }; 
  [AppRoute.TREATMENT_HISTORY_LIST]: { creatureId: string }; 
  [AppRoute.VACCINE_FORM]: { action: 'add' | 'edit', creatureId?: string, vaccineBookId?: string, vaccineId?: string }; 
  [AppRoute.VACCINE_INFO]: { creatureId?: string, vaccineId: string }; 
  [AppRoute.VACCINE_BOOK_FORM]: { action: 'add' | 'edit', creatureId?: string, vaccineBookId?: string }; 
  [AppRoute.VACCINE_BOOK_INFO]: { creatureId?: string, vaccineBookId: string }; 
  [AppRoute.VACCINE_BOOK_LIST]: { creatureId: string }; 
}

export interface ChangePasswordScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.CHANGE_PASSWORD>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.CHANGE_PASSWORD>;
}

export interface CreatureInfoScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.CREATURE_INFO>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.CREATURE_INFO>;
}

export interface CreatureFormScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.CREATURE_FORM>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.CREATURE_FORM>;
}

export type DrawerHomeScreenProps = DrawerContentComponentProps & {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.HOME>;
};

export interface DrugFormScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.DRUG_FORM>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.DRUG_FORM>;
}

export interface DrugInfoScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.DRUG_INFO>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.DRUG_INFO>;
}

export interface DrugListScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.DRUG_LIST>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.DRUG_LIST>;
}

export interface DrugAllergyFormScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.DRUG_ALLERGY_FORM>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.DRUG_ALLERGY_FORM>;
}

export interface DrugAllergyInfoScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.DRUG_ALLERGY_INFO>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.DRUG_ALLERGY_INFO>;
}

export interface DrugAllergyListScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.DRUG_ALLERGY_LIST>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.DRUG_ALLERGY_LIST>;
}

export interface FoodAllergyFormScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.FOOD_ALLERGY_FORM>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.FOOD_ALLERGY_FORM>;
}

export interface FoodAllergyInfoScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.FOOD_ALLERGY_INFO>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.FOOD_ALLERGY_INFO>;
}

export interface FoodAllergyListScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.FOOD_ALLERGY_LIST>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.FOOD_ALLERGY_LIST>;
}

export interface HomeScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.HOME>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.HOME>;
}

export interface LocationScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.LOCATION>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.LOCATION>;
}

export interface MedicalInfoScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.MEDICAL_INFO>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.MEDICAL_INFO>;
}

export interface MedicalFormScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.MEDICAL_FORM>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.MEDICAL_FORM>;
}

export interface ProfileScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.PROFILE>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.PROFILE>;
}

export interface ScanQRCodeScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.SCAN_QR_CODE>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.SCAN_QR_CODE>;
}

export  interface TreatmentHistoryEmailFormScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.TREATMENT_HISTORY_EMAIL_FORM>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.TREATMENT_HISTORY_EMAIL_FORM>;
}

export  interface TreatmentHistoryFormScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.TREATMENT_HISTORY_FORM>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.TREATMENT_HISTORY_FORM>;
}

export interface TreatmentHistoryInfoScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.TREATMENT_HISTORY_INFO>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.TREATMENT_HISTORY_INFO>;
}

export interface TreatmentHistoryListScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.TREATMENT_HISTORY_LIST>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.TREATMENT_HISTORY_LIST>;
}

export interface TreatmentHistoryFilesScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.TREATMENT_HISTORY_FILES>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.TREATMENT_HISTORY_FILES>;
}

export interface VaccineFormScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.VACCINE_FORM>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.VACCINE_FORM>;
}

export interface VaccineInfoScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.VACCINE_INFO>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.VACCINE_INFO>;
}

export interface VaccineBookFormScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.VACCINE_BOOK_FORM>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.VACCINE_BOOK_FORM>;
}

export interface VaccineBookInfoScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.VACCINE_BOOK_INFO>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.VACCINE_BOOK_INFO>;
}

export interface VaccineBookListScreenProps {
  navigation: DrawerNavigationProp<HomeDrawerNavigatorParams, AppRoute.VACCINE_BOOK_LIST>;
  route: RouteProp<HomeDrawerNavigatorParams, AppRoute.VACCINE_BOOK_LIST>;
}

const Drawer = createDrawerNavigator<HomeDrawerNavigatorParams>();

export const HomeNavigator = (): React.ReactElement => (
  // @ts-ignore: `drawerContent` also contains a DrawerNavigationProp
  <Drawer.Navigator edgeWidth={0} drawerContent={(props) => <HomeDrawer {...props}/>} initialRouteName={AppRoute.HOME} screenOptions={{ unmountOnBlur: true }}>
    <Drawer.Screen name={AppRoute.HOME} component={HomeScreen} options={{unmountOnBlur: true}}/>
    <Drawer.Screen name={AppRoute.CHANGE_PASSWORD} component={ChangePasswordScreen} />
    <Drawer.Screen name={AppRoute.CREATURE_FORM} component={CreatureFormScreen} />
    <Drawer.Screen name={AppRoute.CREATURE_INFO} component={CreatureInfoScreen} />
    <Drawer.Screen name={AppRoute.DRUG_FORM} component={DrugFormScreen} />
    <Drawer.Screen name={AppRoute.DRUG_INFO} component={DrugInfoScreen} />
    <Drawer.Screen name={AppRoute.DRUG_LIST} component={DrugListScreen} />
    <Drawer.Screen name={AppRoute.DRUG_ALLERGY_FORM} component={DrugAllergyFormScreen} />
    <Drawer.Screen name={AppRoute.DRUG_ALLERGY_INFO} component={DrugAllergyInfoScreen} />
    <Drawer.Screen name={AppRoute.DRUG_ALLERGY_LIST} component={DrugAllergyListScreen} />
    <Drawer.Screen name={AppRoute.FOOD_ALLERGY_FORM} component={FoodAllergyFormScreen} />
    <Drawer.Screen name={AppRoute.FOOD_ALLERGY_INFO} component={FoodAllergyInfoScreen} />
    <Drawer.Screen name={AppRoute.FOOD_ALLERGY_LIST} component={FoodAllergyListScreen} />
    <Drawer.Screen name={AppRoute.LOCATION} component={LocationScreen} />
    <Drawer.Screen name={AppRoute.MEDICAL_FORM} component={MedicalFormScreen} />
    <Drawer.Screen name={AppRoute.MEDICAL_INFO} component={MedicalInfoScreen} />
    <Drawer.Screen name={AppRoute.PROFILE} component={ProfileScreen} />
    <Drawer.Screen name={AppRoute.SCAN_QR_CODE} component={ScanQRCodeScreen} />
    <Drawer.Screen name={AppRoute.TREATMENT_HISTORY_EMAIL_FORM} component={TreatmentHistoryEmailFormScreen} />
    <Drawer.Screen name={AppRoute.TREATMENT_HISTORY_FILES} component={TreatmentHistoryFilesScreen} />
    <Drawer.Screen name={AppRoute.TREATMENT_HISTORY_FORM} component={TreatmentHistoryFormScreen} />
    <Drawer.Screen name={AppRoute.TREATMENT_HISTORY_INFO}  component={TreatmentHistoryInfoScreen} />
    <Drawer.Screen name={AppRoute.TREATMENT_HISTORY_LIST} component={TreatmentHistoryListScreen} />
    <Drawer.Screen name={AppRoute.VACCINE_FORM} component={VaccineFormScreen} />
    <Drawer.Screen name={AppRoute.VACCINE_INFO} component={VaccineInfoScreen} />
    <Drawer.Screen name={AppRoute.VACCINE_BOOK_FORM} component={VaccineBookFormScreen} />
    <Drawer.Screen name={AppRoute.VACCINE_BOOK_INFO} component={VaccineBookInfoScreen} />
    <Drawer.Screen name={AppRoute.VACCINE_BOOK_LIST} component={VaccineBookListScreen} />
  </Drawer.Navigator>
);

