import React from 'react';
import { RouteProp } from '@react-navigation/core';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { AppRoute } from './app-routes';
import { AppNavigatorParams } from './app.navigator';
import { 
    GuestCreatureInfoScreen, 
    GuestDrugInfoScreen,  
    GuestDrugListScreen,  
    GuestDrugAllergyInfoScreen,  
    GuestDrugAllergyListScreen,  
    GuestFoodAllergyInfoScreen,
    GuestFoodAllergyListScreen,
    GuestLocationInfoScreen,
    GuestMedicalInfoScreen,
    GuestParentInfoScreen,
    GuestTreatmentHistoryFilesScreen,
    GuestTreatmentHistoryInfoScreen,
    GuestTreatmentHistoryListScreen,
    GuestVaccineInfoScreen,
    GuestVaccineBookInfoScreen,
    GuestVaccineBookListScreen
} from '../scenes/guest-creature';

type GuestCreatureNavigatorParams = AppNavigatorParams & {
  [AppRoute.GUEST_CREATURE_INFO]: undefined;
  [AppRoute.GUEST_DRUG_INFO]: { drugId: string };
  [AppRoute.GUEST_DRUG_LIST]: { creatureId: string };
  [AppRoute.GUEST_DRUG_ALLERGY_INFO]: { drugAllergyId: string };
  [AppRoute.GUEST_DRUG_ALLERGY_LIST]: { creatureId: string };
  [AppRoute.GUEST_FOOD_ALLERGY_INFO]: { foodAllergyId: string };
  [AppRoute.GUEST_FOOD_ALLERGY_LIST]: { creatureId: string };
  [AppRoute.GUEST_LOCATION_INFO]: { parentId: string }; 
  [AppRoute.GUEST_MEDICAL_INFO]: { creatureId: string };
  [AppRoute.GUEST_PARENT_INFO]: { parentId: string }; 
  [AppRoute.GUEST_VACCINE_BOOK_LIST]: { creatureId: string }; 
  [AppRoute.GUEST_TREATMENT_HISTORY_FILES]: { treatmentHistoryId: string };
  [AppRoute.GUEST_TREATMENT_HISTORY_INFO]: { treatmentHistoryId: string };
  [AppRoute.GUEST_TREATMENT_HISTORY_LIST]: { creatureId: string };  [AppRoute.VACCINE_FORM]: { action: 'add' | 'edit', vaccineId?: string }; 
  [AppRoute.GUEST_VACCINE_INFO]: { vaccineId: string }; 
  [AppRoute.GUEST_VACCINE_BOOK_INFO]: { vaccineBookId: string }; 
  [AppRoute.GUEST_VACCINE_BOOK_LIST]: { creatureId: string }; 
}

export interface GuestCreatureInfoScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_CREATURE_INFO>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_CREATURE_INFO>;
}

export interface GuestDrugInfoScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_DRUG_INFO>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_DRUG_INFO>;
}

export interface GuestDrugListScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_DRUG_LIST>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_DRUG_LIST>;
}

export interface GuestDrugAllergyInfoScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_DRUG_ALLERGY_INFO>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_DRUG_ALLERGY_INFO>;
}

export interface GuestDrugAllergyListScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_DRUG_ALLERGY_LIST>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_DRUG_ALLERGY_LIST>;
}

export interface GuestFoodAllergyInfoScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_FOOD_ALLERGY_INFO>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_FOOD_ALLERGY_INFO>;
}

export interface GuestFoodAllergyListScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_FOOD_ALLERGY_LIST>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_FOOD_ALLERGY_LIST>;
}

export interface GuestLocationInfoScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_LOCATION_INFO>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_LOCATION_INFO>;
}

export interface GuestMedicalInfoScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_MEDICAL_INFO>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_MEDICAL_INFO>;
}

export interface GuestParentInfoScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_PARENT_INFO>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_PARENT_INFO>;
}

export interface GuestTreatmentHistoryFilesScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_TREATMENT_HISTORY_FILES>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_TREATMENT_HISTORY_FILES>;
}

export interface GuestTreatmentHistoryInfoScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_TREATMENT_HISTORY_INFO>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_TREATMENT_HISTORY_INFO>;
}

export interface GuestTreatmentHistoryListScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_TREATMENT_HISTORY_LIST>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_TREATMENT_HISTORY_LIST>;
}

export interface GuestVaccineInfoScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_VACCINE_INFO>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_VACCINE_INFO>;
}

export interface GuestVaccineBookInfoScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_VACCINE_BOOK_INFO>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_VACCINE_BOOK_INFO>;
}

export interface GuestVaccineBookListScreenProps {
  navigation: StackNavigationProp<GuestCreatureNavigatorParams, AppRoute.GUEST_VACCINE_BOOK_LIST>;
  route: RouteProp<GuestCreatureNavigatorParams, AppRoute.GUEST_VACCINE_BOOK_LIST>;
}

const Stack = createStackNavigator<GuestCreatureNavigatorParams>();

export const GuestCreature = (): React.ReactElement => (
  <Stack.Navigator headerMode='none'>
    <Stack.Screen name={AppRoute.GUEST_CREATURE_INFO} component={GuestCreatureInfoScreen}/>
    <Stack.Screen name={AppRoute.GUEST_DRUG_INFO} component={GuestDrugInfoScreen}/>
    <Stack.Screen name={AppRoute.GUEST_DRUG_LIST} component={GuestDrugListScreen}/>
    <Stack.Screen name={AppRoute.GUEST_DRUG_ALLERGY_INFO} component={GuestDrugAllergyInfoScreen}/>
    <Stack.Screen name={AppRoute.GUEST_DRUG_ALLERGY_LIST} component={GuestDrugAllergyListScreen}/>
    <Stack.Screen name={AppRoute.GUEST_FOOD_ALLERGY_INFO} component={GuestFoodAllergyInfoScreen}/>
    <Stack.Screen name={AppRoute.GUEST_FOOD_ALLERGY_LIST} component={GuestFoodAllergyListScreen}/>
    <Stack.Screen name={AppRoute.GUEST_LOCATION_INFO} component={GuestLocationInfoScreen}/>
    <Stack.Screen name={AppRoute.GUEST_MEDICAL_INFO} component={GuestMedicalInfoScreen}/>
    <Stack.Screen name={AppRoute.GUEST_PARENT_INFO} component={GuestParentInfoScreen}/>
    <Stack.Screen name={AppRoute.GUEST_TREATMENT_HISTORY_FILES} component={GuestTreatmentHistoryFilesScreen}/>
    <Stack.Screen name={AppRoute.GUEST_TREATMENT_HISTORY_INFO} component={GuestTreatmentHistoryInfoScreen}/>
    <Stack.Screen name={AppRoute.GUEST_TREATMENT_HISTORY_LIST} component={GuestTreatmentHistoryListScreen}/>
    <Stack.Screen name={AppRoute.GUEST_VACCINE_INFO} component={GuestVaccineInfoScreen}/>
    <Stack.Screen name={AppRoute.GUEST_VACCINE_BOOK_INFO} component={GuestVaccineBookInfoScreen}/>
    <Stack.Screen name={AppRoute.GUEST_VACCINE_BOOK_LIST} component={GuestVaccineBookListScreen}/>
  </Stack.Navigator>
);
