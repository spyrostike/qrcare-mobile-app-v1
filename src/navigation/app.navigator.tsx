import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { AppRoute } from './app-routes';
import { AuthNavigator } from './auth.navigator';
import { EntranceNavigator } from './entrance.navigator';
import { GuestCreature } from './guest-creature.navigator';
import { HomeNavigator } from './home.navigator';
import { ScanQRCodeNavigator } from './scan-qr-code.navigator';
import { SignUpNavigator } from './sign-up.navigator';
import { SplashNavigator } from './splash.navigator';
import { useStoreState } from "../store";

type StackNavigatorProps = React.ComponentProps<typeof Stack.Navigator>;

export type AppNavigatorParams = {
  [AppRoute.AUTH]: undefined;
  [AppRoute.GUEST_CREATURE]: undefined;
  [AppRoute.HOME]: undefined;
  [AppRoute.ENTRANCE]: undefined;
  [AppRoute.SCAN_QR_CODE]: undefined;
  [AppRoute.SIGN_UP]: undefined;
  [AppRoute.SPLASH]: undefined;
}

const Stack = createStackNavigator<AppNavigatorParams>();

export const AppNavigator = (props: Partial<StackNavigatorProps>): React.ReactElement => {
  const state = useStoreState(s => s.app.state)
  
  const renderNavigaterRoute = () => {
    if (state === AppRoute.LOADING) {
      return (
        <Stack.Navigator {...props} headerMode='none'>
          <Stack.Screen name={AppRoute.SPLASH} component={SplashNavigator}/>
        </Stack.Navigator>
      )
    } else {
      return (
        <Stack.Navigator {...props} headerMode='none'>
          { renderRoute() }
        </Stack.Navigator>
        )
      
    }
  }
  
  const renderRoute = () => {
    if (state === AppRoute.ENTRANCE) {
      return (
        <Stack.Screen name={AppRoute.ENTRANCE} component={EntranceNavigator}/>
      )
    } else if (state === AppRoute.AUTH) {
      return (
        <Stack.Screen name={AppRoute.AUTH} component={AuthNavigator}/>
      )
    } else if (state === AppRoute.HOME) {
      return (
        <Stack.Screen name={AppRoute.HOME} component={HomeNavigator}/>
      )
    } else if (state === AppRoute.SCAN_QR_CODE) {
      return (
        <Stack.Screen name={AppRoute.SCAN_QR_CODE} component={ScanQRCodeNavigator}/>
      )
    } else if (state === AppRoute.SIGN_UP) {
      return (
        <Stack.Screen name={AppRoute.SIGN_UP} component={SignUpNavigator}/>
      )
    } else if (state === AppRoute.GUEST_CREATURE) {
      return (
        <Stack.Screen name={AppRoute.GUEST_CREATURE} component={GuestCreature}/>
      )
    }
  }

  return (
    <>
      {
        renderNavigaterRoute()
      }
    </>
    // <Stack.Navigator {...props} headerMode='none'>
    //   {
    //     1 !== 1 ? (
    //       <>
    //         <Stack.Screen name={AppRoute.SPLASH} component={SplashNavigator}/>
    //       </>
    //     ) : (
    //       <Stack.Screen name={AppRoute.AUTH} component={AuthNavigator}/>
    //     )
    //   }
      
      
    //   <Stack.Screen name={AppRoute.HOME} component={HomeNavigator}/>
    // </Stack.Navigator>
  )
};
