import React from 'react';
import { CompositeNavigationProp, RouteProp } from '@react-navigation/core';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { AppNavigatorParams } from './app.navigator';
import { AppRoute } from './app-routes';
import { SplashScreen } from '../scenes/splash';

type SplashNavigatorParams = AppNavigatorParams & {
  [AppRoute.SPLASH]: undefined;
}

export interface SplashScreenProps {
  navigation: StackNavigationProp<SplashNavigatorParams, AppRoute.SPLASH>;
  route: RouteProp<SplashNavigatorParams, AppRoute.SPLASH>;
}

const Stack = createStackNavigator<SplashNavigatorParams>();

export const SplashNavigator = (): React.ReactElement => (
  <Stack.Navigator headerMode='none'>
    <Stack.Screen name={AppRoute.SPLASH} component={SplashScreen}/>
  </Stack.Navigator>
);
