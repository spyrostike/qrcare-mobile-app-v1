import React from 'react';
import { RouteProp } from '@react-navigation/core';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { AppRoute } from './app-routes';
import { AppNavigatorParams } from './app.navigator';
import { ScanQRCodeScreen } from '../scenes/scan-qr-code';

type ScanQRCodeNavigatorParams = AppNavigatorParams & {
  [AppRoute.SCAN_QR_CODE]: undefined;
}

export interface ScanQRCodeScreenProps {
  navigation: StackNavigationProp<ScanQRCodeNavigatorParams, AppRoute.SCAN_QR_CODE>;
  route: RouteProp<ScanQRCodeNavigatorParams, AppRoute.SCAN_QR_CODE>;
}

const Stack = createStackNavigator<ScanQRCodeNavigatorParams>();

export const ScanQRCodeNavigator = (): React.ReactElement => (
  <Stack.Navigator headerMode='none'>
    <Stack.Screen name={AppRoute.SCAN_QR_CODE} component={ScanQRCodeScreen}/>
  </Stack.Navigator>
);
