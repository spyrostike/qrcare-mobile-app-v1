//////////////////////////////////////////// APP Version //////////////////////////////////////
export const APP_NAME = 'QR Care';
export const APP_VERSION_FULL = '3.0.0 (16122019)';
export const APP_VERSION = '3.0.0';


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// Theme Color /////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////// Main Color Theme ///////////////////////////////////
export const MainTheme = {
    colorPrimary: '#000000',
    colorSecondary: '#FFFFFF',
    colorTertiary: '#2FBA74',
    colorQuaternary: '#1C8365',
    colorQuinary: '#2E858E',
    colorSenary: '#8AB291',
    colorSeptenary: '#D4EADE',
    colorOctonary: '#FFB6BB',
    colorNonary: '#D9D9D9',
    colorDenary: '#0084C5',
    colorElevendary: '#E63C49',
    colorDuodenary: '#848b79',
    colorThirteendary: '#FFB6BB',
    colorFourteendary: '#A31A34',
    colorEnabled: '#8FEA80',
    colorDisabled: '#B6B6B4',
    activePrimary: '#8FEA80',
    inActivePrimary: '#FFFFFF',
    colorSuccess: '#00FF00',
    colorDanger: '#FF0000',
    placeholder: '#000000',
    searchBarInput: '#347C17',
    searchBarContainer: '#C3FDB8',
    searchHeaderListItems: '#F9F995',
    colorHeaderSection: '#F9F995',
    placeholerTextInput:'#D6D7DA',
    buttonColor: '#3BB9FF',
    itemBackGround: '#3BB9FF'
}


///////////////////////////////////////////////////////////////////////////////////////////////