import { createStore, createTypedHooks, persist } from "easy-peasy";
import model, { StoreModel } from "./models";
import { AsyncStorage } from "react-native";

const { useStoreDispatch, useStore, useStoreActions , useStoreState } = createTypedHooks<StoreModel>();

export { useStoreDispatch, useStoreActions, useStore , useStoreState };

const storage = {
  async getItem(key) {
    const str = await AsyncStorage.getItem(key)
    return JSON.parse(str || '{}')
  },
  setItem(key, data) {
    AsyncStorage.setItem(key, JSON.stringify(data))
  },
  removeItem(key) {
    AsyncStorage.removeItem(key)
  },
}

const store = createStore(persist(model, { storage: storage, whitelist: ['member'] }));

export default store;