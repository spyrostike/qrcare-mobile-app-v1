import { Action, action, Thunk , thunk } from 'easy-peasy';
import api from '../../resources/api'
import { API_STATUS_SUCCESS } from '../../../app-config';
import { ResponseModel } from '../../resources/response-model';
import { convertMedicalFromServerToApp } from '../../utils/ObjectUtils'
import { CriteriaSearch } from '../../model/criteria-search.model';
import { serializeQueryString } from '../../utils/HttpUtils';

export interface MedicalProps {
  id: string;
  hospitalName: string;
  hospitalTel: string;
  patientId: string;
  bloodType: string;
  bloodTypeName: string;
  doctorName: string;
  doctorContactNo: string;
  congenitalDisorder: string;
  creatureId: string;
  birthCertificate: string;

}

export interface MedicalModel {
  item: MedicalProps | null;
  setItem: Action<MedicalModel, MedicalProps | null>;
  add: Thunk<MedicalModel, MedicalProps>,
  edit: Thunk<MedicalModel, MedicalProps>,
  getInfo: Thunk<MedicalModel, string>,
}

const drugModel : MedicalModel = { 
  item : null,
  setItem: action((state, payload) => {
    state.item = payload
  }),
  add: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/medical/add`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          // action.setItem(data.result.item)
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  edit: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/medical/update`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  getInfo: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/medical/get-medical-by-creature-id/${payload}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          try {
            const item: MedicalProps = convertMedicalFromServerToApp(data.result)
            resolve(item)
          } catch (error) {
            resolve({} as MedicalProps)
          }
          
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
};

export default drugModel;