import { Action, action, Thunk , thunk } from 'easy-peasy';
import api from '../../resources/api'
import { API_STATUS_SUCCESS } from '../../../app-config';
import { ResponseModel } from '../../resources/response-model';
import { convertTreamentHistoryFromServerToApp } from '../../utils/ObjectUtils'
import { CriteriaSearch } from '../../model/criteria-search.model';
import { serializeQueryString } from '../../utils/HttpUtils';

export interface TreatmentHistoryProps {
  id: string,
  date: Date,
  time: string,
  description: string,
  department: number,
  departmentName?: string,
  fileCategory: number,
  fileCategoryOther: string,
  fileCategoryName?: string,
  creatureId: string,
  imageId?: string;
  image?: string;
  imageThumbnailId?: string;
  imageThumbnail?: string;
  oldImageId?: string;
  oldImageThumbnailId?: string;
  imageCount?: string
}

export interface TreatmentHistoryEmailProps {
  items: string[];
  from: string;
  to: string;
  tag: string;
  subject: string;
  description: string;
}

export interface TreatmentHistoryModel {
  items: TreatmentHistoryProps[];
  item: TreatmentHistoryProps | null;
  setItems: Action<TreatmentHistoryModel, TreatmentHistoryProps[] | []>;
  setItem: Action<TreatmentHistoryModel, TreatmentHistoryProps | null>;
  add: Thunk<TreatmentHistoryModel, TreatmentHistoryProps>,
  edit: Thunk<TreatmentHistoryModel, TreatmentHistoryProps>,
  find: Thunk<TreatmentHistoryModel, CriteriaSearch>,
  getInfo: Thunk<TreatmentHistoryModel, string>,
  removeItems: Thunk<TreatmentHistoryModel, string[]>
  sendEmail: Thunk<TreatmentHistoryModel, TreatmentHistoryEmailProps>
}

const treatmentHistoryModel : TreatmentHistoryModel = {
  items: [],
  item : null,
  setItems: action((state, payload) => {
    state.items = payload
  }),
  setItem: action((state, payload) => {
    state.item = payload
  }),
  add: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/treatment-history/add`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  edit: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/treatment-history/update`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  find: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/treatment-history/list/${payload.creatureId}?${serializeQueryString(payload)}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        
        if (status === API_STATUS_SUCCESS) {
          const items: TreatmentHistoryProps[] = data.result.map(item => convertTreamentHistoryFromServerToApp(item))
          if (items.length > 0) { 
            action.setItems([ ...getState().items, ...items ])
            resolve(items)
          } else {
            reject('array = 0')
          }
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  getInfo: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/treatment-history/${payload}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          const item: TreatmentHistoryProps = convertTreamentHistoryFromServerToApp(data.result)
          resolve(item)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  removeItems: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/treatment-history/delete`, payload);
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  sendEmail: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/treatment-history/send-mail`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  })
};

export default treatmentHistoryModel;