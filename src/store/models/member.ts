import { Action, action, Thunk , thunk } from 'easy-peasy';
import api from '../../resources/api'
import { API_STATUS_SUCCESS } from '../../../app-config';
import { ResponseModel } from '../../resources/response-model';
import { convertMemberFromServerToApp } from '../../utils/ObjectUtils'

export interface TokenProps {
  value: string | null;
}

export interface MemberProps {
  id: string;
  code: string;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  gender: string;
  bloodTypeName?: string;
  bloodType: string;
  genderName?: string;
  birthDate: Date;
  relationship: string;
  identificationNo: string;
  email: string;
  contactNo1: string;
  contactNo2: string;
  address1: string;
  address2: string;
  address3: string;
  placeLatitude: number;
  placeLongitude: number;
  profileImageId?: string;
  profileImage?: string;
  profileImageThumbnailId?: string;
  profileImageThumbnail?: string;
  locationImageId?: string;
  locationImage?: string;
  locationImageThumbnailId?: string;
  locationImageThumbnail?: string;
  oldProfileImageId?: string;
  oldProfileImageThumbnailId?: string,
  oldLocationImageId?: string,
  oldLocationImageThumbnailId?: string,
  
  qrcodeItemId: string;
}

export interface MemberModel {
  token: TokenProps | null;
  item: MemberProps | null;
  setToken: Action<MemberModel, TokenProps | null>;
  setItem: Action<MemberModel, MemberProps | null>;
  checkUserExist: Thunk<MemberModel, { username: string }>
  checkEmailExist: Thunk<MemberModel, { email: string }>
  signIn: Thunk<MemberModel, MemberProps>,
  signUp: Thunk<MemberModel, MemberProps>,
  edit: Thunk<MemberModel, MemberProps>,
  getInfo: Thunk<MemberModel>,
  getInfoById: Thunk<MemberModel, string>,
  changePassword: Thunk<MemberModel, { currentPassword: string, newPassword: string }>,
  resetPassword: Thunk<MemberModel, { email: string }>,
}

const memberModel : MemberModel = {
  token: null,
  item : null,
  setToken: action((state, payload) => {
    state.token = payload
  }),
  setItem: action((state, payload) => {
    state.item = payload
  }),
  checkUserExist: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/member/check-username-exist`, payload);
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  checkEmailExist: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/member/check-email-exist`, payload);
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  signIn: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/member/authen`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          const item: MemberProps = convertMemberFromServerToApp(data.result.item)
          resolve({ token: data.result.token, item: item })
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  signUp: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/member/register`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          const item: MemberProps = convertMemberFromServerToApp(data.result.item)
          resolve({ token: data.result.token, item: item })
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  edit: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/member/update-profile`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          // action.setItem(data.result.item)
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  getInfo: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/member/profile`);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          const item: MemberProps = convertMemberFromServerToApp(data.result)
          resolve(item)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    
    })
  }),
  getInfoById: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/member/${payload}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          const item: MemberProps = convertMemberFromServerToApp(data.result)
          resolve(item)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    
    })
  }),
  changePassword: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/member/change-password`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          // action.setItem(data.result.item)
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  resetPassword: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/member/forget-password`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          // action.setItem(data.result.item)
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  })
};

export default memberModel;