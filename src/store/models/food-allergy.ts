import { Action, action, Thunk , thunk } from 'easy-peasy';
import api from '../../resources/api'
import { API_STATUS_SUCCESS } from '../../../app-config';
import { ResponseModel } from '../../resources/response-model';
import { convertFoodAllergyFromServerToApp } from '../../utils/ObjectUtils'
import { CriteriaSearch } from '../../model/criteria-search.model';
import { serializeQueryString } from '../../utils/HttpUtils';

export interface FoodAllergyProps {
  id: string;
  name: string;
  imageId?: string;
  image?: string;
  imageThumbnailId?: string;
  imageThumbnail?: string;
  oldImageId?: string;
  oldImageThumbnailId?: string;
  description: string;
  creatureId: string;
}

export interface FoodAllergyModel {
  items: FoodAllergyProps[];
  item: FoodAllergyProps | null;
  setItems: Action<FoodAllergyModel, FoodAllergyProps[] | []>;
  setItem: Action<FoodAllergyModel, FoodAllergyProps | null>;
  add: Thunk<FoodAllergyModel, FoodAllergyProps>,
  edit: Thunk<FoodAllergyModel, FoodAllergyProps>,
  find: Thunk<FoodAllergyModel, CriteriaSearch>,
  getInfo: Thunk<FoodAllergyModel, string>,
  removeItems: Thunk<FoodAllergyModel, string[]>
}

const foodAllergyModel : FoodAllergyModel = {
  items: [],
  item : null,
  setItems: action((state, payload) => {
    state.items = payload
  }),
  setItem: action((state, payload) => {
    state.item = payload
  }),
  add: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/food-allergy/add`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  edit: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/food-allergy/update`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  find: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/food-allergy/list/${payload.creatureId}?${serializeQueryString(payload)}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        
        if (status === API_STATUS_SUCCESS) {    
          const items: FoodAllergyProps[] = data.result.map(item => convertFoodAllergyFromServerToApp(item))

          if (items.length > 0) { 
            action.setItems([ ...getState().items, ...items ])
            resolve(items)
          } else {
            reject('array = 0')
          }
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  getInfo: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/food-allergy/${payload}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        
        if (status === API_STATUS_SUCCESS) {
          const item: FoodAllergyProps = convertFoodAllergyFromServerToApp(data.result)
          resolve(item)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  removeItems: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/food-allergy/delete`, payload);
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  })
};

export default foodAllergyModel;