import { Action, action , Thunk ,thunk } from 'easy-peasy';
import api from '../../resources/api'
import { API_STATUS_SUCCESS } from '../../../app-config';
import { ResponseModel } from '../../resources/response-model'
import { convertFileFromServerToApp, genenrateFileObjToServer } from '../../utils/ObjectUtils';
import { CriteriaSearch } from '../../model/criteria-search.model';
import { serializeQueryString } from '../../utils/HttpUtils';

export interface FileProps {
    id: string;
    uri: string;
    path: string;
    name: string;
    type: string;
    subType: string;
    size: string;
    refId: string;
    tag: string;
    status: string;
    createDate: string;
    createBy: string;
    editDate: string;
    editBy: string;
}

export interface FileModel {
  items: FileProps[];
  item: FileProps | null;
  setItems: Action<FileModel, FileProps[] | []>;
  setItem: Action<FileModel, FileProps | null>;
  add: Thunk<FileModel, FileProps>;
  find: Thunk<FileModel, CriteriaSearch>;
  removeItems: Thunk<FileModel, string[]>;
}

const fileModel : FileModel = {
  items: [],
  item : null,
  setItems: action((state, payload) => {
    state.items = payload
  }),
  setItem: action((state, payload) => {
    state.item = payload
  }),
  add: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/file/add`, genenrateFileObjToServer(payload));
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    
    })
  }),
  find: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/file/get-file-list?${serializeQueryString(payload)}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {    
          const items: FileProps[] = data.result.items.map(item => convertFileFromServerToApp(item))
          if (items.length > 0) { 
            action.setItems([ ...getState().items, ...items ])
            resolve(items)
          } else {
            reject('array = 0')
          }
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  removeItems: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/file/delete`, payload);
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  })
};

export default fileModel;