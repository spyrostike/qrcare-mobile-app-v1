import { Action, action, Thunk , thunk } from 'easy-peasy';
import api from '../../resources/api'
import { API_STATUS_SUCCESS } from '../../../app-config';
import { ResponseModel } from '../../resources/response-model';
import { convertCreatureFromServerToApp } from '../../utils/ObjectUtils'
import { CriteriaSearch } from '../../model/criteria-search.model';
import { serializeQueryString } from '../../utils/HttpUtils';

export interface CreatureProps {
    qrcodeItemId: string;
    id?: string;
    firstName: string;
    lastName: string;
    gender: string;
    bloodType: string;
    birthDate: Date;
    weight: string;
    height: string;
    creatureImageId?: string;
    creatureImage?: string;
    creatureImageThumbnailId?: string;
    creatureImageThumbnail?: string;
    birthCertificateId?: string;
    birthCertificate?: string;
    birthCertificateThumbnailId?: string;
    birthCertificateThumbnail?: string;
    oldCreatureImageId?: string;
    oldCreatureImageThumbnailId?: string,
    oldBirthCertificateImageId?: string,
    oldBirthCertificateImageThumbnailId?: string,
    bloodTypeName?: string;
    genderName?: string;
    memberId?: string;
}

export interface CreatureModel {
  items: CreatureProps[];
  item: CreatureProps | null;
  setItems: Action<CreatureModel, CreatureProps[] | []>;
  setItem: Action<CreatureModel, CreatureProps | null>;
  add: Thunk<CreatureModel, CreatureProps>,
  edit: Thunk<CreatureModel, CreatureProps>,
  find: Thunk<CreatureModel, CriteriaSearch>,
  getInfo: Thunk<CreatureModel, string>,
  getInfoByQRCodeId: Thunk<CreatureModel, string>,
  removeItems: Thunk<CreatureModel, string[]>
}

const creatureModel : CreatureModel = {
  items: [],
  item : null,
  setItems: action((state, payload) => {
    state.items = payload
  }),
  setItem: action((state, payload) => {
    state.item = payload
  }),
  add: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/creature/add`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  edit: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/creature/update`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          // action.setItem(data.result.item)
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  find: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/creature/list?${serializeQueryString(payload)}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        
        if (status === API_STATUS_SUCCESS) {      
          const items: CreatureProps[] = data.result.map(item => convertCreatureFromServerToApp(item))
          if (items.length > 0) { 
            action.setItems([ ...getState().items, ...items ])
            resolve(items)
          } else {
            reject('array = 0')
          }
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  getInfo: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/creature/${payload}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          const item: CreatureProps = convertCreatureFromServerToApp(data.result)
          resolve(item)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  getInfoByQRCodeId: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/creature/creature-by-qrcode-id/${payload}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          const item: CreatureProps = convertCreatureFromServerToApp(data.result)
          resolve(item)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  removeItems: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/creature/delete`, payload);
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  })
};

export default creatureModel;