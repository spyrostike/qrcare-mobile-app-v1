import { Action, action, Thunk , thunk } from 'easy-peasy';
import api from '../../resources/api'
import { API_STATUS_SUCCESS } from '../../../app-config';
import { ResponseModel } from '../../resources/response-model';
import { convertVaccineBookFromServerToApp } from '../../utils/ObjectUtils'
import { CriteriaSearch } from '../../model/criteria-search.model';
import { serializeQueryString } from '../../utils/HttpUtils';

export interface VaccineBookProps {
  id: string;
  vaccineType: number;
  vaccineTypeName?: string;
  vaccineTypeOther?: string;
  creatureId: string;
}

export interface VaccineBookModel {
  items: VaccineBookProps[];
  item: VaccineBookProps | null;
  setItems: Action<VaccineBookModel, VaccineBookProps[] | []>;
  setItem: Action<VaccineBookModel, VaccineBookProps | null>;
  add: Thunk<VaccineBookModel, VaccineBookProps>,
  edit: Thunk<VaccineBookModel, VaccineBookProps>,
  find: Thunk<VaccineBookModel, CriteriaSearch>,
  getInfo: Thunk<VaccineBookModel, string>,
  removeItems: Thunk<VaccineBookModel, string[]>
}

const vaccineBookModel : VaccineBookModel = {
  items: [],
  item : null,
  setItems: action((state, payload) => {
    state.items = payload
  }),
  setItem: action((state, payload) => {
    state.item = payload
  }),
  add: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/vaccine/book/add`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          // action.setItem(data.result.item)
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  edit: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/vaccine/book/update`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  find: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/vaccine/book/list/${payload.creatureId}?${serializeQueryString(payload)}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        
        if (status === API_STATUS_SUCCESS) { 
          const items: VaccineBookProps[] = data.result.map(item => convertVaccineBookFromServerToApp(item))
          if (items.length > 0) { 
            action.setItems([ ...getState().items, ...items ])
            resolve(items)
          } else {
            reject('array = 0')
          }
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  getInfo: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/vaccine/book/${payload}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        
        if (status === API_STATUS_SUCCESS) {
          const item: VaccineBookProps = convertVaccineBookFromServerToApp(data.result)
          resolve(item)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  removeItems: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/vaccine/book`, payload);
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  })
};

export default vaccineBookModel;