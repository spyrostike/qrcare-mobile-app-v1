import { Action, action, Thunk , thunk } from 'easy-peasy';
import api from '../../resources/api'
import { API_STATUS_SUCCESS } from '../../../app-config';
import { ResponseModel } from '../../resources/response-model';
import { convertVaccineFromServerToApp } from '../../utils/ObjectUtils'
import { CriteriaSearch } from '../../model/criteria-search.model';
import { serializeQueryString } from '../../utils/HttpUtils';

export interface VaccineProps {
  id: string;
  hospitalName: string;
  date: Date;
  vaccineBookId: string;
  description: string;
  imageId?: string;
  image?: string;
  imageThumbnailId?: string;
  imageThumbnail?: string;
}

export interface VaccineModel {
  items: VaccineProps[];
  item: VaccineProps | null;
  setItems: Action<VaccineModel, VaccineProps[] | []>;
  setItem: Action<VaccineModel, VaccineProps | null>;
  add: Thunk<VaccineModel, VaccineProps>,
  edit: Thunk<VaccineModel, VaccineProps>,
  find: Thunk<VaccineModel, CriteriaSearch>,
  getInfo: Thunk<VaccineModel, string>,
  removeItems: Thunk<VaccineModel, string[]>
}

const vaccineModel : VaccineModel = {
  items: [],
  item : null,
  setItems: action((state, payload) => {
    state.items = payload
  }),
  setItem: action((state, payload) => {
    state.item = payload
  }),
  add: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/vaccine/add`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  edit: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/vaccine/update`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  find: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/vaccine/list/${payload.vaccineBookId}?${serializeQueryString(payload)}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        
        if (status === API_STATUS_SUCCESS) { 
          const items: VaccineProps[] = data.result.map(item => convertVaccineFromServerToApp(item))
          if (items.length > 0) { 
            action.setItems([ ...getState().items, ...items ])
            resolve(items)
          } else {
            reject('array = 0')
          }
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  getInfo: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/vaccine/${payload}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        
        if (status === API_STATUS_SUCCESS) {
          const item: VaccineProps = convertVaccineFromServerToApp(data.result)
          resolve(item)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  removeItems: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/vaccine/delete`, payload);
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  })
};

export default vaccineModel;