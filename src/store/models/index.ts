import test, { TestModel } from './test';
import app, { AppModel } from './app';
import creature, { CreatureModel } from './creature';
import drugAllergy, { DrugAllergyModel } from './drug-allergy';
import drug, { DrugModel } from './drug';
import foodAllergy, { FoodAllergyModel } from './food-allergy';
import file, { FileModel } from './file';
import medical, { MedicalModel } from './medical';
import member, { MemberModel } from './member';
import qrcode, { QRCodeModel } from './qrcode';
import treatmentHistory, { TreatmentHistoryModel } from './treatment-history';
import vaccineBook, { VaccineBookModel } from './vaccine-book';
import vaccine, { VaccineModel } from './vaccine';

export interface StoreModel {
  test: TestModel;
  app: AppModel;
  creature: CreatureModel;
  drugAllergy: DrugAllergyModel
  drug: DrugModel;
  foodAllergy: FoodAllergyModel
  file: FileModel;
  medical: MedicalModel;
  member: MemberModel;
  qrcode: QRCodeModel;
  treatmentHistory: TreatmentHistoryModel;
  vaccineBook: VaccineBookModel;
  vaccine: VaccineModel;
};

const model: StoreModel = {
  test,
  app,
  creature,
  drugAllergy,
  drug,
  foodAllergy,
  file,
  medical,
  member,
  qrcode,
  treatmentHistory,
  vaccineBook,
  vaccine
};

export default model;