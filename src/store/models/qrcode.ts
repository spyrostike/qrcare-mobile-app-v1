import { Action, action , Thunk ,thunk } from 'easy-peasy';
import api from '../../resources/api'
import { API_STATUS_SUCCESS } from '../../../app-config';
import { ResponseModel } from '../../resources/response-model'

export interface QRCodeProps {
  id: string;
  usage_status: number;
}

export interface QRCodeModel {
  item: QRCodeProps | null;
  setItem: Action<QRCodeModel, QRCodeProps | null>;
  getQRCodeById: Thunk<QRCodeModel>;
}

const qrcodeModel : QRCodeModel = {
  item : null,
  setItem: action((state, payload) => {
    state.item = payload
  }),
  getQRCodeById: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/qrcode/get-qrcode-item-by-id/${payload}`);
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          // action.setItem(data.result as QRCodeProps)
          resolve(data.result)
        } else {
          // action.setItem(null)
          reject(errorMessage)
        }
      } catch (error) {
        // action.setItem(null)
        reject(error.message)
      }
    
    })
   
    // action.setItem()
          
  })
};

export default qrcodeModel;