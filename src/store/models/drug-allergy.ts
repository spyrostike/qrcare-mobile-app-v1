import { Action, action, Thunk , thunk } from 'easy-peasy';
import api from '../../resources/api'
import { API_STATUS_SUCCESS } from '../../../app-config';
import { ResponseModel } from '../../resources/response-model';
import { convertDrugAllergyFromServerToApp } from '../../utils/ObjectUtils'
import { CriteriaSearch } from '../../model/criteria-search.model';
import { serializeQueryString } from '../../utils/HttpUtils';

export interface DrugAllergyProps {
  id: string;
  name: string;
  imageId?: string;
  image?: string;
  imageThumbnailId?: string;
  imageThumbnail?: string;
  oldImageId?: string;
  oldImageThumbnailId?: string;
  description: string;
  creatureId: string;
}

export interface DrugAllergyModel {
  items: DrugAllergyProps[];
  item: DrugAllergyProps | null;
  setItems: Action<DrugAllergyModel, DrugAllergyProps[] | []>;
  setItem: Action<DrugAllergyModel, DrugAllergyProps | null>;
  add: Thunk<DrugAllergyModel, DrugAllergyProps>,
  edit: Thunk<DrugAllergyModel, DrugAllergyProps>,
  find: Thunk<DrugAllergyModel, CriteriaSearch>,
  getInfo: Thunk<DrugAllergyModel, string>,
  removeItems: Thunk<DrugAllergyModel, string[]>
}

const drugAllergyModel : DrugAllergyModel = {
  items: [],
  item : null,
  setItems: action((state, payload) => {
    state.items = payload
  }),
  setItem: action((state, payload) => {
    state.item = payload
  }),
  add: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.post(`/drug-allergy/add`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  edit: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/drug-allergy/update`, payload);
        const { status, data, errorMessage } = response.data as ResponseModel
        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  find: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/drug-allergy/list/${payload.creatureId}?${serializeQueryString(payload)}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        
        if (status === API_STATUS_SUCCESS) {
          const items: DrugAllergyProps[] = data.result.map(item => convertDrugAllergyFromServerToApp(item))

          if (items.length > 0) { 
            action.setItems([ ...getState().items, ...items ])
            resolve(items)
          } else {
            reject('array = 0')
          }
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  getInfo: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.get(`/drug-allergy/${payload}`);
        const { status, data, errorMessage } = response.data as ResponseModel
        
        if (status === API_STATUS_SUCCESS) {
          const item: DrugAllergyProps = convertDrugAllergyFromServerToApp(data.result)
          resolve(item)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  }),
  removeItems: thunk((action, payload, {getState ,getStoreActions , getStoreState}) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await api.put(`/drug-allergy/delete`, payload);
        const { status, data , errorMessage  } = response.data as ResponseModel

        if (status === API_STATUS_SUCCESS) {
          resolve(data.result)
        } else {
          reject(errorMessage)
        }
      } catch (error) {
        reject(error.message)
      }
    })
  })
};

export default drugAllergyModel;