export const API_ENDPOINT = 'https://qrcare-official.com/api/v1'
// export const API_ENDPOINT = 'http://128.199.233.185/api/v1'
// export const API_ENDPOINT = 'http://192.168.1.39:3002/api/v1'
// export const API_ENDPOINT = 'http://192.168.1.40:3002/api/v1'
export const APPLICATION_ID = 'xxxxxx'

export const API_IMAGE_PATH = 'https://qrcare-official.com/uploads'
// export const API_IMAGE_PATH = 'https://qrcare-official.com/uploads'
export const API_VIDEO_PATH = 'https://qrcare-official.com/uploads/video'
export const API_STATUS_SUCCESS = '00'
export const API_STATUS_ERROR = '10'

export const DOMAIN_URL = 'https://qrcare-official.com'
